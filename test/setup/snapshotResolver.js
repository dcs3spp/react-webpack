/**
 * At the time of writing .ts based snapshot resolve is not possible
 * @remarks https://github.com/facebook/jest/issues/8330
 */
const path = require('path');

const rootDir = path.resolve(__dirname, '../..');

module.exports = {
  /** resolves from test to snapshot path */
  resolveSnapshotPath: (testPath, snapshotExtension) => {
    return testPath.replace('src/', '__snapshots__/') + snapshotExtension;
  },

  /** resolves from snapshot to test path */
  resolveTestPath: (snapshotFilePath, snapshotExtension) => {
    return snapshotFilePath
      .replace('__snapshots__/', 'src/')
      .slice(0, -snapshotExtension.length);
  },

  testPathForConsistencyCheck: `${rootDir}/src/app/components/Course/Course.spec.tsx`,
};
