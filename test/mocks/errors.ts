import { AxiosError, AxiosResponse } from 'axios';

import { apiInterfaces } from '../../src/app/redux/features/api';
import { courseModels } from '../../src/app/redux/features/course';

/**
 * Mock a failure payload for an api request
 * @param uniqueId  Id of the component that sent the api request
 * @param fromAction  Action that raised component
 * @param fromUrl The url request
 * @param fromMethod  The request method
 * @param message  The error message
 * @param stack  Optional stacktrace
 * @returns Payload encapsulating api request and error message
 * @remarks The failure payload is encapsulated in an IApiFailure instance
 */
export const mockError = (
  uniqueId: string,
  fromAction: string,
  fromUrl: string,
  fromMethod: string,
  message: string,
  stack?: string,
): apiInterfaces.IApiFailure => {
  const axiosError: AxiosError = {
    config: {},
    isAxiosError: true,
    message: message,
    name: 'Mock Axios Error',
    stack: stack,
    toJSON: () => Object
  };

  return {
    fromAction: fromAction,
    request: {
      method: fromMethod,
      sourceComponent: uniqueId,
      url: fromUrl,
    },
    error: axiosError,
  };
};

/**
 * Mock a failure payload for an api request including an axios response within
 * the error
 * @param uniqueId  Id of the component that sent the api request
 * @param fromAction  Action that raised component
 * @param fromUrl The url request
 * @param fromMethod  The request method
 * @param message  The error message
 * @param stack  Optional stacktrace
 * @returns Payload encapsulating api request and error message including response
 * The data payload of the response contains a single course [{ courseID: 1014760, courseName: 'Course 1' }]
 * @remarks The failure payload is encapsulated in an IApiFailure instance
 */
export const mockErrorWithResponse = (
  uniqueId: string,
  fromAction: string,
  fromUrl: string,
  fromMethod: string,
  message: string,
  stack?: string,
): apiInterfaces.IApiFailure => {
  const course: courseModels.Course = {
    courseID: 1014760,
    courseName: 'Course 1',
  };

  const axiosError: AxiosError = {
    config: {},
    isAxiosError: true,
    message: message,
    name: 'Mock Axios Error',
    response: {
      data: [course],
      status: 200,
      statusText: 'Ok',
      headers: {},
      config: {},
    } as AxiosResponse<courseModels.Course[]>,
    stack: stack,
    toJSON: () => Object
  };

  return {
    fromAction: fromAction,
    request: {
      method: fromMethod,
      sourceComponent: uniqueId,
      url: fromUrl,
    },
    error: axiosError,
  };
};

/**
 * Mock a failure payload for an api request including an axios request within
 * the error
 * @param uniqueId  Id of the component that sent the api request
 * @param fromAction  Action that raised component
 * @param fromUrl The url request
 * @param fromMethod  The request method
 * @param message  The error message
 * @param stack  Optional stacktrace
 * @returns Payload encapsulating api request and error message, including request
 * @remarks The failure payload is encapsulated in an IApiFailure instance
 */
export const mockErrorWithRequest = (
  uniqueId: string,
  fromAction: string,
  fromUrl: string,
  fromMethod: string,
  message: string,
  stack?: string,
): apiInterfaces.IApiFailure => {
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function (args) {
    if (this.readyState == this.DONE) {
      console.log('');
    }
  };

  const axiosError: AxiosError = {
    config: {},
    isAxiosError: true,
    message: message,
    name: 'Mock Axios Error',
    request: xhr,
    stack: stack,
    toJSON: () => Object
  };

  return {
    fromAction: fromAction,
    request: {
      method: fromMethod,
      sourceComponent: uniqueId,
      url: fromUrl,
    },
    error: axiosError,
  };
};
