import { createStore, Store } from 'redux';

import rootReducer from '../../src/app/redux/store/rootReducer';

/**
 * Create an empty mock redux store with reducers only
 * @returns Empty store with reducers initialised to empty state.
 */
export const mockStore = (
  initialState = {
    courses: {
      isLoadingCourses: false,
      courses: { courseIds: [], coursesById: {} },
    },
    errors: { error: { errors: {}, components: [] } },
  },
): Store => {
  return createStore(rootReducer, initialState);
};
