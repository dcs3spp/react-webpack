import * as errorMocks from './errors';
import * as courseMocks from './courses';
import * as storeMocks from './store';

export { errorMocks, courseMocks, storeMocks };
