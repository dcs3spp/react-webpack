import { AxiosResponse } from 'axios';

import { ApiRequest } from '../../src/app/redux/features/api/actions';
import { Courses } from '../../src/app/redux/features/course/types';
import { IApiSuccess } from '../../src/app/redux/features/api/interfaces';

/**
 * Create a mock payload to emulate successful retrieval of all courses
 * @param uniqueId  Unique id for the component that made the request
 * @returns An indexed object structure of all courses
 * @remarks An example structure of index returned is:
 * {
 *   coursesById: {
 *     ['4321']: { courseName: 'Course 1', courseID: 4321 },
 *     ['1234']: { courseName: 'Course 2', courseID: 1234 },
 *   },
 *   courseIds: [4321, 1234],
 * };
 */
export const mockCourses = (uniqueId: string): IApiSuccess<Courses> => {
  const mockRequestPayload = new ApiRequest<Courses>(
    'GET',
    uniqueId,
    'http://localhost:3000/courses',
  );

  const mockData: Courses = {
    coursesById: {
      ['4321']: { courseName: 'Course 1', courseID: 4321 },
      ['1234']: { courseName: 'Course 2', courseID: 1234 },
    },
    courseIds: [4321, 1234],
  };

  const mockResponse: AxiosResponse<Courses> = {
    config: {},
    data: mockData,
    headers: {},
    status: 200,
    statusText: 'Ok',
  };

  return {
    request: mockRequestPayload,
    response: mockResponse,
    data: mockResponse.data,
  };
};

/**
 * Create a mock payload to emulate retrieval of empty courses
 * @param uniqueId  Unique id for the component that made the request
 * @returns An indexed object structure of all courses
 * @remarks An example structure of index returned is:
 * {
 *   coursesById: { },
 *   courseIds: [ ],
 * };
 */
export const mockEmptyCourses = (uniqueId: string): IApiSuccess<Courses> => {
  const mockRequestPayload = new ApiRequest<Courses>(
    'GET',
    uniqueId,
    'http://localhost:3000/courses',
  );

  const mockData: Courses = {
    coursesById: {},
    courseIds: [],
  };

  const mockResponse: AxiosResponse<Courses> = {
    config: {},
    data: mockData,
    headers: {},
    status: 200,
    statusText: 'Ok',
  };

  return {
    request: mockRequestPayload,
    response: mockResponse,
    data: mockResponse.data,
  };
};
