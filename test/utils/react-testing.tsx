import React from 'react';
import { RenderResult, render } from '@testing-library/react';
import { MemoryRouter, Route } from 'react-router';
import { Store } from 'redux';
import { Provider } from 'react-redux';

import { storeMocks } from '../mocks';

/**
 * Wrap a react element within a memory router.
 * @param ui React element to render in router
 * @param history Router history representing as a list of strings of route paths.
 * Defaults to an empty list
 * @returns Result of react-testing-library render operation
 */
export const renderWithRouter = (
  ui: JSX.Element,
  history: string[] = [],
): RenderResult => {
  return {
    ...render(
      <MemoryRouter initialEntries={history}>
        <div>
          <Route path="/courses" render={() => 'COURSES'} />
          <Route path="/about" render={() => 'ABOUT'} />
          <Route path="/" exact render={() => 'HOME'} />
        </div>
        {ui}
      </MemoryRouter>,
    ),
  };
};

/**
 * Wraps a React element within a Redux Provider element
 * @param ui  The React element to wrap
 * @param store  Redux store instance, defaults to [[storeMocks.mockStore()]]
 * @returns  Result of react-testing-library render with store attribute appended
 * @remarks The React element is wrapped within a React-redux Provider as follows:
 *
 * ```
 * <Provider store={store}>
 * {ui}
 * </Provider>
 * ```
 *
 * Returns store attributed appended to react-testing-library render operation
 * ``` typescript
 * RenderResult & { store: Store }
 * ```
 */
export const renderWithRedux = (
  ui: JSX.Element,
  store: Store = storeMocks.mockStore(),
): RenderResult & { store: Store } => {
  return {
    ...render(<Provider store={store}>{ui}</Provider>),
    store,
  };
};
