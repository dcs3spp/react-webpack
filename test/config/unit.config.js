module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts', 'tsx'],
  rootDir: __dirname + '../../../src',
  testRegex: '.spec.(ts|tsx)$',
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
  coverageDirectory: './coverage',
  moduleNameMapper: {
    '\\.(css|less|scss|sass)$': 'identity-obj-proxy',
  },
  setupFilesAfterEnv: [`${__dirname} + ../../setup/setupTestsAfterEnv.ts`],
  snapshotResolver: `${__dirname} + ../../setup/snapshotResolver.js`,
  snapshotSerializers: ['enzyme-to-json/serializer'],
  testEnvironment: 'jsdom',
  testRunner: 'jest-circus/runner',
  verbose: false,
  globals: {
    APP_CONF: {
      apiBaseURL: 'http://localhost:3000',
    },
    'ts-jest': {
      diagnostics: false,
    },
  },
};
