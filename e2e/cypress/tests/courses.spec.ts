context('Courses', () => {
  beforeEach(() => {
    cy.visit('/index.html');
    cy.contains('Courses').click();
  });

  it('should retrieve courses from api', () => {
    cy.get('[role="listitem"]').should('have.length.greaterThan', 0);
    cy.get('[aria-label="course name"')
      .first()
      .should($el => {
        expect($el.text()).to.have.length.greaterThan(0);
      });
  });
});

context('Empty Courses', () => {
  let apiBaseUrl = '';

  before(function() {
    apiBaseUrl = Cypress.env('API_BASE_URL');
  });

  it('should render empty courses when none retrieved', () => {
    cy.visit('/index.html');

    // stub the /course route with to force an empty response
    cy.server();
    cy.route({
      method: 'GET',
      url: `${apiBaseUrl}/course`,
      response: [],
    });

    // click the Courses link
    cy.contains('Courses').click();

    // 'No Courses' should be rendered
    cy.contains('No Courses');
  });
});

context('Courses fixtures', () => {
  let apiBaseUrl = '';

  before(function() {
    apiBaseUrl = Cypress.env('API_BASE_URL');
  });

  beforeEach(function() {
    cy.fixture('courses.json').as('coursesJSON');

    cy.visit('/index.html');

    cy.server();
    cy.route('GET', `${apiBaseUrl}/course`, '@coursesJSON');

    cy.contains('Courses').click();
  });

  it('should retrieve courses from fixtures data', function() {
    cy.get('[role="listitem"]').should('have.length.greaterThan', 0);
    cy.get('[aria-label="course name"')
      .first()
      .should($el => {
        expect($el.text()).to.have.length.greaterThan(0);
        expect($el.text()).to.contains('Fixture');
      });
  });
});
