context('Window', () => {
  beforeEach(() => {
    cy.visit('/index.html');
  });

  it('tests title is React Demo', () => {
    cy.title().should('include', 'React Demo');
  });
});
