context('Navigation Bar', () => {
  beforeEach(() => {
    cy.visit('/index.html');
  });

  it('tests that links navigate correctly', () => {
    cy.contains('About').click();
    cy.location('pathname').should('include', 'about');

    cy.contains('Home').click();
    cy.location('pathname').should('include', '/');

    cy.contains('Courses').click();
    cy.location('pathname').should('include', 'courses');
  });

  it('tests that can go back or forward in the browsers history', () => {
    cy.contains('About').click();
    cy.location('pathname').should('include', 'about');

    cy.go('back');
    cy.location('pathname').should('include', '/');

    cy.go('forward');
    cy.location('pathname').should('include', 'about');
  });
});

context('About page links', () => {
  const selector = 'have.attr';
  const href = 'href';

  const brotli =
    'https://medium.com/oyotech/how-brotli-compression-gave-us-37-latency-improvement-14d41e50fee4';
  const definePlugin = 'https://webpack.js.org/plugins/define-plugin/';
  const esesHyphenLint = 'https://eslint.org';
  const esLint = 'https://eslint.org';
  const fileLoader = 'https://github.com/webpack-contrib/file-loader';
  const gzip = 'https://www.gnu.org/software/gzip/';
  const htmlLoader = 'https://webpack.js.org/loaders/html-loader/';
  const terserPlugin = 'https://webpack.js.org/plugins/terser-webpack-plugin/';
  const tsLoader = 'https://github.com/TypeStrong/ts-loader';
  const typescript = 'https://www.typescriptlang.org';
  const webpack = 'https://webpack.js.org';
  const webpackBundleAnalyzer =
    'https://github.com/webpack-contrib/webpack-bundle-analyzer';
  const webpackMerge = 'https://github.com/survivejs/webpack-merge';

  beforeEach(() => {
    cy.visit('/index.html');
    cy.contains('About').click();
  });

  it('tests that has correct external navigation links', () => {
    cy.contains('Brotli').should(selector, href, brotli);
    cy.contains('DefinePlugin').should(selector, href, definePlugin);
    cy.contains('es-lint').should(selector, href, esesHyphenLint);
    cy.contains('Eslint').should(selector, href, esLint);
    cy.contains('file-loader').should(selector, href, fileLoader);
    cy.contains('gzip').should(selector, href, gzip);
    cy.contains('html-loader').should(selector, href, htmlLoader);
    cy.contains('Quickstart');
    cy.contains('ts-loader').should(selector, href, tsLoader);
    cy.contains('TerserPlugin').should(selector, href, terserPlugin);
    cy.contains('Typescript').should(selector, href, typescript);
    cy.contains('Webpack')
      .first()
      .should(selector, href, webpack);
    cy.get('a')
      .contains('Webpack Bundle Analyzer')
      .should(selector, href, webpackBundleAnalyzer);
    cy.contains('Webpack-merge').should(selector, href, webpackMerge);
  });

  it('tests anchor links', () => {
    cy.contains('Configuration').click();
    cy.hash().should('be', 'configuration');

    cy.contains('Image loading').click();
    cy.hash().should('be', 'image');

    cy.contains('Quickstart').click();
    cy.hash().should('be', 'quickstart');

    cy.contains('Stylesheet loading').click();
    cy.hash().should('be', 'stylesheet');

    cy.get('a[href*="#webpack"]').click();
    cy.hash().should('be', 'webpack');
  });
});

context('Home page links', () => {
  const baseUrl = Cypress.config('baseUrl');
  const selector = 'have.attr';
  const href = 'href';

  const about = '/about';
  const courses = '/courses';
  const cuid = 'https://www.npmjs.com/package/cuid';
  const here = 'https://gitlab.com/dcs3spp/nestjs_application';
  const materialUi = 'https://material-ui.com';
  const nestJs = 'https://nestjs.com';
  const react = 'https://reactjs.org';
  const readMe =
    'https://gitlab.com/dcs3spp/nestjs_application/blob/master/README.md';
  const redux = 'https://redux.js.org';
  const repository = 'https://gitlab.com/dcs3spp/nestjs_application';

  beforeEach(() => {
    cy.visit('/index.html');
  });

  it('tests that has correct external navigation links', () => {
    cy.get('[data-testid="homeIntroParagraph"]')
      .contains('About')
      .should(selector, href, about);
    cy.get('[data-testid="apiInstructionsParagraph"]')
      .contains('Courses')
      .should(selector, href, courses);
    cy.get('a')
      .contains('cuid')
      .should(selector, href, cuid);
    cy.get('a')
      .contains('here')
      .should(selector, href, here);
    cy.get('a')
      .contains('Material-UI')
      .should(selector, href, materialUi);
    cy.get('a')
      .contains('NestJS')
      .should(selector, href, nestJs);
    cy.get('a')
      .contains('React')
      .should(selector, href, react)
      .each($el => {
        cy.wrap($el).should(selector, href, react);
      });
    cy.get('a')
      .contains('ReadMe')
      .should(selector, href, readMe);
    cy.get('a')
      .contains('repository')
      .should(selector, href, repository);
    cy.get('a')
      .contains('Redux')
      .should(selector, href, redux);
  });
});
