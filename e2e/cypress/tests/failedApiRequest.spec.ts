context('Api Request Error', () => {
  let apiBaseUrl = '';

  before(function() {
    apiBaseUrl = Cypress.env('API_BASE_URL');
  });

  beforeEach(function() {
    cy.server({
      method: 'GET',
      delay: 1000,
      status: 400,
      response: {},
    });

    cy.route(`${apiBaseUrl}/course`, {
      errors: 'Simulated network failure',
    });

    cy.visit('/index.html');
    cy.contains('Courses').click();
  });

  it('should display error card upon network failure of backend api server', () => {
    const errorMessage = 'Request failed with status code 400';
    const action = '[course] ALL_COURSES_REQUEST';
    const learnMore = 'Learn More';
    const homeIcon = '[aria-label="home icon"]';

    cy.contains(errorMessage);
    cy.contains(action);
    cy.contains(learnMore);
    cy.get(homeIcon);
  });

  it('should display dialog when "Learn More" is clicked', () => {
    const actionFrom = '[course] ALL_COURSES_REQUEST';
    const expectedComponentIdLength = 25;
    const learnMore = 'Learn More';
    const responseText = 'Bad Request';
    const title = 'Request failed with status code 400';

    cy.contains(learnMore).click();

    cy.get('[id="error-dialog-title"').should($el => {
      expect($el.text()).to.equal(title);
    });

    cy.get(`input[value="${actionFrom}"]`);
    cy.get(`input[value="${responseText}"]`);
    cy.get('[data-testid="error-stacktrace"]').should($el => {
      expect($el.text()).to.have.length.greaterThan(0);
    });
    cy.get('[data-testid="error-requestinfo"').should($el => {
      expect($el.text()).to.have.length.greaterThan(0);
    });
    cy.get('[data-testid="error-sourceinfo-component"]')
      .find('input')
      .should('have.length', 1);

    cy.get('[data-testid="error-sourceinfo-component"]')
      .find('input')
      .should($el => {
        expect($el.val()).to.have.length(expectedComponentIdLength);
      });

    cy.get('[aria-label="close"]').should('have.length', 1);
  });

  it('should close dialog when close icon clicked', () => {
    const closeIcon = '[aria-label="close"]';
    const learnMore = 'Learn More';

    cy.contains(learnMore).click();
    cy.get(closeIcon).click();
  });

  it('should navigate to home route when home icon clicked', () => {
    const homeIcon = '[aria-label="home icon"]';

    cy.get(homeIcon).click();

    cy.location().should(location => {
      expect(location.pathname).to.equal('/');
    });
  });
});
