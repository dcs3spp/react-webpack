const bundleAnalyzer = require('webpack-bundle-analyzer');
const clean = require('clean-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

const constants = require('./webpack.constants');
const legalEnvironments = [
  'dev',
  'development',
  'prod',
  'production',
  'test',
  'tsting',
];

/**
 * Validate options object includes env property that includes a legal build environment value
 * @param {env: string} options The environment for the build: development | production | test
 */
function validateOptions(options) {
  if (!options.env) {
    throw new Error(
      'webpack.config.common:CommonConfig must be invoked with env property set',
    );
  }

  const valid = legalEnvironments.includes(options.env);
  if (!valid) {
    const errorMessage =
      'webpack.config.common:CommonConfig invoked with illegal build environment.\n' +
      'legal values are dev | development | prod | production | test | testing';

    throw new Error(errorMessage);
  }

  return valid;
}

/**
 * Create a webpack common configuration object for merging with environment
 * specific configuration object via webpack-merge utility.
 * @param {env: string} options The environment for the build: development | production | test
 */
const CommonConfig = function(options) {
  // validate that options object has an env property with legal build environment set
  validateOptions(options);

  // create application config object for build environment
  const configUtil = require('./webpack.env');
  const config = configUtil.createAppConfig(options.env);
  const APP_CONFIG = config.env;

  console.log(
    '\x1b[36m%s\x1b[0m',
    `Building with configuration ${JSON.stringify(APP_CONFIG, null, 2)}`,
  );

  const webpackConfig = {
    entry: {
      main: ['./main.tsx'],
    },
    context: constants.dir.SRC,
    output: {
      chunkFilename: '[name].[chunkhash].bundle.js',
      filename: '[name].js',
      path: constants.dir.BUILD,
    },
    target: 'web',
    resolve: {
      // imports for @material-ui/core will resolve to es version
      // to reduce bundle size, remove this if wish to use IE11 or
      // using a third party library that has a @material-ui/core dependency
      // that uses commonjs modules
      alias: {
        '@material-ui/core': '@material-ui/core/es',
      },
      extensions: ['.ts', '.tsx', '.js', '.json', '.css', '.scss'],
    },
    optimization: {
      splitChunks: {
        cacheGroups: {
          material: {
            // split material-ui
            test: /[\\/]node_modules[\\/]@material-ui[\\/]/,
            name: 'vendor.material',
            chunks: 'all',
            enforce: true,
            priority: 90,
          },
          vendor: {
            // split node_modules
            name: 'vendor',
            chunks: 'all',
            test: /[\\/]node_modules[\\/]/,
            priority: -20,
            enforce: true,
          },
          commonsAsync: {
            // common async chunks
            name: 'commons.async',
            minChunks: 2,
            chunks: 'async',
            priority: 0,
            reuseExistingChunk: true,
            minSize: 10000,
          },
        },
      },
    },
    plugins: [
      // clean the dist output directory
      new clean.CleanWebpackPlugin(),

      // inject into css scripts etc
      new HtmlWebPackPlugin({
        template: 'index.html',
      }),

      // ignore typings, e.g. to allow for stylesheets
      new webpack.WatchIgnorePlugin([/css\.d\.ts$/]),

      // allow analysing bundles
      new bundleAnalyzer.BundleAnalyzerPlugin({
        analyzerMode: 'disabled',
        generateStatsFile: true,
        statsOptions: { source: false },
      }),

      // define constant properties, e.g. application config
      // NOTE: ensure properties included in custom-typings.d.ts
      new webpack.DefinePlugin({
        APP_CONF: JSON.stringify(APP_CONFIG),
      }),
    ],
    module: {
      rules: [
        {
          // run tslint prior to transpiling
          test: /\.tsx?$/,
          enforce: 'pre',
          exclude: /node_modules/,
          loader: 'eslint-loader',
          options: {
            cache: false,
            configFile: './.eslintrc.yaml',
            emitWarning: true,
            eslintPath: require.resolve('eslint'),
            fix: true,
          },
        },
        {
          // transpile typescript into javascript
          test: /\.tsx?$/,
          loader: 'ts-loader',
          options: {
            configFile: path.resolve(__dirname, '../tsconfig.build.json'),
          },
        },
        {
          // html loader allows resolving image paths with file-loader
          test: /.\.html$/,
          use: ['html-loader'],
        },
        {
          // transform image paths to ./assets/images/'
          test: /.*\.(jpe?g|gif|png|svg)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: './assets/images/',
                publicPath: './assets/images/',
              },
            },
          ],
        },
      ],
    },
  };

  // return app config and webpack config
  return {
    appConfig: APP_CONFIG,
    webpackConfig: webpackConfig,
  };
};

module.exports = { CommonConfig };
