/**
 * Validate environment
 *
const legalEnvVars = ['SERVER_HOST', 'SERVER_PORT', 'API_BASE_URL'];
function validate() {
  legalEnvVars.forEach(function(item) {
    if (!process.env[item]) {
      throw new Error(`Environment variable ${item} is missing`);
    }
  });
}
validate();
*/

const createAppConfig = function(env) {
  return Object.freeze({
    env: {
      apiBaseURL: process.env.API_BASE_URL
        ? `${process.env.API_BASE_URL}`
        : 'http://localhost:3000',
      host: process.env.SERVER_HOST
        ? `${process.env.SERVER_HOST}`
        : 'localhost',
      isProd: (env === 'production') | (env === 'prod'),
      mode: env,
      port: process.env.SERVER_PORT ? `${process.env.SERVER_PORT}` : 5000,
    },
  });
};

module.exports = { createAppConfig };
