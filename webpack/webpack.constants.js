const path = require('path');

const buildDir = path.resolve(__dirname, '../dist');
const srcDir = path.resolve(__dirname, '../src');

module.exports = {
  dir: {
    BUILD: buildDir,
    SRC: srcDir,
  },
};
