const merge = require('webpack-merge');
const path = require('path');
const webpack = require('webpack');

const common = require('./webpack.config.common');
const constants = require('./webpack.constants');

console.log('\x1b[36m%s\x1b[0m', 'Building for development ...');
console.log('\x1b[36m%s\x1b[0m', `Content base => ${constants.dir.BUILD}`);
console.log('\x1b[36m%s\x1b[0m', `Content src => ${constants.dir.SRC}`);

const commonConfig = common.CommonConfig({ env: 'development' });
const developmentConfig = {
  devtool: 'inline-source-map',
  entry: {
    main: [
      `webpack-dev-server/client?http://${commonConfig.appConfig.host}:${commonConfig.appConfig.port}`,
      'webpack/hot/only-dev-server',
    ],
  },
  devServer: {
    port: commonConfig.appConfig.port,
    historyApiFallback: true,
    host: commonConfig.appConfig.host,
    hot: true,
    noInfo: false,
    quiet: false,
    contentBase: constants.dir.BUILD,
  },
  mode: 'development', // https://webpack.js.org/configuration/mode/#mode-development
  module: {
    rules: [
      {
        // link css modules
        test: /\.css$/,
        include: path.resolve(constants.dir.SRC, 'app'),
        use: [
          { loader: require.resolve('style-loader') },
          {
            loader: require.resolve('css-loader'),
            options: {
              modules: {
                localIdentName: '[path][name]__[local]___[hash:base64:5]',
              },
              importLoaders: 1,
              sourceMap: true,
            },
          },
        ],
      },
      {
        // link global css module in styles
        test: /\.css$/,
        include: path.resolve(constants.dir.SRC, 'styles'),
        use: [
          { loader: require.resolve('style-loader') },
          {
            loader: require.resolve('css-loader'),
            options: { sourceMap: true },
          },
        ],
      },
    ],
  },
  plugins: [
    // allow hot module replacement
    new webpack.HotModuleReplacementPlugin(),
  ],

  watch: true,
  watchOptions: {
    ignored: /node_modules/,
  },
};

module.exports = merge.strategy({
  'entry.main': 'prepend',
})(commonConfig.webpackConfig, developmentConfig);
