const common = require('./webpack.config.common');
const constants = require('./webpack.constants');

const CompressionPlugin = require('compression-webpack-plugin');
const merge = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const WebpackCdnPlugin = require('webpack-cdn-plugin');

console.log('\x1b[36m%s\x1b[0m', 'Building for production ...');

const commonConfig = common.CommonConfig({ env: 'production' });
const productionConfig = {
  devtool: 'source-map',
  mode: 'production', // https://webpack.js.org/configuration/mode/#mode-production
  module: {
    rules: [
      {
        // link css modules
        test: /\.css$/,
        include: path.resolve(constants.dir.SRC, 'app'),
        use: [
          { loader: MiniCssExtractPlugin.loader },
          {
            loader: require.resolve('css-loader'),
            options: {
              modules: {
                localIdentName: '[hash:base64]',
              },
              importLoaders: 1,
              sourceMap: true,
            },
          },
        ],
      },
      {
        // link global css module in styles
        test: /\.css$/,
        include: path.resolve(constants.dir.SRC, 'styles'),
        use: [
          { loader: MiniCssExtractPlugin.loader },
          {
            loader: require.resolve('css-loader'),
            options: { sourceMap: true },
          },
        ],
      },
    ],
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        sourceMap: true,
        terserOptions: {
          output: {
            comments: false,
          },
          extractComments: false,
        },
      }),
      new OptimizeCSSAssetsPlugin(),
    ],
  },
  performance: {
    maxEntrypointSize: 286720,
  },
  plugins: [
    // inject react and react-dom CDN to scripts in index.html
    new WebpackCdnPlugin({
      crossOrigin: 'anonymous',
      modules: {
        react: [
          {
            name: 'react',
            version: 16,
            path: 'umd/react.production.min.js',
          },
          {
            name: 'react-dom',
            version: 16,
            path: 'umd/react-dom.production.min.js',
          },
        ],
      },
      prod: true,
    }),

    // extract css to file, minimize in optimization
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css',
      ignoreOrder: false,
    }),

    // use brotli compression to compress resources larger than ~10K
    new CompressionPlugin({
      filename: '[path].br[query]',
      algorithm: 'brotliCompress',
      test: /\.(css|gif|html|jpg|jpeg|js|png|svg|)$/,
      compressionOptions: { level: 11 },
      threshold: 10240,
      minRatio: 0.8,
      deleteOriginalAssets: false,
    }),

    // use gzip compression to compress resources larger than ~10K
    new CompressionPlugin({
      filename: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.(css|gif|html|jpg|jpeg|js|png|svg|)$/,
      compressionOptions: { level: 9 },
      threshold: 10240,
      minRatio: 0.8,
      deleteOriginalAssets: false,
    }),
  ],
  externals: [
    // add libraries to be excluded from bundle add here
    // for example loaded from CDN
    { react: 'React' },
    { 'react-dom': 'ReactDOM' },
  ],
};

module.exports = merge(commonConfig.webpackConfig, productionConfig);
