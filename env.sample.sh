#!/usr/bin/env sh

export API_BASE_URL=http://localhost:3000
export SERVER_HOST=localhost
export SERVER_PORT=5000

# 
# API Server Configuration
# Api server environment variables required to run api production build
# on local development machine. Use only if running cypress to perform
# end-to-end testing with a full stack installed on local development machine.
#
export API_PATH=/Users/simon/Development/typescript/nestjs-demo-gitlab-clean
export DB_HOST=localhost
export DB_NAME=coursemanagement
export DB_USER=app
export DB_PASSWORD=Pa55w0rd
export DB_SCHEMA=coursemanagement
export DB_PORT=5432