import { StateType, ActionType } from 'typesafe-actions';

declare module 'typesafe-actions' {
  export type Store = StateType<
    typeof import('../app/redux/store/index').default
  >;

  export type RootState = StateType<
    ReturnType<typeof import('../app/redux/store/rootReducer').default>
  >;

  export type RootAction = ActionType<
    typeof import('../app/redux/store/rootAction').default
  >;

  export type Services = typeof import('../app/services').default;

  interface Types {
    RootAction: RootAction;
  }
}
