declare const APP_CONF: AppConfig;

interface AppConfig {
  apiBaseURL: string;
  isProd: boolean;
  mode: string;
}
