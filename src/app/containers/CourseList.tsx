import React, { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { GridSpacing } from '@material-ui/core/Grid';
import { RootState } from 'typesafe-actions';

import { Course } from '../components';

import {
  courseActions,
  courseSelectors,
  courseUtils,
} from '../redux/features/course';

import { UniqueIdComponentProps } from '../higher-order-components/withId';
import { withReduxErrorListener } from '../higher-order-components/withReduxErrorListener';

/**
 * Type declarations
 */
type CourseListProps = UniqueIdComponentProps;

/**
 * CourseList component
 */
const CourseList: React.FunctionComponent<CourseListProps> = (
  props: CourseListProps,
): JSX.Element => {
  const dispatch = useDispatch();
  const courses = useSelector((state: RootState) =>
    courseSelectors.getCoursesTransformed(state.courses),
  );
  const isLoading = useSelector((state: RootState) =>
    courseSelectors.getIsLoadingCourses(state.courses),
  );

  useEffect(() => {
    console.log('COURSELIST FETCHING COURSES');
    dispatch(
      courseActions.allCourseAction.request(
        courseUtils.requestFactories.getCourses(props.uniqueId),
      ),
    );
  }, [dispatch, props.uniqueId]); //[courses, dispatch, props.uniqueId]);

  if (isLoading) {
    return <p>Loading...</p>;
  }

  return (
    <div style={{ marginTop: 20, padding: 30 }}>
      {
        <Grid container justify="center" role="list" spacing={2 as GridSpacing}>
          {courses.length > 0 ? (
            courses.map(element => (
              <Grid item key={element.courseID}>
                <Course course={element} />
              </Grid>
            ))
          ) : (
            <Grid item key="emptyCourses">
              <p>No Courses</p>
            </Grid>
          )}
        </Grid>
      }
    </div>
  );
};

/**
 * Exports
 */

export default withReduxErrorListener(CourseList);
