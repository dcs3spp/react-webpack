import { Store } from 'redux';
import { MemoryRouter } from 'react-router';
import { mount, shallow } from 'enzyme';
import { Provider } from 'react-redux';
import React from 'react';

import { createMount, createShallow } from '@material-ui/core/test-utils';

import { apiInterfaces } from '../redux/features/api';
import { courseConstants } from '../redux/features/course';
import CourseList from './CourseList';
import { errorConstants } from '../redux/features/error';

import { courseMocks, errorMocks, storeMocks } from '../../../test/mocks';

/**
 * Create a mock CourseList encapsulated within a Provider element
 * @param  store The redux store
 * @returns CourseList embedded in Provider element
 * @remarks Creates a mock redux store and initialises the following
 * fragment:
 *
 * ```
 *  <Provider store={store}>
 *     <CourseList />
 *   </Provider>
 * ```
 */
const mockComponent = (store: Store = storeMocks.mockStore()): JSX.Element => {
  return (
    <Provider store={store}>
      <MemoryRouter>
        <CourseList />
      </MemoryRouter>
    </Provider>
  );
};

describe('<CourseList />', () => {
  let shallowFunc: typeof shallow;

  beforeAll(() => {
    shallowFunc = createShallow();
  });

  test('renders uniqueId property, wrapped inside a withErrorListener HoC', () => {
    const wrapper = shallowFunc(<CourseList />);
    const uniqueId: string = wrapper.prop('uniqueId');

    expect(uniqueId).toHaveLength(25);
    expect(wrapper.name()).toMatch('Connect(withErrorListener(CourseList))');
  });
});

describe('<CourseList /> Integration with Redux Store', () => {
  let mountFunc: typeof mount;

  beforeAll(() => {
    mountFunc = createMount();
  });

  test('renders properties from withId and withErrorListener HoCs with initial loading state', () => {
    const component = mockComponent();
    const wrapper = mountFunc(component);

    try {
      const courseListWrapper = wrapper.find('CourseList');
      expect(courseListWrapper).toHaveLength(1);

      const uniqueId = courseListWrapper.prop('uniqueId');
      expect(uniqueId).toHaveLength(25);

      const componentId = courseListWrapper.prop('componentId');
      expect(uniqueId).toEqual(componentId);

      const filteredErrors = courseListWrapper.prop('filteredErrors');
      expect(filteredErrors).toEqual([]);

      const clearComponentErrors = courseListWrapper.prop(
        'clearComponentErrors',
      );
      expect(typeof clearComponentErrors).toEqual('function');

      expect(courseListWrapper.render().text()).toEqual('Loading...');
    } finally {
      wrapper.unmount();
    }
  });

  test('renders error card when api errors exist in store for component instance', () => {
    const store = storeMocks.mockStore();
    const component = mockComponent(store);
    const wrapper = mountFunc(component);

    try {
      const courseListWrapper = wrapper.find('CourseList');
      const uniqueId: string = courseListWrapper.prop('uniqueId');

      const expectedUrl = 'http://localhost:3000/course';
      const expectedMethod = 'GET';
      const expectedMessage = 'Network Failure';

      store.dispatch({
        type: errorConstants.actions.NOTIFY_API_ERROR,
        payload: errorMocks.mockError(
          uniqueId,
          courseConstants.actions.ALL_COURSES_REQUEST.toString(),
          'http://localhost:3000/course',
          'GET',
          'Network Failure',
        ),
      });

      // update the wrapper and check error is rendered
      wrapper.update();

      // test that withErrorListener is rendered
      const errorListenerHoC = wrapper.find('withErrorListener(CourseList)');
      expect(errorListenerHoC).toHaveLength(1);

      // test that child component exists
      const apiErrorInfo = errorListenerHoC.childAt(0);
      expect(apiErrorInfo).toBeDefined();

      // test componentId property is defined and matches unique id
      const componentId: string = apiErrorInfo.prop('componentId');
      expect(componentId).toBeDefined();
      expect(componentId).toEqual(uniqueId);

      // test that errors property is defined and contains error dispatched
      const errors: apiInterfaces.IApiFailure[] = apiErrorInfo.prop('errors');
      expect(errors).toHaveLength(1);
      expect(errors[0].fromAction).toEqual(
        courseConstants.actions.ALL_COURSES_REQUEST.toString(),
      );
      expect(errors[0].error).toBeDefined();
      expect(errors[0].error.message).toEqual(expectedMessage);
      expect(errors[0].request).toBeDefined();
      expect(errors[0].request.method).toEqual(expectedMethod);
      expect(errors[0].request.sourceComponent).toEqual(uniqueId);
      expect(errors[0].request.url).toEqual(expectedUrl);
    } finally {
      wrapper.unmount();
    }
  });

  test('renders courses when no errors exist in store for component instance', () => {
    const store = storeMocks.mockStore();
    const component = mockComponent(store);
    const wrapper = mountFunc(component);

    try {
      const courseListWrapper = wrapper.find('CourseList');
      const uniqueId: string = courseListWrapper.prop('uniqueId');

      // dispatch an all courses success all action for this component
      const coursesPayload = courseMocks.mockCourses(uniqueId);
      store.dispatch({
        type: courseConstants.actions.ALL_COURSES_SUCCESS,
        payload: coursesPayload,
      });

      // there should be two courses retrieved
      wrapper.update();
      const courses = wrapper.find('Course');
      expect(courses).toHaveLength(2);
    } finally {
      wrapper.unmount();
    }
  });

  test('renders empty state when no courses exist in store for component instance', () => {
    const store = storeMocks.mockStore();
    const component = mockComponent(store);
    const wrapper = mountFunc(component);

    try {
      const courseListWrapper = wrapper.find('CourseList');
      const uniqueId: string = courseListWrapper.prop('uniqueId');

      // dispatch an all courses success all action for this component
      const coursesPayload = courseMocks.mockEmptyCourses(uniqueId);
      store.dispatch({
        type: courseConstants.actions.ALL_COURSES_SUCCESS,
        payload: coursesPayload,
      });

      // there should be no courses retrieved
      wrapper.update();
      const courses = wrapper.find({ children: 'No Courses' });
      expect(courses).toHaveLength(1);
    } finally {
      wrapper.unmount();
    }
  });

  test('dispatches CLEAR_COMPONENT_API_ERRORS action when component is unmounted', () => {
    const store = storeMocks.mockStore();
    const component = mockComponent(store);
    const wrapper = mountFunc(component);
    const courseListWrapper = wrapper.find('CourseList');
    const uniqueId: string = courseListWrapper.prop('uniqueId');

    try {
      // update store with mock error
      store.dispatch({
        type: errorConstants.actions.NOTIFY_API_ERROR,
        payload: errorMocks.mockError(
          uniqueId,
          courseConstants.actions.ALL_COURSES_REQUEST.toString(),
          'http://localhost:3000/course',
          'GET',
          'Network Failure',
        ),
      });

      // update the wrapper and check error is rendered
      wrapper.update();

      // test that withErrorListener is rendered before unmounting
      const errorListenerHoC = wrapper.find('withErrorListener(CourseList)');
      expect(errorListenerHoC).toHaveLength(1);
    } finally {
      wrapper.unmount();

      // the errors for the component should be cleared from the store
      const rootState = store.getState();
      expect(rootState.errors.error.errors[uniqueId]).toBeUndefined();
      expect(rootState.errors.error.components).toHaveLength(0);
    }
  });
});
