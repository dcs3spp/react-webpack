import React, { Component, ComponentType } from 'react';

import { connect, MapStateToProps } from 'react-redux';
import { RootState } from 'typesafe-actions';
import { Diff } from 'utility-types';

import { ApiErrorInfo } from './components/ApiErrorInfo';

import { errorActions } from '../../redux/features/error';
import { ErrorState } from '../../redux/features/error/reducer';
import { filterErrors } from '../../redux/features/error/selectors';
import { IApiFailure } from '../../redux/features/api/interfaces';
import { withId, UniqueIdComponentProps } from '../withId';

/* ===================================== HoC =========================================== */

/**
 * HoC that enhances the behaviour of base component by listening on redux store for errors notified.
 * If no errors are found then the base component is rendered with it's own properties, otherwise
 * an error dialog is displayed summarising the api error.
 *
 * @param BaseComponent  The component to wrap. This must have a uniqueId property rendered by withId HoC.
 * @returns A Component class that is composed of connected reat-redux class and withId HoC's.
 * @remarks The base component has no properties injected.
 */
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const withReduxErrorListener = <
  BaseProps extends UniqueIdComponentProps
>(
  BaseComponent: ComponentType<BaseProps>,
) => {
  /** ================================ Redux properties =================== */

  /**
   * The object type returned by mapState function.
   */
  type StateProps = {
    componentId: string;
    filteredErrors: IApiFailure[];
  };

  /**
   * Function to return subset of store state that filters errors for the wrapped component via uniqueId property
   * @param state  The root state
   * @param ownProps  uniqueId property is required to fullfil filter operation.
   * @returns  StateProps type that contains a list of filtered errors by component id.
   */
  const mapState: MapStateToProps<
    StateProps,
    UniqueIdComponentProps,
    RootState
  > = (state: RootState, ownProps: UniqueIdComponentProps): StateProps => {
    console.log(`withErrorListener mapStateToProps => ${ownProps.uniqueId}`);
    return {
      componentId: ownProps.uniqueId,
      filteredErrors: filterErrors(state.errors as ErrorState, ownProps),
    } as StateProps;
  };

  /**
   * Dispatch object to clear component errors
   */
  const mapDispatch = {
    clearComponentErrors: errorActions.clearComponentErrorsAction,
  };

  /**
   * Used internally within HOC class and excluded from react-redux connect
   * function ownProps argument.
   * The only properties that should be possed down when rendering BaseComponent is BaseProps.
   */
  type THocInnerProps = TReduxProps;
  type TDispatchProps = typeof mapDispatch;
  type TStateProps = ReturnType<typeof mapState>;
  type TReduxProps = TStateProps & TDispatchProps;

  /** ================================ Redux properties =================== */

  /**
   * Remove the following injected properties injected from Base props:
   * - redux state and dispatch
   */
  type HocProps = Diff<BaseProps, THocInnerProps>;

  /** =============================== ErrorListener Component Class ==================== */

  /**
   * ErrorListener component class
   * This accepts:
   * - Redux state and dispatch properties
   * On unmount the component dispatches the CLEAR_COMPONENT_API_ERRORS
   * action to remove all errors from the store that are associated with
   * the managed component.
   */
  class ErrorListener extends Component<THocInnerProps, never> {
    static displayName = `withErrorListener(${BaseComponent.name})`;
    static readonly WrappedComponent = BaseComponent;

    /**
     * Handler for when component unmounted
     * @remarks Dispatches CLEAR_COMPONENT_API_ERRORS for the managed component
     */
    componentWillUnmount(): void {
      console.log(
        `withErrorListener HoC is unmounting => clearing errors for managed component ${this.props.componentId}`,
      );
      this.props.clearComponentErrors(this.props.componentId);
    }

    /**
     * Render error if there is one to display, otherwise render the base component
     * @returns Rendered error if error occurred. Rendered base component if no error occurred.
     * Base Component is rendered with it's own props only
     * @remarks Need to cast to unknown and then BaseProps to keep compiler happy.
     */
    render(): JSX.Element {
      const { ...restProps } = this.props;
      console.log(
        `withErrorListener [error_count=${this.props.filteredErrors.length}]`,
      );

      if (this.props.filteredErrors.length > 0) {
        console.log('ErrorListener spawning ErrorInfoBase');
        return (
          <ApiErrorInfo
            componentId={this.props.componentId}
            errors={this.props.filteredErrors}
          />
        );
      } else {
        return <BaseComponent {...((restProps as unknown) as BaseProps)} />;
      }
    }
  }

  /**
   * Connect listener to redux store
   */
  const ConnectedListener = connect<
    TStateProps,
    TDispatchProps,
    HocProps, // BaseProps - TReduxProps.
    RootState
  >(
    mapState,
    mapDispatch,
    null, // no merge props
    {}, // no options
  )(ErrorListener); // Accepts TReduxProps.

  /**
   * Inject uniqueId property into error listener
   */
  return withId(ConnectedListener);
};
