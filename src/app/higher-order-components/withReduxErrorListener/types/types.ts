import { apiInterfaces } from '../../../redux/features/api';

export type ErrorDialogProps = {
  info: apiInterfaces.IApiFailure;
  open: boolean;
  onClose: () => void;
};
export type FilteredErrorProps = {
  componentId: string;
  errors: apiInterfaces.IApiFailure[];
};
export type ErrorProps = { info: apiInterfaces.IApiFailure };
