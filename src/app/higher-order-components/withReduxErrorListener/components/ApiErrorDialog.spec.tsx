import { act } from 'react-dom/test-utils';
import { mount, ReactWrapper, shallow } from 'enzyme';
import React, { useState } from 'react';

import { ApiErrorDialog } from './ApiErrorDialog';
import { apiInterfaces } from '../../../redux/features/api';
import { courseConstants } from '../../../redux/features/course';
import { errorMocks } from '../../../../../test/mocks';

import Button from '@material-ui/core/Button';
import { createShallow, createMount } from '@material-ui/core/test-utils';

/** TestCase properties */
type TestDialogProps = {
  error: apiInterfaces.IApiFailure;
  onClose: jest.Mock<any, any>;
};

/**
 * Test component to mock opening the dialog via a button
 * @param props Properties containing error and onClose mock function
 * @returns A React element that contains a button and an ApiErrorDialog
 */
const TestCaseDialog: React.FunctionComponent<TestDialogProps> = (
  props: TestDialogProps,
): JSX.Element => {
  const [open, setOpen] = useState(false);

  const onOpen = (): void => {
    setOpen(true);
  };

  return (
    <React.Fragment>
      <Button onClick={onOpen}>Show Dialog</Button>
      <ApiErrorDialog
        data-testid="testerdialog"
        info={props.error}
        open={open}
        onClose={props.onClose}
      />
    </React.Fragment>
  );
};

/**
 * Find close icon in wrapper
 * @param wrapper The wrapper to perform the search in
 * @returns Result of wrapper find operation
 * @remarks Searches for the close icon for element matching aria-label="close"
 */
function findCloseIcon(wrapper: ReactWrapper): ReactWrapper {
  return wrapper.find('[aria-label="close"]');
}

/**
 * Find an error message as a child within a supplied React wrapper instance.
 * @param wrapper The react wrapper instance to perform the search in
 * @param message The error message to search for. If not specified then uses default value
 * 'Unknown - Error message unavailable'
 * @returns React wrapper returned from issuing a find on the wrapper parameter to search
 * for the error message
 */
const findErrorMessage = (
  wrapper: ReactWrapper,
  message = 'Unknown - Error message unavailable',
): ReactWrapper => {
  return wrapper.find({ children: message });
};

/**
 * Find error requestinfo based upon the data-testid property, within a supplied React wrapper instance.
 * @param wrapper The react wrapper instance to perform the search in
 * @returns React wrapper returned from issuing a find on the wrapper parameter to search
 * for the error message
 * @remarks Looks for element with data-testid of 'error-requestinfo'
 */
const findRequestInfo = (wrapper: ReactWrapper): ReactWrapper => {
  return wrapper.find('[data-testid="error-requestinfo"]');
};

/**
 * Find axios error response based upon the data-testid property, within a supplied React wrapper instance.
 * @param wrapper The react wrapper instance to perform the search in
 * @returns React wrapper returned from issuing a find on the wrapper parameter to search
 * for the error message
 * @remarks Looks for element with data-testid of 'error-responseinfo'
 */
const findResponseInfo = (wrapper: ReactWrapper): ReactWrapper => {
  return wrapper.find('[data-testid="error-responseinfo"]');
};

/**
 * Find source action based upon the data-testid property, within a supplied React wrapper instance.
 * @param wrapper The react wrapper instance to perform the search in
 * @returns React wrapper returned from issuing a find on the wrapper parameter to search
 * for control having data-testid property.
 * @remarks Looks for element with data-testid of 'sourceinfo-action'
 */
const findSourceAction = (wrapper: ReactWrapper): ReactWrapper => {
  return wrapper.find('[data-testid="error-sourceinfo-action"]');
};

/**
 * Find source component based upon the data-testid property, within a supplied React wrapper instance.
 * @param wrapper The react wrapper instance to perform the search in
 * @returns React wrapper returned from issuing a find on the wrapper parameter to search
 * for control having data-testid property.
 * @remarks Looks for element with data-testid of 'sourceinfo-component'
 * @
 */
const findSourceComponent = (wrapper: ReactWrapper): ReactWrapper => {
  return wrapper.find('[data-testid="error-sourceinfo-component"]');
};

/**
 * Find a stack trace based upon the data-testid property, within a supplied React wrapper instance.
 * @param wrapper The react wrapper instance to perform the search in
 * @returns React wrapper returned from issuing a find on the wrapper parameter to search
 * for control having data-testid property.
 * @remarks Looks for element with data-testid of 'stacktrace'
 */
const findStackTrace = (wrapper: ReactWrapper): ReactWrapper => {
  return wrapper.find('[data-testid="error-stacktrace"]');
};

/**
 * Find test case dialog in wrapper
 * @param wrapper The wrapper to perform the search in
 * @returns Result of wrapper find operation
 * @remarks Searches for the test case dialog for element matching data-testid="testerdialog"
 */
function findTestCaseDialog(wrapper: ReactWrapper): ReactWrapper {
  return wrapper.find('[data-testid="testerdialog"]');
}

/**
 * Find XmlHttpRequestInfo based upon the data-testid property, within a supplied React wrapper instance.
 * @param wrapper The react wrapper instance to perform the search in
 * @returns React wrapper returned from issuing a find on the wrapper parameter to search
 * for control having data-testid property.
 * @remarks Looks for element with data-testid of 'error-xmlhttprequest'
 */
const findXmlHttpRequestInfo = (wrapper: ReactWrapper): ReactWrapper => {
  return wrapper.find('[data-testid="error-xmlhttprequest"]');
};

/**
 * Open the test case dialog by simulating a click on the button
 * @param wrapper  Wrapper instance of TestCaseDialog
 */
const openTestCaseDialog = async (wrapper: ReactWrapper): Promise<void> => {
  const btnText = 'Show Dialog';
  const btn = wrapper.find({ children: btnText });
  expect(btn.length).toBeGreaterThan(0);
  await act(async () => {
    btn.first().simulate('click');
  });
  wrapper.update();
};

/**
 * Create a mock error. Use when need to have control over componentID, error message
 * and stack
 * @param componentId  The component id
 * @param message  The component message
 * @param stack  Optional stacktrace
 * @returns Mock error
 * @remarks method is initialsed to 'GET' and url set to 'http://localhost:3000/courses'
 */
const mockError = (
  componentId: string,
  message: string,
  stack?: string,
): apiInterfaces.IApiFailure => {
  const method = 'GET';
  const url = 'http://localhost:3000/courses';

  return errorMocks.mockError(
    componentId,
    courseConstants.actions.ALL_COURSES_REQUEST.toString(),
    url,
    method,
    message,
    stack,
  );
};

/**
 * Create a mock error for testing. Use when need to quickly create a default error mock
 * @returns Error instance with componentId set to '12345' and error message 'Mock Error'
 * The request method is set to 'GET' and the request url is set to 'http://localhost:3000/courses'
 */
const mockTestError = (): apiInterfaces.IApiFailure => {
  const componentID = '12345';
  const message = 'Mock Error';

  return mockError(componentID, message);
};

/**
 * Create a mock error for testing that includes a stacktrace. Use when need to
 * quickly create a default error.
 * @returns Error instance with componentId set to '12345' and error message 'Mock Error'
 * The request method is set to 'GET' and the request url is set to 'http://localhost:3000/courses'
 */
const mockTestErrorWithStack = (): apiInterfaces.IApiFailure => {
  const tmpError = new Error();

  const componentID = '12345';
  const message = 'Mock Error';

  return mockError(componentID, message, tmpError.stack);
};

/**
 * Create a mock error for testing that includes an axios request. Use when need to
 * quickly create a default error with request.
 * @returns Error instance with componentId set to '12345' and error message 'Mock Error'
 * The request method is set to 'GET' and the request url is set to 'http://localhost:3000/courses'
 * The fromAction property is set to 'courseConstants.actions.ALL_COURSES_REQUEST.toString()'
 */
const mockTestErrorWithRequest = (): apiInterfaces.IApiFailure => {
  const componentID = '12345';
  const message = 'Mock Error';

  return errorMocks.mockErrorWithRequest(
    componentID,
    courseConstants.actions.ALL_COURSES_REQUEST.toString(),
    'http://localhost:3000/courses',
    'GET',
    message,
  );
};

/**
 * Create a mock error for testing that includes an axios response. Use when need to
 * quickly create a default error with response.
 * @returns Error instance with componentId set to '12345' and error message 'Mock Error'
 * The request method is set to 'GET' and the request url is set to 'http://localhost:3000/courses'
 * The fromAction property is set to 'courseConstants.actions.ALL_COURSES_REQUEST.toString()'
 */
const mockTestErrorWithResponse = (): apiInterfaces.IApiFailure => {
  const componentID = '12345';
  const message = 'Mock Error';

  return errorMocks.mockErrorWithResponse(
    componentID,
    courseConstants.actions.ALL_COURSES_REQUEST.toString(),
    'http://localhost:3000/courses',
    'GET',
    message,
  );
};

describe('<ApiErrorDialog />', () => {
  let shallowFunc: typeof shallow;
  let mountFunc: typeof mount;

  beforeAll(() => {
    shallowFunc = createShallow();
    mountFunc = createMount();
  });

  test('renders info, open and onClose properties', () => {
    const error = mockTestError();
    const onCloseMock = jest.fn();

    const wrapper = shallowFunc(
      <ApiErrorDialog info={error} open={true} onClose={onCloseMock} />,
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('renders error message', async () => {
    const mockError = mockTestError();
    const onCloseMock = jest.fn();
    const wrapper = mountFunc(
      <TestCaseDialog error={mockError} onClose={onCloseMock} />,
    );

    try {
      await openTestCaseDialog(wrapper);

      const error = findErrorMessage(wrapper, mockError.error.message);
      expect(error.length).toBeGreaterThan(0);
      expect(error.first().text()).toEqual(mockError.error.message);
    } finally {
      wrapper.unmount();
    }
  });

  test('close icon calls onClose property and sets open to false', async () => {
    const mockError = mockTestError();
    const onCloseMock = jest.fn();
    const wrapper = mountFunc(
      <TestCaseDialog error={mockError} onClose={onCloseMock} />,
    );

    try {
      await openTestCaseDialog(wrapper);

      // open state should be true
      const dialog = findTestCaseDialog(wrapper);
      expect(dialog.length).toBeGreaterThan(0);
      expect(dialog.first().prop('open')).toBeTruthy();

      // simulate click on close icon
      const closeIcon = findCloseIcon(wrapper);
      expect(closeIcon.length).toBeGreaterThan(0);
      await act(async () => {
        closeIcon.first().simulate('click');
      });

      // onClose mock should be called
      expect(onCloseMock).toHaveBeenCalled();
    } finally {
      wrapper.unmount();
    }
  });

  test('renders "stack trace unavailable" when there is no value for info.error.stack property', async () => {
    const mockError = mockTestError();
    const onCloseMock = jest.fn();
    const stackTraceUnavailable = 'stack trace unavailable';
    const wrapper = mountFunc(
      <TestCaseDialog error={mockError} onClose={onCloseMock} />,
    );

    try {
      await openTestCaseDialog(wrapper);

      const stacktrace = findStackTrace(wrapper);
      expect(stacktrace.length).toBeGreaterThan(0);
      expect(stacktrace.first().text()).toEqual(stackTraceUnavailable);
    } finally {
      wrapper.unmount();
    }
  });

  test('renders error stacktrace', async () => {
    const mockError = mockTestErrorWithStack();
    const onCloseMock = jest.fn();
    const wrapper = mountFunc(
      <TestCaseDialog error={mockError} onClose={onCloseMock} />,
    );

    try {
      await openTestCaseDialog(wrapper);

      const stacktrace = findStackTrace(wrapper);
      expect(stacktrace.length).toBeGreaterThan(0);
      expect(stacktrace.first().text()).toEqual(mockError.error.stack);
    } finally {
      wrapper.unmount();
    }
  });

  test('renders error source info', async () => {
    const mockError = mockTestErrorWithStack();
    const onCloseMock = jest.fn();
    const wrapper = mountFunc(
      <TestCaseDialog error={mockError} onClose={onCloseMock} />,
    );

    try {
      await openTestCaseDialog(wrapper);

      const sourceAction = findSourceAction(wrapper);
      expect(sourceAction.length).toBeGreaterThan(0);
      expect(sourceAction.first().prop('value')).toEqual(mockError.fromAction);

      const sourceComponent = findSourceComponent(wrapper);
      expect(sourceComponent.length).toBeGreaterThan(0);
      expect(sourceComponent.first().prop('value')).toEqual(
        mockError.request.sourceComponent,
      );
    } finally {
      wrapper.unmount();
    }
  });

  test('renders error request info', async () => {
    const mockError = mockTestErrorWithStack();
    const onCloseMock = jest.fn();
    const wrapper = mountFunc(
      <TestCaseDialog error={mockError} onClose={onCloseMock} />,
    );

    try {
      await openTestCaseDialog(wrapper);
      const requestInfo = findRequestInfo(wrapper);
      expect(requestInfo.length).toBeGreaterThan(0);
      expect(requestInfo.first().prop('value')).toEqual(
        JSON.stringify(mockError.error.config, null, 2),
      );
    } finally {
      wrapper.unmount();
    }
  });

  test('renders error response status text', async () => {
    const mockError = mockTestErrorWithResponse();
    const onCloseMock = jest.fn();
    const wrapper = mountFunc(
      <TestCaseDialog error={mockError} onClose={onCloseMock} />,
    );

    try {
      await openTestCaseDialog(wrapper);

      const responseInfo = findResponseInfo(wrapper);
      expect(responseInfo.length).toBeGreaterThan(0);
      expect(responseInfo.first().prop('value')).toEqual(
        mockError.error.response?.statusText,
      );
    } finally {
      wrapper.unmount();
    }
  });

  test('renders no axios error response if info.error.response is undefined', async () => {
    const mockError = mockTestError();
    const onCloseMock = jest.fn();
    const wrapper = mountFunc(
      <TestCaseDialog error={mockError} onClose={onCloseMock} />,
    );

    try {
      await openTestCaseDialog(wrapper);

      const responseInfo = findResponseInfo(wrapper);
      expect(responseInfo).toHaveLength(0);
    } finally {
      wrapper.unmount();
    }
  });

  test('renders XMLHttpRequestInfo', async () => {
    const mockError = mockTestErrorWithRequest();
    const onCloseMock = jest.fn();
    const wrapper = mountFunc(
      <TestCaseDialog error={mockError} onClose={onCloseMock} />,
    );

    try {
      await openTestCaseDialog(wrapper);

      const xmlHttpRequestInfo = findXmlHttpRequestInfo(wrapper);
      expect(xmlHttpRequestInfo.length).toBeGreaterThan(0);
      expect(xmlHttpRequestInfo.first().prop('value')).toEqual(
        JSON.stringify(mockError.error.request, null, 2),
      );
    } finally {
      wrapper.unmount();
    }
  });
});
