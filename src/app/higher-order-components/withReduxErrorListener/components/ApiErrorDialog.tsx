import React, { useState, useEffect } from 'react';

import Box from '@material-ui/core/Box';
import CloseSharpIcon from '@material-ui/icons/CloseSharp';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid, { GridSpacing } from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import { ErrorDialogProps, ErrorProps } from '../types/types';
import { normaliseError } from '../../../utils';

/**
 * Styles factory function to initialise stylesheet with caption typography
 * of underlying theme
 * @param theme  Properties associated with the current theme
 * @returns  A style factory function that creates styling for TextField to use caption typography
 * @remarks  Styles used for typography variants are available from:
 * https://github.com/mui-org/material-ui/blob/v4.5.2/packages/material-ui/src/styles/createTypography.js#L73
 */
const useStyles = makeStyles(theme => ({
  inputRoot: {
    fontSize: theme.typography.pxToRem(12),
    letterSpacing: '0.03333em',
    lineHeight: 1.66,
  },
}));

/**
 * FunctionComponent to display fromAction and sourceComponent for error.
 * @param props  Api error details accessible from `info` property.
 */
const ErrorRequestInfo: React.FunctionComponent<ErrorProps> = (
  props,
): JSX.Element => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Typography variant="body1" component="span">
        <Box mt={1}>Request Info</Box>
      </Typography>
      <Grid item xs={12}>
        <TextField
          data-testid="error-requestinfo"
          fullWidth
          multiline
          rows="4"
          InputProps={{
            className: classes.inputRoot,
            readOnly: true,
          }}
          value={JSON.stringify(props.info.error.config, null, 2)}
        />
      </Grid>
    </React.Fragment>
  );
};

/**
 * FunctionComponent to display fromAction and sourceComponent for error.
 * @param props  Api error details accessible from `info` property.
 */
const ErrorSourceInfo: React.FunctionComponent<ErrorProps> = (
  props,
): JSX.Element => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Typography variant="body1" component="span">
        <Box mt={1}>Error Source</Box>
      </Typography>
      <Grid container spacing={3 as GridSpacing}>
        <Grid item xs={12} sm={6}>
          <TextField
            data-testid="error-sourceinfo-action"
            label="Action"
            InputProps={{ className: classes.inputRoot, readOnly: true }}
            value={props.info.fromAction}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            data-testid="error-sourceinfo-component"
            label="Component"
            InputProps={{ className: classes.inputRoot, readOnly: true }}
            value={props.info.request.sourceComponent}
            fullWidth
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

/**
 * FunctionComponent to display stack trace for error
 * @param props  Api error details accessible from `info` property.
 */
const ErrorStackTraceInfo: React.FunctionComponent<ErrorProps> = (
  props,
): JSX.Element => {
  const classes = useStyles();
  const [stacktrace, setStackTrace] = useState('');

  useEffect(() => {
    async function triggerNormaliseError(): Promise<void> {
      const stacktrace = await normaliseError(props.info.error);
      setStackTrace(stacktrace);
    }
    triggerNormaliseError();
  }, [props.info.error]);

  return (
    <React.Fragment>
      <Typography variant="body1" component="span">
        <Box mt={1}>Stacktrace</Box>
      </Typography>
      <Grid item xs={12}>
        <TextField
          data-testid="error-stacktrace"
          fullWidth
          multiline
          rows="4"
          InputProps={{
            className: classes.inputRoot,
            readOnly: true,
          }}
          value={stacktrace}
        />
      </Grid>
    </React.Fragment>
  );
};

/**
 * FunctionComponent to display XmlHttpRequestInfo
 * @param props  Api error details accessible from `info` property.
 */
const XMLHttpRequestInfo: React.FunctionComponent<ErrorProps> = (
  props,
): JSX.Element => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Typography variant="body1" component="span">
        <Box mt={1}>XMLHttpRequest Info</Box>
      </Typography>
      <Grid container spacing={3 as GridSpacing}>
        <Grid item xs={12}>
          <TextField
            data-testid="error-xmlhttprequest"
            InputProps={{ className: classes.inputRoot, readOnly: true }}
            value={
              props.info.error.request
                ? JSON.stringify(props.info.error.request, null, 2)
                : 'Unavailable'
            }
            fullWidth
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

/**
 * FunctionComponent to diplay response info for error
 * @param props  Api error details accessible from `info` property.
 */
const ErrorResponseInfo: React.FunctionComponent<ErrorProps> = (
  props,
): JSX.Element => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Typography variant="body1" component="span">
        <Box mt={1}>Http Reponse</Box>
      </Typography>
      <Grid container spacing={3 as GridSpacing}>
        <Grid item xs={12} sm={6}>
          <TextField
            data-testid="error-responseinfo"
            label="Http Response"
            InputProps={{
              className: classes.inputRoot,
              readOnly: true,
            }}
            value={
              props.info.error.response
                ? props.info.error.response.statusText
                : 'Unavailable'
            }
            fullWidth
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

/**
 * Dialog displaying api error details
 * @param props  Error information available in `info` property
 */
export const ApiErrorDialog: React.FunctionComponent<ErrorDialogProps> = (
  props: ErrorDialogProps,
): JSX.Element => {
  const { info, onClose, open } = props;

  return (
    <Dialog
      aria-labelledby="error-dialog-title"
      aria-describedby="error-dialog-description"
      disableBackdropClick={true}
      open={open}
      onClose={onClose}
      scroll="paper"
    >
      <DialogTitle
        disableTypography
        id="error-dialog-title"
        style={{
          backgroundColor: 'lightseagreen',
          color: 'cyan',
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <Typography variant="h6">{info.error.message}</Typography>
        <IconButton aria-label="close" onClick={onClose}>
          <CloseSharpIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <DialogContentText component="div" id="error-dialog-description">
          <Typography
            variant="caption"
            style={{ color: 'midnightblue' }}
            component="span"
          >
            <ErrorSourceInfo {...props} />
            {info.error.response ? (
              <ErrorResponseInfo {...props} />
            ) : (
              <XMLHttpRequestInfo {...props} />
            )}
            <ErrorStackTraceInfo {...props} />
            <ErrorRequestInfo {...props} />
          </Typography>
        </DialogContentText>
      </DialogContent>
    </Dialog>
  );
};
