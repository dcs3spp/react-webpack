import '@testing-library/jest-dom/extend-expect';

import React from 'react';
import { act } from 'react-dom/test-utils';
import { fireEvent } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { mount, shallow, ReactWrapper } from 'enzyme';

import { createMount, createShallow } from '@material-ui/core/test-utils';

import { ApiErrorInfo } from './ApiErrorInfo';
import { apiInterfaces } from '../../../redux/features/api';
import { courseConstants } from '../../../redux/features/course';
import { errorMocks } from '../../../../../test/mocks';
import { reactTesting } from '../../../../../test/utils';

/**
 * Find ApiErrorDialog based on aria-labelledby attribute
 * @param wrapper Enzyme wrapper container to perform search in
 * @returns Wrapped dialog controls with aria-labelledby matching 'alert-dialog-title'
 */
const findApiErrorDialog = (wrapper: ReactWrapper): ReactWrapper => {
  const queryString = '[aria-labelledby="error-dialog-title"]';
  return wrapper.find(queryString);
};

/**
 * Find home navigation control based on aria-label
 * @param wrapper Enzyme wrapper container to perform search in
 * @returns Wrapped home navigation control matching with aria-label matching 'home icon'
 */
const findHome = (wrapper: ReactWrapper): ReactWrapper => {
  const queryString = '[aria-label="home icon"]';
  return wrapper.find(queryString);
};

/**
 * Find learn more icon based on aria-label
 * @param wrapper Enzyme wrapper container to perform search in
 * @returns Wrapped learn more icon with aria-label matching 'learn more icon'
 */
const findLearnMore = (wrapper: ReactWrapper): ReactWrapper => {
  const queryString = '[aria-label="learn more icon"]';
  return wrapper.find(queryString);
};

/**
 * Find the next navigation control based on aria-label
 * @param wrapper Enzyme wrapper container to perform search in
 * @returns Wrapped next navigation control with aria-label matching 'next navigation'
 */
const findNextNav = (wrapper: ReactWrapper): ReactWrapper => {
  const queryString = '[aria-label="next navigation"]';
  return wrapper.find(queryString);
};

/**
 * Find the previous navigation control based on aria-label
 * @param wrapper Enzyme wrapper container to perform search in
 * @returns Wrapped next navigation control with aria-label matching 'previous navigation'
 */
const findPreviousNav = (wrapper: ReactWrapper): ReactWrapper => {
  const queryString = '[aria-label="previous navigation"]';
  return wrapper.find(queryString);
};

/**
 * Create a mock error
 * @param componentId  The component id
 * @param message  The component message
 * @returns Mock error
 */
const mockError = (
  componentId: string,
  message: string,
): apiInterfaces.IApiFailure => {
  const method = 'GET';
  const url = 'http://localhost:3000/courses';

  return errorMocks.mockError(
    componentId,
    courseConstants.actions.ALL_COURSES_REQUEST.toString(),
    url,
    method,
    message,
  );
};

/**
 * Mock a list containing two errors
 * @param componentId  Component id
 * @returns A list containing two errors
 */
const mockErrors = (componentId: string): apiInterfaces.IApiFailure[] => {
  return [
    errorMocks.mockError(
      componentId,
      courseConstants.actions.ALL_COURSES_REQUEST.toString(),
      'http://localhost:3000/courses',
      'GET',
      'Mock Error 1',
    ),
    errorMocks.mockError(
      componentId,
      courseConstants.actions.ALL_COURSES_REQUEST.toString(),
      'http://localhost:3000/courses',
      'GET',
      'Mock Error 2',
    ),
  ];
};

/**
 * Mock an ApiErrorInfo within a memory router
 * @param uniqueId Unique Component id
 * @param errors List of errors to render
 * @returns A memory router that encapsulates an ApiErrorInfo component with
 * componentId and errors properties set accordingly
 * @remarks The following component hierarchy is returned:
 *  <MemoryRouter>
 *     <ApiErrorInfo componentId={uniqueId} errors={errors}></ApiErrorInfo>
 *   </MemoryRouter>
 */
const mockComponent = (
  uniqueId: string,
  errors: apiInterfaces.IApiFailure[],
): JSX.Element => {
  return (
    <MemoryRouter initialEntries={[{ pathname: '/', key: 'testKey' }]}>
      <ApiErrorInfo componentId={uniqueId} errors={errors}></ApiErrorInfo>
    </MemoryRouter>
  );
};

describe('<ApiErrorInfo />', () => {
  let shallowFunc: typeof shallow;
  let mountFunc: typeof mount;

  beforeAll(() => {
    shallowFunc = createShallow();
    mountFunc = createMount();
  });

  test('component renders with errors and componentId properties', () => {
    const componentId = '12345';
    const message = 'MockErrorMessage';

    const error = mockError(componentId, message);
    const wrapper = shallowFunc(mockComponent(componentId, [error]));

    expect(wrapper).toMatchSnapshot();
  });

  test('renders home and learn more buttons, with no navigation buttons when errors property contains only one error', () => {
    const componentId = '12345';
    const homeText = 'Home';
    const learnMoreText = 'Learn More';
    const message = 'MockErrorMessage';

    const error = mockError(componentId, message);
    const wrapper = mountFunc(mockComponent(componentId, [error]));

    try {
      // test requesting action renders
      const requestingAction = wrapper.find({
        children: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
      });
      expect(requestingAction.length).toBeGreaterThan(0);
      expect(requestingAction.first().text()).toEqual(
        courseConstants.actions.ALL_COURSES_REQUEST.toString(),
      );

      // test error message renders
      const err = wrapper.find({ children: message });
      expect(err.length).toBeGreaterThan(0);
      expect(err.first().text()).toEqual(message);

      // test that learn more is rendered
      const learnMore = findLearnMore(wrapper);
      expect(learnMore.length).toBeGreaterThan(0);
      expect(learnMore.first().text()).toEqual(learnMoreText);

      // test that home is rendered
      const home = findHome(wrapper);
      expect(home.length).toBeGreaterThan(0);
      expect(home.first().text()).toEqual(homeText);
    } finally {
      wrapper.unmount();
    }
  });

  test('renders navigation, home and learn more controls when errors property contains > 1 error', () => {
    const componentId = '12345';
    const homeText = 'Home';
    const learnMoreText = 'Learn More';
    const message1 = 'Mock Error 1';

    const errors = mockErrors(componentId);
    const wrapper = mountFunc(mockComponent(componentId, errors));

    try {
      // test requesting action renders
      const requestingAction = wrapper.find({
        children: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
      });
      expect(requestingAction.length).toBeGreaterThan(0);
      expect(requestingAction.first().text()).toEqual(
        courseConstants.actions.ALL_COURSES_REQUEST.toString(),
      );

      // test first error message renders
      const err = wrapper.find({ children: message1 });
      expect(err.length).toBeGreaterThan(0);
      expect(err.first().text()).toEqual(message1);

      // test that home is rendered
      const home = findHome(wrapper);
      expect(home.length).toBeGreaterThan(0);
      expect(home.first().text()).toEqual(homeText);

      // test that learn more is rendered
      const learnMore = findLearnMore(wrapper);
      expect(learnMore.length).toBeGreaterThan(0);
      expect(learnMore.first().text()).toEqual(learnMoreText);

      // test that next navigation is rendered and enabled
      const nextBtn = findNextNav(wrapper);
      expect(nextBtn.length).toBeGreaterThan(0);
      expect(nextBtn.first().prop('disabled')).toEqual(false);

      // test that previous navigation is rendered and disabled
      const previousBtn = findPreviousNav(wrapper);
      expect(previousBtn.length).toBeGreaterThan(0);
      expect(previousBtn.first().prop('disabled')).toEqual(true);
    } finally {
      wrapper.unmount();
    }
  });

  test('renders nothing when errors property contains no errors', () => {
    const componentId = '12345';
    const wrapper = mountFunc(mockComponent(componentId, []));

    try {
      expect(wrapper.children).toHaveLength(1);
      expect(wrapper.render().text()).toEqual('');
    } finally {
      wrapper.unmount();
    }
  });

  test('next navigates to next error', () => {
    const componentId = '12345';
    const message1 = 'Mock Error 1';
    const message2 = 'Mock Error 2';

    const errors = mockErrors(componentId);
    const wrapper = mountFunc(mockComponent(componentId, errors));

    try {
      // test first error message renders
      const err = wrapper.find({ children: message1 });
      expect(err.length).toBeGreaterThan(0);
      expect(err.first().text()).toEqual(message1);

      // simulate next navigation click
      let nextNav = findNextNav(wrapper).first();
      nextNav.simulate('click');

      // test that second error is rendered
      const err2 = wrapper.find({ children: message2 });
      expect(err2.length).toBeGreaterThan(0);
      expect(err2.first().text()).toEqual(message2);

      // test that next navigation control is disabled
      nextNav = findNextNav(wrapper).first();
      expect(nextNav.prop('disabled')).toEqual(true);

      // test that previous navigation is rendered and disabled
      const previousNav = findPreviousNav(wrapper);
      expect(previousNav.length).toBeGreaterThan(0);
      expect(previousNav.first().prop('disabled')).toEqual(false);
    } finally {
      wrapper.unmount();
    }
  });

  test('previous navigates to previous error', () => {
    const componentId = '12345';
    const message1 = 'Mock Error 1';
    const message2 = 'Mock Error 2';

    const errors = mockErrors(componentId);
    const wrapper = mountFunc(mockComponent(componentId, errors));

    try {
      // simulate next navigation click
      const nextNav = findNextNav(wrapper).first();
      nextNav.simulate('click');

      // test that second error message is displayed
      const err2 = wrapper.find({ children: message2 });
      expect(err2.length).toBeGreaterThan(0);
      expect(err2.first().text()).toEqual(message2);

      // simulate previous navigation click
      let previousNav = findPreviousNav(wrapper).first();
      previousNav.simulate('click');

      // test that first error message is rendered
      const err = wrapper.find({ children: message1 });
      expect(err.length).toBeGreaterThan(0);
      expect(err.first().text()).toEqual(message1);

      // test that previous is rendered disabled
      previousNav = findPreviousNav(wrapper).first();
      expect(previousNav.prop('disabled')).toEqual(true);
    } finally {
      wrapper.unmount();
    }
  });

  test('click event for "learn more" opens ApiErrorDialog', async () => {
    const componentId = '12345';
    const message = 'MockErrorMessage';

    const error = mockError(componentId, message);
    const wrapper = mountFunc(mockComponent(componentId, [error]));

    try {
      const learnMore = findLearnMore(wrapper);
      expect(learnMore.length).toBeGreaterThan(0);

      // open the learn more dialog for the error currently rendered
      await act(async () => {
        learnMore.first().simulate('click');
      });

      wrapper.update();

      // test that open state of dialog is now true
      const dialogWrapper = findApiErrorDialog(wrapper);
      expect(dialogWrapper.length).toBeGreaterThan(0);
      expect(dialogWrapper.first().prop('open')).toEqual(true);
    } finally {
      wrapper.unmount();
    }
  });

  test('click event for close icon on ApiErrorDialog triggers open state false', async () => {
    const componentId = '12345';
    const message = 'MockErrorMessage';

    const error = mockError(componentId, message);
    const wrapper = mountFunc(mockComponent(componentId, [error]));

    try {
      // open the learn more dialog for the error currently rendered
      const learnMore = findLearnMore(wrapper);
      expect(learnMore.length).toBeGreaterThan(0);
      await act(async () => {
        learnMore.first().simulate('click');
      });
      wrapper.update();

      // test that open state of dialog is now true
      let dialogWrapper = findApiErrorDialog(wrapper);
      expect(dialogWrapper.length).toBeGreaterThan(0);
      expect(dialogWrapper.first().prop('open')).toEqual(true);

      // trigger click event on close icon
      const dialogCloseIcon = wrapper.find('[aria-label="close"]');
      expect(dialogCloseIcon.length).toBeGreaterThan(0);
      await act(async () => {
        dialogCloseIcon.first().simulate('click');
      });
      wrapper.update();

      dialogWrapper = findApiErrorDialog(wrapper);
      expect(dialogWrapper.first().prop('open')).toEqual(false);
    } finally {
      wrapper.unmount();
    }
  });

  test('home click event redirects to home page', () => {
    const componentId = '12345';
    const message = 'MockErrorMessage';
    const error = mockError(componentId, message);

    // render the component within a router with /courses as current route
    const {
      getByText,
      queryByText,
    } = reactTesting.renderWithRouter(
      <ApiErrorInfo componentId={componentId} errors={[error]} />,
      ['/courses'],
    );

    // test that Home is rendered
    const home = getByText('Home');
    expect(home).toHaveTextContent('Home');

    // test that HOME from '/' route is not rendered
    expect(queryByText('HOME')).not.toBeTruthy();

    // fire a left click event on home
    fireEvent.click(home, { button: 0 });

    // test that '/' is rendered
    expect(queryByText('HOME')).toBeTruthy();
  });
});
