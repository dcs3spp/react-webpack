import React, { useState } from 'react';

import { useHistory } from 'react-router';

import ArrowLeftSharpIcon from '@material-ui/icons/ArrowLeftSharp';
import ArrowRightSharpIcon from '@material-ui/icons/ArrowRightSharp';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import { CardContent } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import ErrorSharpIcon from '@material-ui/icons/ErrorSharp';
import HomeSharpIcon from '@material-ui/icons/HomeSharp';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';

import { ApiErrorDialog } from './ApiErrorDialog';
import { FilteredErrorProps } from '../types/types';
import { IApiFailure } from 'src/app/redux/features/api/interfaces';

const useStyles = makeStyles({
  nav: {
    backgroundColor: 'lightgrey',
    color: 'white',
  },
  actions: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  card: {
    backgroundColor: 'midnightblue',
    maxWidth: 250,
    minWidth: 100,
  },
  container: {
    display: 'flex',
    justifyContent: 'center',
  },
  icon: {
    color: 'red',
  },
  text: {
    color: 'white',
  },
});

/**
 * Render a card that summarises api error info for expansion.
 * @param props  Api error details accessible from `info` property.
 * @returns  Card containing error message and raising action.
 * Buttons are rendered for expanding error details and navigating home.
 */
export const ApiErrorInfo: React.FunctionComponent<FilteredErrorProps> = (
  props: FilteredErrorProps,
): JSX.Element => {
  const errors: IApiFailure[] = props.errors;

  const classes = useStyles();
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const [index, setIndex] = useState(0);

  /** Close handler */
  const onClose = (): void => {
    setOpen(false);
  };

  /**
   * Home button click handler
   */
  const onHome = (): void => {
    history.push('/');
  };

  /**
   * Learn more button click handler
   */
  const onOpen = (): void => {
    setOpen(true);
  };

  /**
   * Next click handler
   */
  const onNext = (): void => {
    console.log(
      `onNext() => array length ${errors.length} :: preop index => ${index}`,
    );
    setIndex(index + 1);
  };

  /**
   * Prev click handler
   */
  const onPrev = (): void => {
    console.log(
      `onPrev() => array length ${errors.length} :: preop index => ${index}`,
    );
    setIndex(index - 1);
  };

  const renderButtons = (): JSX.Element => {
    if (errors.length > 1) {
      return (
        <React.Fragment>
          <IconButton
            aria-label="previous navigation"
            disabled={index === 0}
            onClick={onPrev}
          >
            <ArrowLeftSharpIcon className={classes.nav} />
          </IconButton>
          <IconButton
            aria-label="next navigation"
            disabled={index === errors.length - 1}
            onClick={onNext}
          >
            <ArrowRightSharpIcon className={classes.nav} />
          </IconButton>
        </React.Fragment>
      );
    } else {
      return <React.Fragment></React.Fragment>;
    }
  };

  if (errors.length > 0) {
    return (
      <React.Fragment>
        <Container className={classes.container}>
          <Card className={classes.card}>
            <CardContent>
              <Typography className={classes.text} variant="body1">
                {errors[index].error.message}
              </Typography>
              <Typography className={classes.text} variant="caption">
                {errors[index].fromAction}
              </Typography>
            </CardContent>
            <CardActions className={classes.actions}>
              <Button
                aria-label="learn more icon"
                className={classes.text}
                endIcon={<ErrorSharpIcon className={classes.icon} />}
                onClick={onOpen}
                size="small"
                variant="text"
              >
                Learn More
              </Button>
              <Button
                aria-label="home icon"
                className={classes.text}
                endIcon={<HomeSharpIcon />}
                onClick={onHome}
                size="small"
                variant="text"
              >
                Home
              </Button>
              {renderButtons()}
            </CardActions>
          </Card>
        </Container>
        <ApiErrorDialog info={errors[index]} open={open} onClose={onClose} />
      </React.Fragment>
    );
  } else {
    return <React.Fragment></React.Fragment>;
  }
};
