export type UniqueIdComponentProps = {
  uniqueId: string;
};
