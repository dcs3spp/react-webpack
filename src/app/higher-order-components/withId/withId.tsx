import React, { Component, ComponentType } from 'react';
import cuid from 'cuid';
import { Diff } from 'utility-types';

import { UniqueIdComponentProps } from './types';

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const withId = <BaseProps extends UniqueIdComponentProps>(
  BaseComponent: ComponentType<BaseProps>,
) => {
  /**
   * Type declarations
   */
  type HocProps = Diff<BaseProps, UniqueIdComponentProps>;

  type HocState = {
    readonly uniqueId: string;
  };

  return class IdHoC extends Component<HocProps, HocState> {
    static displayName = `withIdListener(${BaseComponent.name})`;
    static readonly WrappedComponent = BaseComponent;

    readonly state: HocState = {
      uniqueId: String(cuid()),
    };

    /**
     * Inject uniqueId property into base component and passes down original properties
     * @returns Rendered component.
     */
    render(): JSX.Element {
      const { ...restProps } = this.props;
      const { uniqueId } = this.state;
      return (
        <BaseComponent uniqueId={uniqueId} {...(restProps as BaseProps)} />
      );
    }
  };
};
