import { normaliseError } from './index';

/**
 * Throw an error with a given message
 * @param message  The error message
 * @throws Error with supplied message
 */
const throwError = (message: string): Error => {
  throw new Error(message);
};

describe('utils', () => {
  test('normalise error for non production environment without stacktrace', async done => {
    const expectedStackTrace = 'stack trace unavailable';

    const error = new Error('Mock Error');
    error.stack = undefined;

    expect(error.stack).toBeUndefined();

    const normalised = await normaliseError(error);
    expect(normalised).toBeDefined();
    expect(normalised).toEqual(expectedStackTrace);
    done();
  });

  test('normalise error for non production environment with stacktrace', async done => {
    expect.assertions(2);

    try {
      throwError('Mock Error');
    } catch (error) {
      const normalised = await normaliseError(error);
      expect(normalised).toBeDefined();
      expect(normalised).toEqual(error.stack);
    } finally {
      done();
    }
  });

  test('normalise error for production with stacktrace', async done => {
    APP_CONF.mode = 'production';
    const error = new Error('Mock Error');

    // this is trying to connect to a url when use in jsdom environment
    const normalised = await normaliseError(error);
    expect(normalised).toBeDefined();
    expect(normalised).not.toEqual(error.stack);
    done();
  });
});
