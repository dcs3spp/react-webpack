import * as stackTrace from 'stacktrace-js';

/**
 * Normalise an error stack trace for production build
 * @param error  Error instance
 * @returns  If the build is for a production environment then a normalised stacktrace
 * is returned, otherwise the error stacktrace remains unmodified returned.
 * @remarks  Global APP_CONF.mode must be defined with values 'production' or 'development'
 */
export async function normaliseError(error: Error): Promise<string> {
  if (APP_CONF.mode !== 'production') {
    const result: string = error.stack
      ? error.stack
      : 'stack trace unavailable';
    console.log('Logging a stack trace for development environment');
    return result;
  }

  console.log('Logging a stack trace for production environment');
  const stackFrames: Array<stackTrace.StackFrame> = await stackTrace.fromError(
    error,
  );

  const stack: string = stackFrames.reduce((prev, cur) => {
    return prev.toString() + cur.toString() + '\n';
  }, '');

  return stack.trim();
}
