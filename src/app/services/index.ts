import { apiServices } from '../redux/features/api';

export default {
  apiService: apiServices.services.api(),
};
