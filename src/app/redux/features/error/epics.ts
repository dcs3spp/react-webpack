import { Epic } from 'redux-observable';
import { filter, tap, ignoreElements } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import { RootAction, RootState, Services } from 'typesafe-actions';

import { clearErrorAction } from './actions';

export const clearErrorEpic: Epic<
  RootAction,
  RootAction,
  RootState,
  Services
> = action$ =>
  action$.pipe(
    filter(isActionOf(clearErrorAction)),
    tap(action => {
      console.log(
        `clearErrorEpic runnning for clearErrorAction ${action.type}`,
      );
      if (action.payload && action.payload.navigation) {
        console.log(
          `clearErrorEpic redirecting to ${action.payload.navigation.navigateTo}`,
        );
        action.payload.navigation.history.push(
          action.payload.navigation.navigateTo,
        );
      }
    }),
    ignoreElements(),
  );
