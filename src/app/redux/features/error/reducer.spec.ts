import { AxiosError } from 'axios';
import { actions } from './constants';
import { courseConstants } from '../course';
import { ErrorState } from './reducer';
import reducer from './reducer';

describe('error reducers', () => {
  const axiosError: AxiosError = {
    config: {},
    isAxiosError: true,
    message: 'Mock Axios Error',
    name: 'Mock Axios Error',
    toJSON: () => Object,
  };
  const AnotherComponentID = 'Another Component';
  const ComponentID = '1234';
  const GetMethod = 'GET';

  test('clear error action removes an error from redux state', () => {
    const initialState: ErrorState = {
      error: {
        errors: {
          [ComponentID]: {
            errors: {
              [courseConstants.actions.GET_COURSE_REQUEST.toString()]: {
                fromAction: courseConstants.actions.GET_COURSE_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: ComponentID,
                  url: 'http://localhost:3000/course',
                },
                error: axiosError,
              },
            },
            actions: [courseConstants.actions.GET_COURSE_REQUEST.toString()],
          },
          ['Another Component']: {
            errors: {
              [courseConstants.actions.ALL_COURSES_REQUEST.toString()]: {
                fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: AnotherComponentID,
                  url: 'http://localhost:3000/course/14760',
                },
                error: axiosError,
              },
            },
            actions: [courseConstants.actions.ALL_COURSES_SUCCESS.toString()],
          },
        },
        components: [ComponentID, AnotherComponentID],
      },
    };

    const expectedState: ErrorState = {
      error: {
        errors: {
          [AnotherComponentID]: {
            errors: {
              [courseConstants.actions.ALL_COURSES_REQUEST.toString()]: {
                fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: AnotherComponentID,
                  url: 'http://localhost:3000/course/14760',
                },
                error: axiosError,
              },
            },
            actions: [courseConstants.actions.ALL_COURSES_SUCCESS.toString()],
          },
        },
        components: [AnotherComponentID],
      },
    };

    const reduced = reducer(initialState, {
      type: actions.CLEAR_API_ERROR,
      payload: {
        navigation: undefined,
        raisingAction: courseConstants.actions.GET_COURSE_REQUEST.toString(),
        sourceComponent: ComponentID,
      },
    });

    expect(reduced).toEqual(expectedState);
    expect(reduced).not.toBe(expectedState);
    expect(reduced.error).not.toBe(initialState.error);
  });

  test('clear error action does not modify state when the error is not found', () => {
    const initialState: ErrorState = {
      error: {
        errors: {
          [ComponentID]: {
            errors: {
              [courseConstants.actions.GET_COURSE_REQUEST.toString()]: {
                fromAction: courseConstants.actions.GET_COURSE_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: ComponentID,
                  url: 'http://localhost:3000/course',
                },
                error: axiosError,
              },
            },
            actions: [courseConstants.actions.GET_COURSE_REQUEST.toString()],
          },
        },
        components: [ComponentID],
      },
    };

    // attempt to clear an error where component id matches but the from action differs
    const reduced = reducer(initialState, {
      type: actions.CLEAR_API_ERROR,
      payload: {
        navigation: undefined,
        raisingAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
        sourceComponent: ComponentID,
      },
    });

    // test that the object structure is not empty, i.e. it has not been cleared
    // test that object structure is unchanged, i.e. is equal to errors in initialState
    // test that reduer returned initial state instance
    expect(reduced).not.toEqual({ error: [] });
    expect(reduced).toEqual(initialState);
    expect(reduced).toBe(initialState);
    expect(reduced.error).toBe(initialState.error);
  });

  test('clear component errors actions removes all errors associated with a specified component id', () => {
    const initialState: ErrorState = {
      error: {
        errors: {
          [ComponentID]: {
            errors: {
              [courseConstants.actions.GET_COURSE_REQUEST.toString()]: {
                fromAction: courseConstants.actions.GET_COURSE_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: ComponentID,
                  url: 'http://localhost:3000/course',
                },
                error: axiosError,
              },
              [courseConstants.actions.ALL_COURSES_REQUEST.toString()]: {
                fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: ComponentID,
                  url: 'http://localhost:3000/course/141760',
                },
                error: axiosError,
              },
            },
            actions: [courseConstants.actions.GET_COURSE_REQUEST.toString()],
          },
        },
        components: [ComponentID],
      },
    };

    const reduced = reducer(initialState, {
      type: actions.CLEAR_COMPONENT_API_ERRORS,
      payload: {
        sourceComponent: ComponentID,
      },
    });

    // test that no errors from source component `ComponentID`
    // test that object structure is a separate instance from initialState.error
    expect(reduced).toEqual({
      error: { errors: {}, components: [] },
    });
    expect(reduced).not.toBe(initialState);
    expect(reduced.error).not.toBe(initialState.error);
  });

  test('clear component errors actions does not modify state when no errors are associated with component id', () => {
    const AnotherComponent = 'AnotherComponent';

    const initialState: ErrorState = {
      error: {
        errors: {
          ['AnotherComponent']: {
            errors: {
              [courseConstants.actions.ALL_COURSES_SUCCESS.toString()]: {
                fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: AnotherComponent,
                  url: 'http://localhost:3000/course',
                },
                error: axiosError,
              },
            },
            actions: [courseConstants.actions.ALL_COURSES_SUCCESS.toString()],
          },
        },
        components: [AnotherComponent],
      },
    };

    const reduced = reducer(initialState, {
      type: actions.CLEAR_COMPONENT_API_ERRORS,
      payload: {
        sourceComponent: ComponentID,
      },
    });

    // test that object structure remains unmodified if there were no errors found for
    // component id
    expect(reduced).toEqual(initialState);
    expect(reduced).toBe(initialState);
    expect(reduced.error).toBe(initialState.error);
  });

  test('notify error action adds an error for an action and source component that is not already logged in the store', () => {
    const initialState: ErrorState = {
      error: { errors: {}, components: [] },
    };

    const reduced = reducer(initialState, {
      type: actions.NOTIFY_API_ERROR,
      payload: {
        fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
        request: {
          method: GetMethod,
          sourceComponent: ComponentID,
          url: 'http://localhost:3000/course',
        },
        error: axiosError,
      },
    });

    const expectedErrors: ErrorState = {
      error: {
        errors: {
          [ComponentID]: {
            errors: {
              [courseConstants.actions.ALL_COURSES_REQUEST.toString()]: {
                fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: ComponentID,
                  url: 'http://localhost:3000/course',
                },
                error: axiosError,
              },
            },
            actions: [courseConstants.actions.ALL_COURSES_REQUEST.toString()],
          },
        },
        components: [ComponentID],
      },
    };

    expect(reduced).toEqual(expectedErrors);
    expect(reduced).not.toBe(initialState);
    expect(reduced.error).not.toBe(initialState.error);
  });

  test('notify error action adds an error for an action and source component replacing an instance already logged in the store', () => {
    const initialState: ErrorState = {
      error: {
        errors: {
          [ComponentID]: {
            errors: {
              [courseConstants.actions.ALL_COURSES_REQUEST.toString()]: {
                fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: ComponentID,
                  url: 'http://localhost:3000/course',
                },
                error: axiosError,
              },
            },
            actions: [courseConstants.actions.ALL_COURSES_REQUEST.toString()],
          },
        },
        components: [ComponentID],
      },
    };

    const expectedState: ErrorState = {
      error: {
        errors: {
          [ComponentID]: {
            errors: {
              [courseConstants.actions.ALL_COURSES_REQUEST.toString()]: {
                fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: ComponentID,
                  url: 'http://localhost:3000/course',
                },
                error: { ...axiosError, message: 'A different message' },
              },
            },
            actions: [courseConstants.actions.ALL_COURSES_REQUEST.toString()],
          },
        },
        components: [ComponentID],
      },
    };

    const reduced = reducer(initialState, {
      type: actions.NOTIFY_API_ERROR,
      payload: {
        fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
        request: {
          method: GetMethod,
          sourceComponent: ComponentID,
          url: 'http://localhost:3000/course',
        },
        error: { ...axiosError, message: 'A different message' },
      },
    });

    expect(reduced).toEqual(expectedState);
    expect(reduced).not.toBe(initialState);
    expect(reduced.error).not.toBe(initialState.error);
  });

  test('notify error action appends an error associated with an action for a component that is already logged', () => {
    const initialState: ErrorState = {
      error: {
        errors: {
          [ComponentID]: {
            errors: {
              [courseConstants.actions.ALL_COURSES_REQUEST.toString()]: {
                fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: ComponentID,
                  url: 'http://localhost:3000/course',
                },
                error: axiosError,
              },
            },
            actions: [courseConstants.actions.ALL_COURSES_REQUEST.toString()],
          },
        },
        components: [ComponentID],
      },
    };

    const expectedState: ErrorState = {
      error: {
        errors: {
          [ComponentID]: {
            errors: {
              [courseConstants.actions.ALL_COURSES_REQUEST.toString()]: {
                fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: ComponentID,
                  url: 'http://localhost:3000/course',
                },
                error: axiosError,
              },
              [courseConstants.actions.GET_COURSE_REQUEST.toString()]: {
                fromAction: courseConstants.actions.GET_COURSE_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: ComponentID,
                  url: 'http://localhost:3000/course/1014760',
                },
                error: { ...axiosError, message: 'Another error message' },
              },
            },
            actions: [
              courseConstants.actions.ALL_COURSES_REQUEST.toString(),
              courseConstants.actions.GET_COURSE_REQUEST.toString(),
            ],
          },
        },
        components: [ComponentID],
      },
    };

    const reduced = reducer(initialState, {
      type: actions.NOTIFY_API_ERROR,
      payload: {
        fromAction: courseConstants.actions.GET_COURSE_REQUEST.toString(),
        request: {
          method: GetMethod,
          sourceComponent: ComponentID,
          url: 'http://localhost:3000/course/1014760',
        },
        error: { ...axiosError, message: 'Another error message' },
      },
    });

    expect(reduced).toEqual(expectedState);
    expect(reduced).not.toBe(initialState);
    expect(reduced.error).not.toBe(initialState.error);
  });
});
