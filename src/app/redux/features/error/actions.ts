import { createAction } from 'typesafe-actions';

import { actions } from './constants';
import { ClearError, ClearComponentErrors, ErrorNavigation } from './types';

export const clearErrorAction = createAction(
  actions.CLEAR_API_ERROR,
  (
    raisingAction: string,
    sourceComponent: string,
    navigation?: ErrorNavigation,
  ): ClearError => ({
    navigation: navigation ? navigation : undefined,
    raisingAction: raisingAction,
    sourceComponent: sourceComponent,
  }),
)<ClearError>();

export const clearComponentErrorsAction = createAction(
  actions.CLEAR_COMPONENT_API_ERRORS,
  (componentId: string): ClearComponentErrors => ({
    sourceComponent: componentId,
  }),
)<ClearComponentErrors>();
