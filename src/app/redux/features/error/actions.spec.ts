import { createMemoryHistory } from 'history';

import { clearComponentErrorsAction, clearErrorAction } from './actions';
import { actions } from './constants';
import { ErrorNavigation } from './types';

describe('error actions', () => {
  const ComponentID = '1234';
  const FromAction = 'FromAction';

  test('create clear component api errors action', () => {
    const expectedAction = {
      type: actions.CLEAR_COMPONENT_API_ERRORS,
      payload: {
        sourceComponent: ComponentID,
      },
    };

    const action = clearComponentErrorsAction(ComponentID);

    expect(action).toEqual(expectedAction);
  });

  test('create clear error action with no optional arguments set', () => {
    const expectedAction = {
      type: actions.CLEAR_API_ERROR,
      payload: {
        sourceComponent: ComponentID,
        history: undefined,
        navigateTo: undefined,
        raisingAction: FromAction,
      },
    };

    const action = clearErrorAction(FromAction, ComponentID);

    expect(action).toEqual(expectedAction);
  });

  test('create clear action with navigate and history arguments set', () => {
    const navigationObj: ErrorNavigation = {
      history: createMemoryHistory(),
      navigateTo: '/',
    };

    const expectedAction = {
      type: actions.CLEAR_API_ERROR,
      payload: {
        sourceComponent: ComponentID,
        navigation: navigationObj,
        raisingAction: FromAction,
      },
    };

    const action = clearErrorAction(FromAction, ComponentID, navigationObj);

    expect(action).toEqual(expectedAction);
  });
});
