import { DeepReadonly } from 'utility-types';

import { History } from 'history';
import { AxiosError } from 'axios';
import { HttpRequest } from '../api/types';

// export type ErrorReport = {
//   info: Error;
//   raisingAction: string;
//   sourceComponent: string;
// };

export type ClearError = {
  navigation?: ErrorNavigation;
  raisingAction: string;
  sourceComponent: string;
};

export type ClearComponentErrors = {
  sourceComponent: string;
};

export type ErrorNavigation = {
  history: History;
  navigateTo: string;
};

/**
 * For each component errors can be logged by actionid
 * A CLEAR_API_ERROR action removes the error from the store for a component and action
 * A CLEAR_COMPONENT_API_ERRORS action removes all errors for a given source component id
 * A NOTIFY_API_ERROR action adds an error for a given action and source component. An existing
 * error for a given action and source component is replaced
 *
 * {
 *  errors: {
 *    'component1': {
 *      errors: {
 *       '[course] ALL_COURSES_REQUEST': {
 *           fromAction: '[course] ALL_COURSES_REQUEST',
 *           request: {
 *              sourceComponent: 'component1',
 *              method: 'GET',
 *              url: 'http://localhost:3000/course',
 *           }
 *           response: AxiosError,
 *        }
 *      }
 *     ids: '[course] ALL_COURSES_REQUEST'
 *    }
 *  }
 * }
 */
export type ApiFailure = {
  readonly fromAction: string;
  readonly error: DeepReadonly<AxiosError>;
  readonly request: HttpRequest;
};

export type ApiFailureProp = {
  readonly [actionId: string]: ApiFailure;
};

export type ActionFailures = {
  readonly errors: ApiFailureProp;
  readonly actions: string[]; // array of ids
};

export type ComponentErrorProp = {
  readonly [componentId: string]: ActionFailures;
};

export type ComponentErrors = {
  readonly errors: ComponentErrorProp;
  readonly components: string[]; // array of ids
};
