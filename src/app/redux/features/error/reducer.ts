import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';
import produce from 'immer';

import { actions } from './constants';
import { apiInterfaces } from '../api';
import { ClearError, ComponentErrors, ApiFailure } from './types';

/**
 * For each component errors can be logged by actionid
 * A CLEAR_API_ERROR action removes the error from the store for a component and action
 * A CLEAR_COMPONENT_API_ERRORS action removes all errors for a given source component id
 * A NOTIFY_API_ERROR action adds an error for a given action and source component. An existing
 * error for a given action and source component is replaced
 *
 * {
 *  errors: {
 *    'component1': {
 *      errors: {
 *       '[course] ALL_COURSES_REQUEST': {
 *           fromAction: '[course] ALL_COURSES_REQUEST',
 *           request: {
 *              sourceComponent: 'component1',
 *              method: 'GET',
 *              url: 'http://localhost:3000/course',
 *           }
 *           error: AxiosError,
 *        }
 *      }
 *     ids: '[course] ALL_COURSES_REQUEST'
 *    }
 *  }
 * }
 */

const initialState: ComponentErrors = { errors: {}, components: [] };

/**
 * Construct an array of api failures containing a new occurrence
 * @param state  Current component api errors logged in store
 * @param item  An api error to log
 * @returns Renewed state for component api errors
 */
const addError = (
  state: ComponentErrors,
  item: apiInterfaces.IApiFailure,
): ComponentErrors => {
  return produce<ComponentErrors>(state, draft => {
    // add item if it does not exist
    if (
      draft.components.find(
        element => element === item.request.sourceComponent,
      ) === undefined
    ) {
      // assign object to indexer - create the actions array with just it inside
      draft.errors[item.request.sourceComponent] = {
        errors: { [item.fromAction]: item as ApiFailure },
        actions: [item.fromAction],
      };

      // log the id in the component id
      draft.components.push(item.request.sourceComponent);
    } else {
      if (!draft.errors[item.request.sourceComponent].errors[item.fromAction]) {
        // append action to array if action not already present
        draft.errors[item.request.sourceComponent].actions.push(
          item.fromAction,
        );
      }

      draft.errors[item.request.sourceComponent].errors[
        item.fromAction
      ] = item as ApiFailure;
    }

    return draft;
  });
};

/**
 * Remove api failures from the list that originate from a matching componentId
 * @param state  List of current api failures
 * @param componentId  The component id to remove errors for
 * @returns List of api failures with items removed that have matching componentId
 */
const removeComponentErrors = (
  state: ComponentErrors,
  componentId: string,
): ComponentErrors => {
  return produce<ComponentErrors>(state, draft => {
    const indx = draft.components.indexOf(componentId);
    if (indx > -1) {
      draft.components.splice(indx, 1);
    }

    delete draft.errors[componentId];
  });
};

/**
 * Remove an api failure from the list
 * @param state  List of current api failures
 * @param item   The item to remove.
 * @returns Api failures with action failure removed for component
 * @remarks If the item is not found then the state remains unchanged.
 * Once the action property has been removed from the component if the
 * component has no actions remaining then the component property is
 * removed and the component is removed from list of component ids.
 */
const removeError = (
  state: ComponentErrors,
  item: ClearError,
): ComponentErrors => {
  return produce<ComponentErrors>(state, draft => {
    if (!draft.errors[item.sourceComponent].errors[item.raisingAction]) return;

    delete draft.errors[item.sourceComponent].errors[item.raisingAction];

    const index = draft.errors[item.sourceComponent].actions.indexOf(
      item.raisingAction,
    );

    if (index > -1) {
      draft.errors[item.sourceComponent].actions.splice(index, 1);
      if (draft.errors[item.sourceComponent].actions.length === 0) {
        delete draft.errors[item.sourceComponent];
        const index = draft.components.indexOf(item.sourceComponent);
        if (index > -1) {
          draft.components.splice(index, 1);
        }
      }
    }
  });
};

/**
 * Reducer that adds error upon receipt of NOTIFY_API_ERROR action.
 * The reducer removes an error property upon receipt of CLEAR_API_ERROR action.
 * @param initialSate  Initial state of api failures logged for a given component and action
 */
const error = createReducer(initialState as ComponentErrors)
  .handleType(
    actions.NOTIFY_API_ERROR,
    (state, action): ComponentErrors => addError(state, action.payload),
  )
  .handleType(
    actions.CLEAR_API_ERROR,
    (state, action): ComponentErrors => removeError(state, action.payload),
  )
  .handleType(
    actions.CLEAR_COMPONENT_API_ERRORS,
    (state, action): ComponentErrors =>
      removeComponentErrors(state, action.payload.sourceComponent),
  );

const errorsReducer = combineReducers({
  error,
});

/**
 * Exports
 */
export default errorsReducer;
export type ErrorState = ReturnType<typeof errorsReducer>;
