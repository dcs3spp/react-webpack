import { AxiosError } from 'axios';

import { ErrorState } from './reducer';
import { apiInterfaces } from '../api';
import { courseConstants } from '../course';
import { filterErrors } from './selectors';

describe('error selectors', () => {
  const axiosError: AxiosError = {
    config: {},
    isAxiosError: true,
    message: 'Mock Axios Error',
    name: 'Mock Axios Error',
    toJSON: () => Object,
  };

  const GetMethod = 'GET';

  test('error selector filters errors by component id', () => {
    const AnotherComponentID = 'AnotherComponent';
    const ComponentID = '1234';
    const GetMethod = 'GET';

    const state: ErrorState = {
      error: {
        errors: {
          [ComponentID]: {
            errors: {
              [courseConstants.actions.ALL_COURSES_REQUEST.toString()]: {
                fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: ComponentID,
                  url: 'http://localhost:3000/course',
                },
                error: axiosError,
              },
              [courseConstants.actions.GET_COURSE_REQUEST.toString()]: {
                fromAction: courseConstants.actions.GET_COURSE_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: ComponentID,
                  url: 'http://localhost:3000/course/14760',
                },
                error: axiosError,
              },
            },
            actions: [
              courseConstants.actions.ALL_COURSES_REQUEST.toString(),
              courseConstants.actions.GET_COURSE_REQUEST.toString(),
            ],
          },
          [AnotherComponentID]: {
            errors: {
              [courseConstants.actions.ALL_COURSES_REQUEST.toString()]: {
                fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: AnotherComponentID,
                  url: 'http://localhost:3000/course',
                },
                error: axiosError,
              },
            },
            actions: [courseConstants.actions.ALL_COURSES_REQUEST.toString()],
          },
        },
        components: [ComponentID, AnotherComponentID],
      },
    };

    const expectedErrors: apiInterfaces.IApiFailure[] = [
      state.error.errors[state.error.components[0]].errors[
        courseConstants.actions.ALL_COURSES_REQUEST.toString()
      ],
      state.error.errors[state.error.components[0]].errors[
        courseConstants.actions.GET_COURSE_REQUEST.toString()
      ],
    ];

    const actualFiltered: apiInterfaces.IApiFailure[] = filterErrors(state, {
      uniqueId: ComponentID,
    });

    expect(actualFiltered).toEqual(expectedErrors);
    expect(actualFiltered[0]).toBe(
      state.error.errors[state.error.components[0]].errors[
        courseConstants.actions.ALL_COURSES_REQUEST.toString()
      ],
    );
    expect(actualFiltered[1]).toBe(
      state.error.errors[state.error.components[0]].errors[
        courseConstants.actions.GET_COURSE_REQUEST.toString()
      ],
    );
  });

  test('error selector filters errors as an empty list for a component id that is not logged in redux state', () => {
    const LoggedComponentID = '12345';
    const FilteredComponentID = '1234';

    const state: ErrorState = {
      error: {
        errors: {
          [LoggedComponentID]: {
            errors: {
              [courseConstants.actions.GET_COURSE_REQUEST.toString()]: {
                fromAction: courseConstants.actions.GET_COURSE_REQUEST.toString(),
                request: {
                  method: GetMethod,
                  sourceComponent: LoggedComponentID,
                  url: 'http://localhost:3000/course/1014760',
                },
                error: axiosError,
              },
            },
            actions: [courseConstants.actions.GET_COURSE_REQUEST.toString()],
          },
        },
        components: [LoggedComponentID],
      },
    };

    expect(filterErrors(state, { uniqueId: FilteredComponentID })).toHaveLength(
      0,
    );
  });
});
