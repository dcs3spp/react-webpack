import { createMemoryHistory } from 'history';
import { ActionsObservable, StateObservable } from 'redux-observable';
import { RootState } from 'typesafe-actions';
import { Subject } from 'rxjs';
import { toArray } from 'rxjs/operators';

import apiService from '../../../services';
import { clearErrorAction } from './actions';
import { clearErrorEpic } from './epics';
import { ErrorNavigation } from './types';

describe('error epics', () => {
  const ComponentID = '1234';
  const FromAction = 'From Action';

  /**
   * Observable for root state
   */
  let state$: StateObservable<RootState>;

  beforeEach(() => {
    state$ = new StateObservable<RootState>(
      new Subject<RootState>(),
      undefined as any,
    );
  });

  test('clear error action pushes url onto router history object when navigation properties defined on payload', done => {
    // initialise history at location /course
    const history = createMemoryHistory({ initialEntries: ['/course'] });
    const home = '/';

    const navigation: ErrorNavigation = {
      history: history,
      navigateTo: home,
    };

    // here we want to test that the epic is triggered when a CLEAR_ERROR_ACTION is dispatched
    // we need to check that the history is updated with navigation properties contained in action payload
    const clearErrorActionObj = clearErrorAction(
      ComponentID,
      FromAction,
      navigation,
    );

    // spawn epic
    const action$ = ActionsObservable.of(clearErrorActionObj);
    const epic$ = clearErrorEpic(action$, state$, apiService);

    // test that CLEAR_ERROR_ACTION has not dispatched an action and
    // a redirection to '/' was triggered
    epic$.pipe(toArray()).subscribe(actions => {
      console.log('running clear error epic');
      expect(actions.length).toBe(0);

      expect(history.location.pathname).toEqual(home);
      done();
    });
  });

  test('clear error action does not update router history when no navigation properties are set in action payload', done => {
    // initialise history at location /course
    const pathname = '/course';
    const history = createMemoryHistory({ initialEntries: [pathname] });

    // here we want to test that the epic is triggered when a CLEAR_ERROR_ACTION is dispatched
    // we need to check that the history is updated with navigation properties contained in action payload
    const clearErrorActionObj = clearErrorAction(ComponentID, FromAction);

    // spawn epic
    const action$ = ActionsObservable.of(clearErrorActionObj);
    const epic$ = clearErrorEpic(action$, state$, apiService);

    // test that CLEAR_ERROR_ACTION has not dispatched an action and
    // a redirection to '/' was not triggered
    epic$.pipe(toArray()).subscribe(actions => {
      console.log('running clear error epic');
      expect(actions.length).toBe(0);

      expect(history.location.pathname).toEqual(pathname);
      done();
    });
  });
});
