import { ErrorState } from './reducer';
import { apiInterfaces } from '../api';
import { UniqueIdComponentProps } from '../../../higher-order-components/withId';

export const filterErrors = (
  state: ErrorState,
  selectorprops: UniqueIdComponentProps,
): apiInterfaces.IApiFailure[] => {
  console.log(
    `Selector filtering errors for source component ${selectorprops.uniqueId}`,
  );
  const filtered: apiInterfaces.IApiFailure[] = [];

  if (state.error.errors[selectorprops.uniqueId]) {
    const keys = Object.keys(state.error.errors[selectorprops.uniqueId].errors);
    keys.map(key =>
      filtered.push(
        state.error.errors[selectorprops.uniqueId].errors[
          key
        ] as apiInterfaces.IApiFailure,
      ),
    );
  }

  return filtered;
};
