import axios, { AxiosError } from 'axios';

import { Api } from './service';
import { Course } from '../course/model';

const baseURL = 'http://localhost:3000';
const mockCourse: Course = {
  courseName: 'Mock Course',
  courseID: 1,
};

const mockCourses: Course[] = [
  mockCourse,
  { courseName: 'Mock Course 2', courseID: 2 },
];

describe('api service', () => {
  test('all returns list of courses', async () => {
    const axiosMock = axios.create({ baseURL: baseURL });
    axiosMock.request = jest.fn().mockResolvedValue({
      status: 200,
      statusText: 'OK',
      data: mockCourses,
      headers: [],
      config: {},
    });

    const api: Api = new Api(axiosMock);
    api.all('http://localhost:3000/course').subscribe(response => {
      expect(response.status).toEqual(200);
      expect(response.statusText).toEqual('OK');
      expect(response.data.length).toEqual(2);
      expect(response.data).toEqual(mockCourses);
    });
  });

  test('get returns a course', async () => {
    const axiosMock = axios.create({ baseURL: baseURL });
    axiosMock.request = jest.fn().mockResolvedValue({
      status: 200,
      statusText: 'OK',
      data: mockCourse,
      headers: [],
      config: {},
    });

    const api: Api = new Api(axiosMock);
    api.get('http://localhost:3000/course/1').subscribe(response => {
      expect(response.status).toEqual(200);
      expect(response.statusText).toEqual('OK');
      expect(response.data).toEqual(mockCourse);
    });
  });

  test('all throws a network error when fails to connect', done => {
    const api: Api = new Api();
    api.all('http://localhost:3432').subscribe(
      () => {
        done();
      },
      err => {
        expect(err).toBeDefined();
        const axiosErr: AxiosError = err as AxiosError;
        expect(axiosErr.isAxiosError).toBe(true);
        expect(axiosErr.message).toContain('Network Error');
        done();
      },
      () => {
        done();
      },
    );
  });

  test('get throws a network error when fails to connect', done => {
    const api: Api = new Api();
    api.get('http://localhost:3432').subscribe(
      () => {
        done();
      },
      err => {
        expect(err).toBeDefined();
        const axiosErr: AxiosError = err as AxiosError;
        expect(axiosErr.isAxiosError).toBe(true);
        expect(axiosErr.message).toContain('Network Error');
        done();
      },
      () => {
        done();
      },
    );
  });
});
