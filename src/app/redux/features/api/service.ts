import axios, { AxiosResponse, AxiosRequestConfig, AxiosInstance } from 'axios';
import { Observable } from 'rxjs';
import { ITransform } from './interfaces';

/**
 * API Http Method
 * Currently define methods DELETE, GET, PATCH, POST and PUT
 */
type HttpMethod = 'DELETE' | 'GET' | 'PATCH' | 'POST' | 'PUT';

/**
 * Generic wrapper utility class for making an api request using axios
 * - all: retrieve an array of resources, T[];
 * - get: retrieve a resource, T
 */
export class Api {
  private api: AxiosInstance;

  // to facilitate testing allow Axios instance to be injected
  constructor(api: AxiosInstance = axios) {
    this.api = api;
  }

  public all<T, From = T, TF extends ITransform<From, T> = never>(
    url: string,
    transformer?: TF,
  ): Observable<AxiosResponse<T[]>> {
    return this.requestAll<T, From, TF>(url, transformer);
  }

  public get<T, From = T, TF extends ITransform<From, T> = never>(
    url: string,
    transform?: TF,
  ): Observable<AxiosResponse<T>> {
    return this.request<T, From, TF>(url, 'GET', undefined, transform);
  }

  private requestAll<T, From = T, TF extends ITransform<From, T> = never>(
    url: string,
    transformResponse?: TF,
  ): Observable<AxiosResponse<T[]>> {
    const config: AxiosRequestConfig = {
      method: 'GET',
      responseType: 'json',
      transformResponse:
        transformResponse !== undefined
          ? transformResponse.transformFromArray
          : undefined,
      url: url,
    };

    console.log(`Making a request ${JSON.stringify(config)}`);
    const request: Promise<AxiosResponse<T[]>> = this.api.request<T[]>(config);
    return new Observable<AxiosResponse<T[]>>(subscriber => {
      request
        .then(response => {
          subscriber.next(response);
          subscriber.complete();
        })
        .catch(err => {
          subscriber.error(err);
          subscriber.complete();
        });
    });
  }

  private request<T, From = T, TF extends ITransform<From, T> = never>(
    url: string,
    method: HttpMethod,
    data?: T,
    transformResponse?: TF,
  ): Observable<AxiosResponse<T>> {
    const config: AxiosRequestConfig = {
      method: method,
      responseType: 'json',
      transformResponse: transformResponse
        ? transformResponse.transformFrom
        : undefined,
      url: url,
    };

    let request: Promise<AxiosResponse<T>>;

    switch (method) {
      // case 'DELETE':
      case 'GET':
        request = this.api.request<T>({ ...config });
        break;
      // case 'POST':
      //   request = this.api.request<T>({
      //     ...config,
      //     data: data,
      //   });
      //   break;
      // case 'PUT':
      //   request = this.api.request<T>({ ...config, data: data });
      //   break;
      // case 'PATCH':
      //   request = this.api.request<T>({ ...config, data: data });
      //   break;
      default:
        throw new Error(`Method ${method} not supported`);
    }

    // return an observable for the response
    return new Observable<AxiosResponse<T>>(subscriber => {
      request
        .then(response => {
          subscriber.next(response);
          subscriber.complete();
        })
        .catch(err => {
          subscriber.error(err);
          subscriber.complete();
        });
    });
  }
}

/**
 * Factory function to create Api instance for use as epic dependency
 */
export const services = {
  api: (): Api => {
    return new Api();
  },
};
