import { AxiosResponse } from 'axios';

/**
 * Api resource contains data or data[]
 */
export type ApiResponse<T> = AxiosResponse<T> | AxiosResponse<T[]>;

/**
 * An Api request contains the http method, url and source component
 */
export type HttpRequest = {
  readonly method: string;
  readonly sourceComponent: string;
  readonly url: string;
};
