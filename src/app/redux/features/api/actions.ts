import { createAsyncAction } from 'typesafe-actions';

import { HttpRequest, ApiResponse } from './types';
import {
  IApiRequest,
  IApiFailure,
  IApiSuccess,
  ITransform,
} from './interfaces';
import { AxiosError } from 'axios';

/* ------------------- Action Payloads --------------------------- */

export class ApiRequest<T, From = T, TF extends ITransform<From, T> = never>
  implements IApiRequest {
  readonly request: HttpRequest;
  readonly transform?: TF;

  constructor(
    method: string,
    sourceComponent: string,
    url: string,
    transform?: TF,
  ) {
    this.request = {
      method: method,
      sourceComponent: sourceComponent,
      url: url,
    };
    this.transform = transform;
  }
}

/* ------------------- Factory methods --------------------------- */

/**
 * Create an asynchronous action for making an api request.
 * @param requestType  Name of request action
 * @param successType  Name of success action
 * @param failType  Name of fail action
 * @param cancelType  Optional parameter, identifying the name of cancel action
 * @returns Asynchronous action with a generic typed APIRequest, IApiSuccess and IApiFailure payload
 */
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function createApiAction<
  TRequest extends string,
  TSuccess extends string,
  TFail extends string,
  TCancel extends string,
  TModel,
  TNormalised = TModel | undefined,
  TFrom = TModel,
  TTransform extends ITransform<TFrom, TModel> = never
>(
  requestType: TRequest,
  successType: TSuccess,
  failType: TFail,
  cancelType?: TCancel | undefined,
) {
  return createAsyncAction(requestType, successType, failType, cancelType)<
    ApiRequest<TModel, TFrom, TTransform>,
    IApiSuccess<TModel, TNormalised>,
    IApiFailure
  >();
}

/**
 * IApiSuccess payload creator
 * @param response  Response from axios request
 * @param request  The request action payload
 * @param normalised  Optional normalised data for axios response. If present data property
 * of payload is initialised with this state.
 * @returns  A payload containing the response, request with an additional data property. The
 * data property is set to normalised argument value if present, otherwise it is set to
 * the data retrieved in the axios response.
 * @remarks if normalised argument is not given then payload data property is set to the
 * data retrieved in the axios response. Otherwise, payload data property is value of normalised argument
 */
const createSuccessPayload = <T, N = never>(
  response: ApiResponse<T>,
  request: IApiRequest,
  normalised?: N,
): IApiSuccess<T, N> => {
  return {
    response: response,
    request: request,
    data: normalised ? normalised : response.data,
  };
};

/**
 * IApiFailure payload creator
 * @param fromAction The action that raised the failure
 * @param response  The error object containing further information relating to the failure
 * @param request  Contains information relating to the request url, http method and source component
 * that triggered the error
 * @returns Information relating to the failure, including the raising action, source component and error response
 */
const createFailurePayload = (
  fromAction: string,
  error: AxiosError,
  request: HttpRequest,
): IApiFailure => {
  return {
    fromAction: fromAction,
    error: error,
    request: request,
  };
};

/**
 * Exports
 */

/** Export payload creator object with failure and success creator properties */
export const payloadCreate = {
  failure: createFailurePayload,
  success: createSuccessPayload,
};
