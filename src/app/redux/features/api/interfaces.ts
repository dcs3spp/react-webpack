import { AxiosError } from 'axios';
import { DeepReadonly } from 'utility-types';

import { ApiResponse, HttpRequest } from './types';

/**
 * Defines the interface for an API request payload
 *   request:  The method, url and sourceComponent
 */
interface IApiRequestBase {
  request: HttpRequest;
}
export type IApiRequest = DeepReadonly<IApiRequestBase>;

/**
 * API failure response payload
 *  response:         AxiosError object
 *  request:          HttpRequest providing info relating to request that triggered the error
 */
interface IApiFailureBase {
  fromAction: string;
  error: AxiosError;
  request: HttpRequest;
}
export type IApiFailure = DeepReadonly<IApiFailureBase>;

/**
 * Api sucess response payload
 *  response:         AxiosReponse object
 *  request:          IApiRequest providing info relating to request that triggered the response
 *  data:             Normalised Data
 */
export interface IApiSuccess<T, N = never> {
  readonly response: ApiResponse<T>;
  readonly request: IApiRequest;
  readonly data: T | T[] | N;
}

/**
 * Functions for transforming to api client model from api resource
 */
export interface ITransform<From, To> {
  transformFromArray(data: From[]): To[];
  transformFrom(data: From): To;
}

/**
 * Interface for normalising to object structure from client model
 */
export interface INormalise<From, To> {
  normaliseFromArray(data: From[]): To;
  normaliseFrom(data: From): To;
}
