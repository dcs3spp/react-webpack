import { ActionsObservable, StateObservable } from 'redux-observable';
import { AxiosResponse } from 'axios';
import { toArray } from 'rxjs/operators';

import { RootState } from 'typesafe-actions';
import { of, Subject } from 'rxjs';

import { Api } from './service';
import apiService from '../../../services';
import {
  courseConstants,
  courseModels,
  courseTypes,
  courseUtils,
} from '../course';
import { ApiRequest, createApiAction } from './actions';
import { errorConstants } from '../error';
import { getHandler } from './epics';
import { IApiSuccess, IApiFailure, ITransform } from './interfaces';

/**
 * Classes
 */
class TestTransformer
  implements ITransform<courseModels.Course, courseModels.ApiCourse> {
  transformFromArray(data: courseModels.Course[]): courseModels.ApiCourse[] {
    return data.map(item => {
      return { CourseID: item.courseID, CourseName: item.courseName };
    });
  }

  transformFrom(data: courseModels.Course): courseModels.ApiCourse {
    return { CourseID: data.courseID, CourseName: data.courseName };
  }
}

/**
 * Mock Actions
 */
const actionTransformerAndNormaliser = createApiAction<
  courseConstants.actions.GET_COURSE_REQUEST,
  courseConstants.actions.GET_COURSE_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
  string,
  courseModels.ApiCourse,
  courseTypes.Courses,
  courseModels.Course,
  TestTransformer
>(
  courseConstants.actions.GET_COURSE_REQUEST,
  courseConstants.actions.GET_COURSE_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
);

const actionTransformerAndNoNormaliser = createApiAction<
  courseConstants.actions.GET_COURSE_REQUEST,
  courseConstants.actions.GET_COURSE_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
  string,
  courseModels.Course,
  never,
  courseModels.ApiCourse,
  courseUtils.ApiCourseTransform
>(
  courseConstants.actions.GET_COURSE_REQUEST,
  courseConstants.actions.GET_COURSE_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
);

const actionNoTransformerAndNormaliser = createApiAction<
  courseConstants.actions.GET_COURSE_REQUEST,
  courseConstants.actions.GET_COURSE_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
  string,
  courseModels.ApiCourse, // model
  courseTypes.Courses, // normalised
  never,
  never
>(
  courseConstants.actions.GET_COURSE_REQUEST,
  courseConstants.actions.GET_COURSE_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
);

const actionNoTransformerAndNoNormaliser = createApiAction<
  courseConstants.actions.GET_COURSE_REQUEST,
  courseConstants.actions.GET_COURSE_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
  string,
  courseModels.Course,
  never,
  never,
  never
>(
  courseConstants.actions.GET_COURSE_REQUEST,
  courseConstants.actions.GET_COURSE_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
);

/**
 * Epics
 */
const epicNoTransformerAndNoNormaliser = getHandler<
  courseConstants.actions.GET_COURSE_REQUEST,
  courseConstants.actions.GET_COURSE_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
  courseModels.Course,
  never,
  never,
  never,
  never
>(actionNoTransformerAndNoNormaliser);

const epicNoTransformerAndNormaliser = getHandler<
  courseConstants.actions.GET_COURSE_REQUEST,
  courseConstants.actions.GET_COURSE_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
  courseModels.ApiCourse,
  never,
  never,
  courseTypes.Courses,
  courseUtils.CourseNormalise
>(actionNoTransformerAndNormaliser, new courseUtils.CourseNormalise());

const epicTransformerAndNoNormaliser = getHandler<
  courseConstants.actions.GET_COURSE_REQUEST,
  courseConstants.actions.GET_COURSE_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
  courseModels.Course,
  courseModels.ApiCourse,
  courseUtils.ApiCourseTransform,
  never,
  never
>(actionTransformerAndNoNormaliser);

// incompatible with transformer types available
const epicTransformerAndNormaliser = getHandler<
  courseConstants.actions.GET_COURSE_REQUEST,
  courseConstants.actions.GET_COURSE_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
  courseModels.ApiCourse,
  courseModels.Course,
  TestTransformer,
  courseTypes.Courses,
  courseUtils.CourseNormalise
>(actionTransformerAndNormaliser, new courseUtils.CourseNormalise());

describe('epic get tests', () => {
  const ComponentID = '1234';
  const GetMethod = 'GET';
  const Url = 'http://localhost:3000/course';

  let state$: StateObservable<RootState>;
  let transformer: courseUtils.ApiCourseTransform;
  let testTransformer: TestTransformer;

  /**
   * Create an observable of root state
   */
  beforeEach(() => {
    state$ = new StateObservable<RootState>(
      new Subject<RootState>(),
      undefined as any,
    );
  });

  /**
   * Create an ApiCourseTransform for transforming from ApiCourse => Course
   */
  beforeAll(() => {
    transformer = new courseUtils.ApiCourseTransform();
    testTransformer = new TestTransformer();
  });

  test('epic with transformer and no normaliser dispatches success with correct payload', done => {
    const expectedActionRequest = actionTransformerAndNoNormaliser.request(
      new ApiRequest<
        courseModels.Course,
        courseModels.ApiCourse,
        courseUtils.ApiCourseTransform
      >(GetMethod, ComponentID, Url, transformer),
    );
    const action$ = ActionsObservable.of(expectedActionRequest);
    // mock the Api all method to return expected courses
    const allSpy = jest.spyOn<Api, 'get'>(Api.prototype, 'get');
    try {
      // mock a return value for Api.all
      const expectedHttpResponse: AxiosResponse<courseModels.Course> = {
        data: { courseID: 1014760, courseName: 'Course 1' },
        status: 200,
        statusText: 'ok',
        headers: {},
        config: {},
      };

      const expectedHttpResponseObs = of(expectedHttpResponse);
      allSpy.mockReturnValueOnce(expectedHttpResponseObs);
      // create the epic stream
      const epic$ = epicTransformerAndNoNormaliser(action$, state$, apiService);
      // test that success action is dispatched and apiService.all has been called
      // with transformer & correct url
      epic$.pipe(toArray()).subscribe(actions => {
        expect(allSpy).toHaveBeenCalledWith(Url, transformer);
        expect(allSpy).toReturnWith(expectedHttpResponseObs);
        expect(actions.length).toBe(1);
        expect(actions[0].type).toBe(
          courseConstants.actions.GET_COURSE_SUCCESS,
        );
        const payload = actions[0].payload as IApiSuccess<courseModels.Course>;
        expect(payload.request).toBeTruthy();
        expect(payload.request.request).toEqual(
          expectedActionRequest.payload.request,
        );
        expect(payload.response.data).toEqual(expectedHttpResponse.data);
        expect(payload.response.status).toEqual(expectedHttpResponse.status);
        expect(payload.response.statusText).toEqual(
          expectedHttpResponse.statusText,
        );
        expect(payload.data).toEqual(expectedHttpResponse.data);
        done();
      });
    } finally {
      allSpy.mockRestore();
    }
  });

  test('epic with transformer and normaliser set dispatches success with correct payload', done => {
    const expectedActionRequest = actionTransformerAndNormaliser.request(
      new ApiRequest<
        courseModels.ApiCourse,
        courseModels.Course,
        TestTransformer
      >(GetMethod, ComponentID, Url, testTransformer),
    );
    const action$ = ActionsObservable.of(expectedActionRequest);
    // mock the Api all method to return expected courses
    const allSpy = jest.spyOn<Api, 'get'>(Api.prototype, 'get');
    try {
      // mock a return value for Api.all
      const expectedHttpResponse: AxiosResponse<courseModels.ApiCourse> = {
        data: { CourseID: 1014760, CourseName: 'Course 1' },
        status: 200,
        statusText: 'ok',
        headers: {},
        config: {},
      };

      const expectedHttpResponseObs = of(expectedHttpResponse);
      allSpy.mockReturnValueOnce(expectedHttpResponseObs);

      // mock the normalised data property on success payload
      const expectedSuccessData: courseTypes.Courses = {
        coursesById: {
          [expectedHttpResponse.data.CourseID.toString()]: {
            courseID: expectedHttpResponse.data.CourseID,
            courseName: expectedHttpResponse.data.CourseName,
          },
        },
        courseIds: [expectedHttpResponse.data.CourseID],
      };

      // create the epic stream
      const epic$ = epicTransformerAndNormaliser(action$, state$, apiService);
      // test that success action is dispatched and apiService.all has been called
      // with transformer & correct url
      epic$.pipe(toArray()).subscribe(actions => {
        expect(allSpy).toHaveBeenCalledWith(Url, testTransformer);
        expect(allSpy).toReturnWith(expectedHttpResponseObs);
        expect(actions.length).toBe(1);
        expect(actions[0].type).toBe(
          courseConstants.actions.GET_COURSE_SUCCESS,
        );
        const payload = actions[0].payload as IApiSuccess<
          courseModels.ApiCourse,
          courseTypes.Courses
        >;
        expect(payload.request).toBeTruthy();
        expect(payload.request.request).toEqual(
          expectedActionRequest.payload.request,
        );
        expect(payload.response.data).toEqual(expectedHttpResponse.data);
        expect(payload.response.status).toEqual(expectedHttpResponse.status);
        expect(payload.response.statusText).toEqual(
          expectedHttpResponse.statusText,
        );
        expect(payload.data).toEqual(expectedSuccessData);
        done();
      });
    } finally {
      allSpy.mockRestore();
    }
  });

  test('epic with no transformer and no normaliser set dispatches with correct payload', done => {
    const expectedActionRequest = actionNoTransformerAndNoNormaliser.request(
      new ApiRequest<courseModels.Course, never, never>(
        GetMethod,
        ComponentID,
        Url,
      ),
    );
    const action$ = ActionsObservable.of(expectedActionRequest);
    // mock the Api all method to return expected courses
    const allSpy = jest.spyOn<Api, 'get'>(Api.prototype, 'get');
    try {
      // mock a return value for Api.all
      const expectedHttpResponse: AxiosResponse<courseModels.Course> = {
        data: { courseID: 1014760, courseName: 'Course 1' },
        status: 200,
        statusText: 'ok',
        headers: {},
        config: {},
      };

      const expectedHttpResponseObs = of(expectedHttpResponse);
      allSpy.mockReturnValueOnce(expectedHttpResponseObs);
      // create the epic stream
      const epic$ = epicNoTransformerAndNoNormaliser(
        action$,
        state$,
        apiService,
      );
      // test that success action is dispatched and apiService.all has been called
      // with expected data payload & status
      epic$.pipe(toArray()).subscribe(actions => {
        expect(allSpy).toHaveBeenCalledWith(Url, undefined);
        expect(allSpy).toReturnWith(expectedHttpResponseObs);
        expect(actions.length).toBe(1);
        expect(actions[0].type).toBe(
          courseConstants.actions.GET_COURSE_SUCCESS,
        );
        const payload = actions[0].payload as IApiSuccess<courseModels.Course>;
        expect(payload.request).toBeTruthy();
        expect(payload.request.request).toEqual(
          expectedActionRequest.payload.request,
        );
        expect(payload.response.data).toEqual(expectedHttpResponse.data);
        expect(payload.response.status).toEqual(expectedHttpResponse.status);
        expect(payload.response.statusText).toEqual(
          expectedHttpResponse.statusText,
        );
        expect(payload.data).toEqual(expectedHttpResponse.data);
        done();
      });
    } finally {
      allSpy.mockRestore();
    }
  });

  test('epic with no transformer and normaliser set dispatches with correct payload', done => {
    const expectedActionRequest = actionNoTransformerAndNormaliser.request(
      new ApiRequest<courseModels.ApiCourse, never, never>(
        GetMethod,
        ComponentID,
        Url,
      ),
    );
    const action$ = ActionsObservable.of(expectedActionRequest);
    // mock the Api all method to return expected courses
    const allSpy = jest.spyOn<Api, 'get'>(Api.prototype, 'get');
    try {
      // mock a return value for Api.all
      const expectedHttpResponse: AxiosResponse<courseModels.ApiCourse> = {
        data: { CourseID: 1014760, CourseName: 'Course 1' },
        status: 200,
        statusText: 'ok',
        headers: {},
        config: {},
      };

      const expectedHttpResponseObs = of(expectedHttpResponse);
      allSpy.mockReturnValueOnce(expectedHttpResponseObs);

      // mock the normalised data property on success payload
      const expectedSuccessData: courseTypes.Courses = {
        coursesById: {
          [expectedHttpResponse.data.CourseID.toString()]: {
            courseID: expectedHttpResponse.data.CourseID,
            courseName: expectedHttpResponse.data.CourseName,
          },
        },
        courseIds: [expectedHttpResponse.data.CourseID],
      };

      // create the epic stream
      const epic$ = epicNoTransformerAndNormaliser(action$, state$, apiService);
      // test that success action is dispatched and apiService.all has been called
      // with expected data payload & status
      epic$.pipe(toArray()).subscribe(actions => {
        expect(allSpy).toHaveBeenCalledWith(Url, undefined);
        expect(allSpy).toReturnWith(expectedHttpResponseObs);
        expect(actions.length).toBe(1);
        expect(actions[0].type).toBe(
          courseConstants.actions.GET_COURSE_SUCCESS,
        );
        const payload = actions[0].payload as IApiSuccess<
          courseModels.ApiCourse,
          courseTypes.Courses
        >;
        expect(payload.request).toBeTruthy();
        expect(payload.request.request).toEqual(
          expectedActionRequest.payload.request,
        );
        expect(payload.response.data).toEqual(expectedHttpResponse.data);
        expect(payload.response.status).toEqual(expectedHttpResponse.status);
        expect(payload.response.statusText).toEqual(
          expectedHttpResponse.statusText,
        );
        expect(payload.data).toEqual(expectedSuccessData);
        done();
      });
    } finally {
      allSpy.mockRestore();
    }
  });

  test('epic dispatches fail on network error', done => {
    const FailUrl = 'http://localhost:3002/testfail';

    const expectedCourseRequest = actionNoTransformerAndNormaliser.request(
      new ApiRequest<courseModels.ApiCourse, never, never>(
        GetMethod,
        ComponentID,
        FailUrl,
      ),
    );
    const action$ = ActionsObservable.of(expectedCourseRequest);
    const epic$ = epicNoTransformerAndNormaliser(action$, state$, apiService);

    // test that NOTIFY_API_ERROR action is dispatched and IApiFailure payload
    // contains expected request attributes and is an axios error.
    epic$.pipe(toArray()).subscribe(actions => {
      expect(actions.length).toBe(1);
      expect(actions[0].type).toBe(errorConstants.actions.NOTIFY_API_ERROR);

      const payload = actions[0].payload as IApiFailure;
      expect(payload.fromAction).toBe(
        courseConstants.actions.GET_COURSE_REQUEST,
      );

      expect(payload.request).toEqual({
        method: GetMethod,
        sourceComponent: ComponentID,
        url: FailUrl,
      });

      expect(payload.error.isAxiosError).toEqual(true);
      done();
    });
  });
});
