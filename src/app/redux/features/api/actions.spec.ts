import { AxiosResponse, AxiosError } from 'axios';
import { PayloadActionCreator } from 'typesafe-actions';

import { ApiRequest, payloadCreate } from './actions';
import {
  courseConstants,
  courseModels,
  courseTypes,
  courseUtils,
} from '../course';
import { createApiAction } from './actions';
import { errorConstants } from '../error';
import { IApiFailure, IApiRequest, IApiSuccess } from './interfaces';
import { HttpRequest } from './types';

/**
 * Define type alias for course action
 * Use to test if action has request, success, failure
 * and cancel properties of expected types in
 * user-defined typeguard isCourseAction
 */
type CourseAction = {
  request: PayloadActionCreator<
    courseConstants.actions.ALL_COURSES_REQUEST,
    ApiRequest<
      courseModels.Course,
      courseModels.ApiCourse,
      courseUtils.ApiCourseTransform
    >
  >;
  success: PayloadActionCreator<
    courseConstants.actions.ALL_COURSES_SUCCESS,
    IApiSuccess<courseModels.Course>
  >;
  failure: PayloadActionCreator<
    errorConstants.actions.NOTIFY_API_ERROR,
    IApiFailure
  >;
  cancel: string;
};

/**
 * User defined typeguard for determining if action is of a specific Payload action type
 * @param action  Typed PayloadActionCreator instance representing the course action
 * @returns  True if action has fail, request and success properties defined, otherwise returns false
 * @remarks This tests that the action has success, request and failure function properties
 * set. At the time of writing Typescript does not allow runtime check of function
 * signature, other than the length of arguments passed to the function. The
 * test in createApi test block makes test requests to the fail, request and success functions
 * and asserts that they do not throw. This function checks that the fail, request and sucess
 * function properties are defined and of the function type.
 */
function isCourseAction(action: any): action is CourseAction {
  const FunctionType = 'function';

  try {
    // check expected properties defined and of expected type
    if (!action.success || typeof action.success !== FunctionType) {
      return false;
    } else if (!action.request || typeof action.request !== FunctionType) {
      return false;
    } else if (!action.failure || typeof action.failure !== FunctionType) {
      return false;
    }
  } catch (err) {
    return false;
  }

  return true;
}

describe('ApiRequest tests', () => {
  const GetMethod = 'GET';
  const ComponentID = '1234';
  const url = 'http://localhost:3000/course';

  test('ApiRequest constructor initialises with transformer and request properties', () => {
    const transform = new courseUtils.ApiCourseTransform();

    const payload: ApiRequest<
      courseModels.Course,
      courseModels.ApiCourse,
      courseUtils.ApiCourseTransform
    > = new ApiRequest<
      courseModels.Course,
      courseModels.ApiCourse,
      courseUtils.ApiCourseTransform
    >(GetMethod, ComponentID, url, transform);

    expect(payload).toBeDefined();
    expect(payload.transform).toBe(transform);
    expect(payload.transform).toBeInstanceOf(courseUtils.ApiCourseTransform);
    expect(payload.request).toEqual({
      method: GetMethod,
      sourceComponent: ComponentID,
      url: url,
    });
  });

  test('ApiRequest constructor sets request properties and undefined transformer property when no transformer argument in constructor', () => {
    const payload: ApiRequest<
      courseModels.Course,
      courseModels.ApiCourse,
      courseUtils.ApiCourseTransform
    > = new ApiRequest<
      courseModels.Course,
      courseModels.ApiCourse,
      courseUtils.ApiCourseTransform
    >(GetMethod, ComponentID, url);

    expect(payload).toBeDefined();
    expect(payload.transform).toBeUndefined();
    expect(payload.request).toEqual({
      method: GetMethod,
      sourceComponent: ComponentID,
      url: url,
    });
  });
});

describe('createApi tests', () => {
  const ComponentID = '1234';
  const GetMethod = 'GET';
  const Url = 'http://localhost:3000/course';

  test('create an action with normaliser set and transformer set', () => {
    const myType = createApiAction<
      courseConstants.actions.ALL_COURSES_REQUEST,
      courseConstants.actions.ALL_COURSES_SUCCESS,
      errorConstants.actions.NOTIFY_API_ERROR,
      string,
      courseModels.Course,
      courseTypes.Courses,
      courseModels.ApiCourse,
      courseUtils.ApiCourseTransform
    >(
      courseConstants.actions.ALL_COURSES_REQUEST,
      courseConstants.actions.ALL_COURSES_SUCCESS,
      errorConstants.actions.NOTIFY_API_ERROR,
    );

    expect(myType).toBeDefined();

    // check that the request, failure and success types are defined
    expect(isCourseAction(myType)).toEqual(true);

    // now try and call the success method
    const requestPayload: ApiRequest<
      courseModels.Course,
      courseModels.ApiCourse,
      courseUtils.ApiCourseTransform
    > = new ApiRequest<
      courseModels.Course,
      courseModels.ApiCourse,
      courseUtils.ApiCourseTransform
    >(GetMethod, ComponentID, Url, new courseUtils.ApiCourseTransform());

    // fail payload
    const failPayload: IApiFailure = {
      fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
      error: {
        config: {},
        isAxiosError: true,
        message: 'Axios Error',
        name: 'Axios Error',
        toJSON: (): object => Object,
      },
      request: requestPayload.request,
    };

    // success response
    const response: AxiosResponse<courseModels.Course> = {
      data: { courseID: 1015762, courseName: 'Course 2' },
      status: 200,
      statusText: 'ok',
      headers: {},
      config: {},
    };
    const successPayload: IApiSuccess<courseModels.Course> = {
      request: requestPayload,
      response: response,
      data: response.data,
    };

    // success => check that the correct type is returned with the correct properties in payload
    expect(() => {
      myType.success(successPayload);
    }).not.toThrow();
    expect(myType.success(successPayload)).toEqual({
      type: courseConstants.actions.ALL_COURSES_SUCCESS,
      payload: successPayload,
    });

    // request => check that the correct type is returned with the correct properties in payload
    expect(() => {
      myType.request(requestPayload);
    }).not.toThrow();
    expect(myType.request(requestPayload)).toEqual({
      type: courseConstants.actions.ALL_COURSES_REQUEST,
      payload: requestPayload,
    });

    // failure => check that the correct type is returned with the correct properties in payload
    expect(() => {
      myType.failure(failPayload);
    }).not.toThrow();
    expect(myType.failure(failPayload)).toEqual({
      type: errorConstants.actions.NOTIFY_API_ERROR,
      payload: failPayload,
    });
  });

  test('create an action with normaliser set and no transformer set', () => {
    const myType = createApiAction<
      courseConstants.actions.ALL_COURSES_REQUEST,
      courseConstants.actions.ALL_COURSES_SUCCESS,
      errorConstants.actions.NOTIFY_API_ERROR,
      string,
      courseModels.ApiCourse,
      courseTypes.Courses,
      never,
      never
    >(
      courseConstants.actions.ALL_COURSES_REQUEST,
      courseConstants.actions.ALL_COURSES_SUCCESS,
      errorConstants.actions.NOTIFY_API_ERROR,
    );

    expect(myType).toBeDefined();

    // check that the request, failure and success types are defined
    expect(isCourseAction(myType)).toEqual(true);

    // now try and call the success method
    const requestPayload: ApiRequest<courseModels.ApiCourse> = new ApiRequest<
      courseModels.ApiCourse
    >(GetMethod, ComponentID, Url);

    // fail payload
    const failPayload: IApiFailure = {
      fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
      error: {
        config: {},
        isAxiosError: true,
        message: 'Axios Error',
        name: 'Axios Error',
        toJSON: (): object => Object,
      },
      request: requestPayload.request,
    };

    // success response
    const response: AxiosResponse<courseModels.ApiCourse> = {
      data: { CourseID: 1015762, CourseName: 'Course 2' },
      status: 200,
      statusText: 'ok',
      headers: {},
      config: {},
    };
    const successPayload: IApiSuccess<
      courseModels.ApiCourse,
      courseTypes.Courses
    > = {
      request: requestPayload,
      response: response,
      data: {
        courseIds: [1015762],
        coursesById: {
          ['1015762']: {
            courseID: response.data.CourseID,
            courseName: response.data.CourseName,
          },
        },
      },
    };

    // success => check that the correct type is returned with the correct properties in payload
    expect(() => {
      myType.success(successPayload);
    }).not.toThrow();
    expect(myType.success(successPayload)).toEqual({
      type: courseConstants.actions.ALL_COURSES_SUCCESS,
      payload: successPayload,
    });

    // request => check that the correct type is returned with the correct properties in payload
    expect(() => {
      myType.request(requestPayload);
    }).not.toThrow();
    expect(myType.request(requestPayload)).toEqual({
      type: courseConstants.actions.ALL_COURSES_REQUEST,
      payload: requestPayload,
    });

    // failure => check that the correct type is returned with the correct properties in payload
    expect(() => {
      myType.failure(failPayload);
    }).not.toThrow();
    expect(myType.failure(failPayload)).toEqual({
      type: errorConstants.actions.NOTIFY_API_ERROR,
      payload: failPayload,
    });
  });

  test('create an action with normaliser not set and transformed set', () => {
    const myType = createApiAction<
      courseConstants.actions.ALL_COURSES_REQUEST,
      courseConstants.actions.ALL_COURSES_SUCCESS,
      errorConstants.actions.NOTIFY_API_ERROR,
      string,
      courseModels.Course,
      never,
      courseModels.ApiCourse,
      courseUtils.ApiCourseTransform
    >(
      courseConstants.actions.ALL_COURSES_REQUEST,
      courseConstants.actions.ALL_COURSES_SUCCESS,
      errorConstants.actions.NOTIFY_API_ERROR,
    );

    expect(myType).toBeDefined();

    // check that the request, failure and success types are defined
    expect(isCourseAction(myType)).toEqual(true);

    // now try and call the success method
    const requestPayload: ApiRequest<
      courseModels.Course,
      courseModels.ApiCourse,
      courseUtils.ApiCourseTransform
    > = new ApiRequest<
      courseModels.Course,
      courseModels.ApiCourse,
      courseUtils.ApiCourseTransform
    >(
      GetMethod,
      ComponentID.toString(),
      Url,
      new courseUtils.ApiCourseTransform(),
    );

    // fail payload
    const failPayload: IApiFailure = {
      fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
      error: {
        config: {},
        isAxiosError: true,
        message: 'Axios Error',
        name: 'Axios Error',
        toJSON: (): object => Object,
      },
      request: requestPayload.request,
    };

    // success response
    const response: AxiosResponse<courseModels.Course> = {
      data: { courseID: 1015762, courseName: 'Course 2' },
      status: 200,
      statusText: 'ok',
      headers: {},
      config: {},
    };
    const successPayload: IApiSuccess<courseModels.Course> = {
      request: requestPayload,
      response: response,
      data: response.data,
    };

    // success => check that the correct type is returned with the correct properties in payload
    expect(() => {
      myType.success(successPayload);
    }).not.toThrow();
    expect(myType.success(successPayload)).toEqual({
      type: courseConstants.actions.ALL_COURSES_SUCCESS,
      payload: successPayload,
    });

    // request => check that the correct type is returned with the correct properties in payload
    expect(() => {
      myType.request(requestPayload);
    }).not.toThrow();
    expect(myType.request(requestPayload)).toEqual({
      type: courseConstants.actions.ALL_COURSES_REQUEST,
      payload: requestPayload,
    });

    // failure => check that the correct type is returned with the correct properties in payload
    expect(() => {
      myType.failure(failPayload);
    }).not.toThrow();
    expect(myType.failure(failPayload)).toEqual({
      type: errorConstants.actions.NOTIFY_API_ERROR,
      payload: failPayload,
    });
  });

  test('create an action with normaliser not set and transformer not set', () => {
    const myType = createApiAction<
      courseConstants.actions.ALL_COURSES_REQUEST,
      courseConstants.actions.ALL_COURSES_SUCCESS,
      errorConstants.actions.NOTIFY_API_ERROR,
      string,
      courseModels.Course,
      never,
      never,
      never
    >(
      courseConstants.actions.ALL_COURSES_REQUEST,
      courseConstants.actions.ALL_COURSES_SUCCESS,
      errorConstants.actions.NOTIFY_API_ERROR,
    );

    expect(myType).toBeDefined();

    // check that the request, failure and success types are defined
    expect(isCourseAction(myType)).toEqual(true);

    // now try and call the success method
    const requestPayload: ApiRequest<courseModels.Course> = new ApiRequest<
      courseModels.Course
    >(GetMethod, ComponentID, Url);

    // fail payload
    const failPayload: IApiFailure = {
      fromAction: courseConstants.actions.ALL_COURSES_REQUEST.toString(),
      error: {
        config: {},
        isAxiosError: true,
        message: 'Axios Error',
        name: 'Axios Error',
        toJSON: (): object => Object,
      },
      request: requestPayload.request,
    };

    // success response
    const response: AxiosResponse<courseModels.Course> = {
      data: { courseID: 1015762, courseName: 'Course 2' },
      status: 200,
      statusText: 'ok',
      headers: {},
      config: {},
    };
    const successPayload: IApiSuccess<courseModels.Course> = {
      request: requestPayload,
      response: response,
      data: response.data,
    };

    // success => check that the correct type is returned with the correct properties in payload
    expect(() => {
      myType.success(successPayload);
    }).not.toThrow();
    expect(myType.success(successPayload)).toEqual({
      type: courseConstants.actions.ALL_COURSES_SUCCESS,
      payload: successPayload,
    });

    // request => check that the correct type is returned with the correct properties in payload
    expect(() => {
      myType.request(requestPayload);
    }).not.toThrow();
    expect(myType.request(requestPayload)).toEqual({
      type: courseConstants.actions.ALL_COURSES_REQUEST,
      payload: requestPayload,
    });

    // failure => check that the correct type is returned with the correct properties in payload
    expect(() => {
      myType.failure(failPayload);
    }).not.toThrow();
    expect(myType.failure(failPayload)).toEqual({
      type: errorConstants.actions.NOTIFY_API_ERROR,
      payload: failPayload,
    });
  });
});

describe('payload creator tests', () => {
  let mockApiRequest: IApiRequest;
  let mockResponse: AxiosResponse<courseModels.ApiCourse[]>;
  let mockNormalised: courseTypes.Courses;
  let mockError: AxiosError;
  let mockRequest: HttpRequest;

  beforeAll(() => {
    const courses: courseModels.ApiCourse[] = [];
    for (let i = 0; i < 3; i++) {
      courses.push({ CourseID: i, CourseName: `Course ${i}` });
    }

    mockError = {
      message: 'MockError',
      name: 'MockError',
      config: {},
      isAxiosError: true,
      toJSON: (): object => Object,
    };

    mockRequest = {
      method: 'GET',
      sourceComponent: 'Comp1',
      url: 'http://localhost:3000/course',
    };

    mockApiRequest = { request: mockRequest };

    mockResponse = {
      data: courses,
      status: 200,
      statusText: 'Ok',
      headers: {},
      config: {},
    };

    mockNormalised = {
      coursesById: {},
      courseIds: [],
    };

    courses.forEach(course => {
      mockNormalised.coursesById[course.CourseID.toString()] = {
        courseID: course.CourseID,
        courseName: course.CourseName,
      };
      mockNormalised.courseIds.push(course.CourseID);
    });
  });

  test('create failure payload', () => {
    const expectedFailure = {
      error: mockError,
      fromAction: 'fromAction',
      request: mockRequest,
    };

    const failure: IApiFailure = payloadCreate.failure(
      expectedFailure.fromAction,
      mockError,
      mockRequest,
    );

    expect(failure).toEqual(expectedFailure);
  });

  test('create success payload with normalised data', () => {
    const success = payloadCreate.success<
      courseModels.ApiCourse,
      courseTypes.Courses
    >(mockResponse, mockApiRequest, mockNormalised);

    expect(success.data).toEqual(mockNormalised);
    expect(success.request).toEqual(mockApiRequest);
    expect(success.response).toEqual(mockResponse);
  });

  test('create success payload with no nromalised data', () => {
    const success = payloadCreate.success<courseModels.ApiCourse>(
      mockResponse,
      mockApiRequest,
    );

    expect(success.data).toEqual(mockResponse.data);
    expect(success.request).toEqual(mockApiRequest);
    expect(success.response).toEqual(mockResponse);
  });
});
