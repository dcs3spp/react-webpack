import { Epic } from 'redux-observable';
import { of } from 'rxjs';
import {
  AsyncActionCreatorBuilder,
  isActionOf,
  PayloadAction,
} from 'typesafe-actions';
import { catchError, filter, map, switchMap } from 'rxjs/operators';

import { ApiRequest } from './actions';
import { IApiFailure, IApiSuccess, INormalise, ITransform } from './interfaces';
import { RootState, Services } from 'typesafe-actions';
import { payloadCreate } from './actions';

/**
 * Epics
 * allHandler - Issue a request to handle all data request from api.
 * getHandler - Issue a request to handle get data request from api.
 */

/**
 * Create an epic to listen for an api all request and trigger a success or failure action
 * @typeparam TRequestType  Name identifying request action type
 * @typeparam TSuccessType  Name identifying success action type
 * @typeparam TFailType  Name identifying fail action type
 * @typeparam TModel  Type of api request response payload
 * @typeparam TTransformModel  Type of api request response. Defaults to TModel
 * @typeparam TTransform  Type used to transform TTransformModel to/from TModel
 * @typeparam TNormalised  Type representing normalised oject structure of TModel
 * @typeparam TNormaliser  Type used to normalise TModel into TNormalised
 * @param asyncAction  The asynchronous action that the epic should listen for and handle success / failures
 * @param normaliser  Normalises TModel Api response into TNormalised
 * @returns epic for handling all api requests for asyncAction
 */
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const allHandler = <
  TRequestType extends string,
  TSuccessType extends string,
  TFailType extends string,
  TModel,
  TTransformModel = TModel,
  TTransform extends ITransform<TTransformModel, TModel> = never,
  TNormalised = never,
  TNormaliser extends INormalise<TModel, TNormalised> = never
>(
  asyncAction: AsyncActionCreatorBuilder<
    [TRequestType, ApiRequest<TModel, TTransformModel, TTransform>],
    [TSuccessType, IApiSuccess<TModel, TNormalised>],
    [TFailType, IApiFailure]
  >,
  normaliser?: TNormaliser,
) => {
  const epic: Epic<
    | PayloadAction<
        TRequestType,
        ApiRequest<TModel, TTransformModel, TTransform>
      >
    | PayloadAction<TSuccessType, IApiSuccess<TModel, TNormalised>>
    | PayloadAction<TFailType, IApiFailure>,
    | PayloadAction<
        TRequestType,
        ApiRequest<TModel, TTransformModel, TTransform>
      >
    | PayloadAction<TSuccessType, IApiSuccess<TModel, TNormalised>>
    | PayloadAction<TFailType, IApiFailure>,
    RootState,
    Services
  > = (action$, state$, { apiService }) =>
    action$.pipe(
      filter(isActionOf(asyncAction.request)),
      switchMap(action =>
        apiService
          .all<TModel, TTransformModel, TTransform>(
            action.payload.request.url,
            action.payload.transform,
          )
          .pipe(
            map(resp =>
              asyncAction.success(
                payloadCreate.success<TModel, TNormalised>(
                  resp,
                  action.payload,
                  normaliser
                    ? normaliser.normaliseFromArray(resp.data)
                    : undefined,
                ),
              ),
            ),
            catchError(error =>
              of(
                asyncAction.failure(
                  payloadCreate.failure(action.type, error, {
                    url: action.payload.request.url,
                    sourceComponent: action.payload.request.sourceComponent,
                    method: action.payload.request.method,
                  }),
                ),
              ),
            ),
          ),
      ),
    );
  return epic;
};

/**
 * Create an epic to listen for an api get request and trigger a success or failure action
 * @typeparam TRequestType  Name identifying request action type
 * @typeparam TSuccessType  Name identifying success action type
 * @typeparam TFailType  Name identifying fail action type
 * @typeparam TModel  Type of api request response payload
 * @typeparam TTransformModel  Type of api request response. Defaults to TModel
 * @typeparam TTransform  Type used to transform TTransformModel to/from TModel
 * @typeparam TNormalised  Type representing normalised oject structure of TModel
 * @typeparam TNormaliser  Type used to normalise TModel into TNormalised
 * @param asyncAction  The asynchronous action that the epic should listen for and handle success / failures
 * @param normaliser  Normalises TModel Api response into TNormalised
 * @returns epic for handling all api requests for asyncAction
 */
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const getHandler = <
  TRequestType extends string,
  TSuccessType extends string,
  TFailType extends string,
  TModel,
  TTransformModel = TModel,
  TTransform extends ITransform<TTransformModel, TModel> = never,
  TNormalised = never,
  TNormaliser extends INormalise<TModel, TNormalised> = never
>(
  asyncAction: AsyncActionCreatorBuilder<
    [TRequestType, ApiRequest<TModel, TTransformModel, TTransform>],
    [TSuccessType, IApiSuccess<TModel, TNormalised>],
    [TFailType, IApiFailure]
  >,
  normaliser?: TNormaliser,
) => {
  const epic: Epic<
    | PayloadAction<
        TRequestType,
        ApiRequest<TModel, TTransformModel, TTransform>
      >
    | PayloadAction<TSuccessType, IApiSuccess<TModel, TNormalised>>
    | PayloadAction<TFailType, IApiFailure>,
    | PayloadAction<
        TRequestType,
        ApiRequest<TModel, TTransformModel, TTransform>
      >
    | PayloadAction<TSuccessType, IApiSuccess<TModel, TNormalised>>
    | PayloadAction<TFailType, IApiFailure>,
    RootState,
    Services
  > = (action$, state$, { apiService }) =>
    action$.pipe(
      filter(isActionOf(asyncAction.request)),
      switchMap(action =>
        apiService
          .get<TModel, TTransformModel, TTransform>(
            action.payload.request.url,
            action.payload.transform,
          )
          .pipe(
            map(resp =>
              asyncAction.success(
                payloadCreate.success<TModel, TNormalised>(
                  resp,
                  action.payload,
                  normaliser ? normaliser.normaliseFrom(resp.data) : undefined,
                ),
              ),
            ),
            catchError(error =>
              of(
                asyncAction.failure(
                  payloadCreate.failure(action.type, error, {
                    url: action.payload.request.url,
                    sourceComponent: action.payload.request.sourceComponent,
                    method: action.payload.request.method,
                  }),
                ),
              ),
            ),
          ),
      ),
    );
  return epic;
};
