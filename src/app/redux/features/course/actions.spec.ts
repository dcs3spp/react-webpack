import axios, { AxiosResponse, AxiosError } from 'axios';

import { actions } from './constants';
import { allCourseAction, getCourseAction } from './actions';
import { ApiRequest } from '../api/actions';
import { ApiCourse, Course } from './model';
import { IApiRequest } from '../api/interfaces';

/**
 * Create a AxiosError
 * @returns An axios error
 * @remarks Makes a request to http://localhost:8888/thisShouldError
 */
function createMockError(): AxiosError {
  // create default error
  let error: AxiosError = {
    config: {},
    isAxiosError: false,
    message: 'Undefined message => mock',
    name: 'MockError',
    toJSON: () => Object,
  };

  try {
    axios
      .get<Course[]>('http://localhost:8888/thisShouldError')
      .then(() => {
        return;
      })
      .catch(err => (error = err)); //Promise.reject(err));
  } catch (err) {
    error = err;
  } finally {
    return error;
  }
}

describe('all course async action', () => {
  let mockRequestPayload: IApiRequest;

  /**
   * Setup a mock request payload for use within success and fail payload tests
   */
  beforeAll(() => {
    mockRequestPayload = new ApiRequest<ApiCourse>(
      'GET',
      'MyComponent',
      'http://localhost:3000/courses',
    );
  });

  test('request action without transform argument generated with expected type and payload', () => {
    const expectedAction = {
      type: actions.ALL_COURSES_REQUEST,
      payload: {
        request: {
          method: 'GET',
          sourceComponent: 'MyComponent',
          url: 'http://localhost:3000/courses',
        },
      },
    };

    const req = allCourseAction.request(mockRequestPayload);

    expect(req).toEqual(expectedAction);
  });

  test('success action generated with type and payload containing originating request and an axios response', () => {
    const mockData: ApiCourse[] = [
      {
        CourseName: 'Course 1',
        CourseID: 4321,
      },
      { CourseName: 'Course 2', CourseID: 1234 },
    ];

    const mockResponse: AxiosResponse<ApiCourse[]> = {
      config: {},
      data: mockData,
      headers: {},
      status: 200,
      statusText: 'Ok',
    };

    // use spy to mock the response
    const spy = jest.spyOn(axios, 'get');
    spy.mockResolvedValueOnce(mockResponse);

    try {
      // mock success payload
      const successPayload = {
        request: mockRequestPayload,
        response: mockResponse,
        data: mockResponse.data,
      };

      // mock success action
      const expectedSuccessAction = {
        type: allCourseAction.success,
        payload: successPayload,
      };

      // create the success action with the success payload
      const actualSuccessAction = allCourseAction.success(successPayload);

      // test that type and payload properties match
      expect(actualSuccessAction.type.toString()).toEqual(
        expectedSuccessAction.type.toString(),
      );
      expect(actualSuccessAction.payload.request).toEqual(
        expectedSuccessAction.payload.request,
      );
      expect(actualSuccessAction.payload.response).toEqual(mockResponse);
    } finally {
      spy.mockRestore();
    }
  });

  test('fail action generated with type and payload containing originating request and an axios error', () => {
    // create mock error
    const mockError: AxiosError = createMockError();

    // mock failure payload
    const failurePayload = {
      request: mockRequestPayload.request,
      error: mockError,
      fromAction: 'My Action',
    };

    // mock failure action
    const expectedFailureAction = {
      type: allCourseAction.failure,
      payload: failurePayload,
    };

    // create the failure action with the failure payload
    const actualFailureAction = allCourseAction.failure(failurePayload);

    // test that type and payload properties match
    expect(actualFailureAction.type.toString()).toEqual(
      expectedFailureAction.type.toString(),
    );
    expect(actualFailureAction.payload.request).toEqual(
      expectedFailureAction.payload.request,
    );
    expect(actualFailureAction.payload.error).toEqual(
      expectedFailureAction.payload.error,
    );
  });
});

describe('get course async action', () => {
  let mockRequestPayload: IApiRequest;

  /**
   * Setup a mock request payload for use within success and fail payload tests
   */
  beforeAll(() => {
    mockRequestPayload = new ApiRequest<ApiCourse>(
      'GET',
      'MyComponent',
      'http://localhost:3000/courses/1014760',
    );
  });

  test('request action without transform argument generated with expected type and payload', () => {
    const expectedAction = {
      type: actions.GET_COURSE_REQUEST,
      payload: {
        request: {
          method: 'GET',
          sourceComponent: 'MyComponent',
          url: 'http://localhost:3000/courses/1014760',
        },
      },
    };

    const req = getCourseAction.request(mockRequestPayload);

    expect(req).toEqual(expectedAction);
  });

  test('success action generated with type and payload containing originating request and an axios response', () => {
    const mockData: ApiCourse = {
      CourseName: 'Course 1',
      CourseID: 1014760,
    };

    const mockResponse: AxiosResponse<ApiCourse> = {
      config: {},
      data: mockData,
      headers: {},
      status: 200,
      statusText: 'Ok',
    };

    // use spy to mock the response
    const spy = jest.spyOn(axios, 'get');
    spy.mockResolvedValueOnce(mockResponse);

    try {
      // mock success payload
      const successPayload = {
        request: mockRequestPayload,
        response: mockResponse,
        data: mockResponse.data,
      };

      // mock success action
      const expectedSuccessAction = {
        type: getCourseAction.success,
        payload: successPayload,
      };

      // create the success action with the success payload
      const actualSuccessAction = getCourseAction.success(successPayload);

      // test that type and payload properties match
      expect(actualSuccessAction.type.toString()).toEqual(
        expectedSuccessAction.type.toString(),
      );
      expect(actualSuccessAction.payload.request).toEqual(
        expectedSuccessAction.payload.request,
      );
      expect(actualSuccessAction.payload.response).toEqual(mockResponse);
    } finally {
      spy.mockRestore();
    }
  });

  test('fail action generated with type and payload containing originating request and an axios error', () => {
    // create mock error
    const mockError: AxiosError = createMockError();

    // mock failure payload
    const failurePayload = {
      request: mockRequestPayload.request,
      error: mockError,
      fromAction: actions.GET_COURSE_REQUEST,
    };

    // mock failure action
    const expectedFailureAction = {
      type: getCourseAction.failure,
      payload: failurePayload,
    };

    // create the failure action with the failure payload
    const actualFailureAction = getCourseAction.failure(failurePayload);

    // test that type and payload properties match
    expect(actualFailureAction.type.toString()).toEqual(
      expectedFailureAction.type.toString(),
    );
    expect(actualFailureAction.payload.request).toEqual(
      expectedFailureAction.payload.request,
    );
    expect(actualFailureAction.payload.error).toEqual(
      expectedFailureAction.payload.error,
    );
  });
});
