import { ApiCourse } from './model';
import { actions as courseActions } from './constants';
import { createApiAction } from '../api/actions';
import { errorConstants } from '../error';
import { Courses } from './types';

/**
 * export function createApiAction<
  TRequest extends string,
  TSuccess extends string,
  TFail extends string,
  TCancel extends string,
  TModel,
  TFrom = TModel,
  TNormalised = never,
  TTransform extends ITransform<TFrom, TModel> = never
>(
 */
/** all courses request action */
export const allCourseAction = createApiAction<
  courseActions.ALL_COURSES_REQUEST,
  courseActions.ALL_COURSES_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
  string,
  ApiCourse, // model
  Courses, // normalised,
  never, // no transform type
  never // no transformer class
>(
  courseActions.ALL_COURSES_REQUEST,
  courseActions.ALL_COURSES_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
);

/** get course request action */
export const getCourseAction = createApiAction<
  courseActions.GET_COURSE_REQUEST,
  courseActions.GET_COURSE_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
  string,
  ApiCourse,
  Courses
>(
  courseActions.GET_COURSE_REQUEST,
  courseActions.GET_COURSE_SUCCESS,
  errorConstants.actions.NOTIFY_API_ERROR,
);
