import { createSelector } from 'reselect';

import { Course } from './model';
import { Courses } from './types';
import { CoursesState } from './reducer';

const transformCourses = (data: Courses): Course[] => {
  return Object.values(data.coursesById).map(element => ({
    courseID: element.courseID,
    courseName: element.courseName,
  }));
};

export const getCourses = (state: CoursesState): Courses => state.courses;

export const getCoursesTransformed = createSelector(
  getCourses,
  (courses: Courses): Course[] => {
    const transformedData = transformCourses(courses);
    console.log('Selector running for getCoursesTransformed');
    return transformedData;
  },
);

export const getIsLoadingCourses = (state: CoursesState): boolean =>
  state.isLoadingCourses;
