export interface Course {
  readonly courseID: number;
  readonly courseName: string;
}

export type ApiCourse = {
  readonly CourseID: number;
  readonly CourseName: string;
};
