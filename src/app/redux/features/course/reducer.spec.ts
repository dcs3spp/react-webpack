import { AxiosResponse, AxiosError } from 'axios';

import { actions } from './constants';
import { ApiRequest } from '../api/actions';
import { Course, ApiCourse } from './model';
import { Courses } from './types';
import { errorConstants } from '../error';
import { IApiRequest } from '../api/interfaces';
import { mockStore } from '../../../../../test/mocks/store';
import reducer, { CoursesState } from './reducer';
import { requestFactories } from './utils';
import { RootState } from 'typesafe-actions';

describe('course reducers', () => {
  const ComponentID = 'MyComponentID';

  let mockRequestPayload: IApiRequest;

  /**
   * Setup a mock request payload for use within success and fail payload tests
   */
  beforeAll(() => {
    mockRequestPayload = new ApiRequest<Course>(
      'GET',
      'MyComponent',
      'http://localhost:3000/courses',
    );
  });

  test('isLoading reducer is true when request action is dispatched', () => {
    const initialState: CoursesState = {
      isLoadingCourses: false,
      courses: { coursesById: {}, courseIds: [] },
    };

    expect(
      reducer(initialState, {
        type: actions.ALL_COURSES_REQUEST,
        payload: requestFactories.getCourses(ComponentID),
      }),
    ).toEqual({
      isLoadingCourses: true,
      courses: { coursesById: {}, courseIds: [] },
    });
  });

  test('isLoading reducer is false when success action is dispatched and courses property is updated with payload', () => {
    const mockData: Courses = {
      coursesById: {
        ['4321']: { courseName: 'Course 1', courseID: 4321 },
        ['1234']: { courseName: 'Course 2', courseID: 1234 },
      },
      courseIds: [4321, 1234],
    };

    const mockResponse: AxiosResponse<ApiCourse[]> = {
      config: {},
      data: Object.values(mockData.coursesById).map(element => ({
        CourseID: element.courseID,
        CourseName: element.courseName,
      })),
      headers: {},
      status: 200,
      statusText: 'Ok',
    };

    const initialState: CoursesState = {
      isLoadingCourses: true,
      courses: { coursesById: {}, courseIds: [] },
    };

    const expectedState: CoursesState = {
      isLoadingCourses: false,
      courses: {
        coursesById: {
          [mockData.coursesById['4321'].courseID]: {
            courseID: 4321,
            courseName: 'Course 1',
          },
          [mockData.coursesById['1234'].courseID]: {
            courseID: 1234,
            courseName: 'Course 2',
          },
        },
        courseIds: [4321, 1234],
      },
    };

    const reduced = reducer(initialState, {
      type: actions.ALL_COURSES_SUCCESS,
      payload: {
        request: mockRequestPayload,
        response: mockResponse,
        data: mockData,
      },
    });

    expect(reduced).toEqual(expectedState);
    expect(reduced).not.toBe(initialState);
    expect(reduced.courses).not.toBe(initialState.courses);
  });
});

describe('course reducers integrate with error Reducer', () => {
  test('isLoading reducer is false when failure action is dispatched and courses property remains unchanged', () => {
    const initialCoursesState = {
      isLoadingCourses: true,
      courses: { coursesById: {}, courseIds: [] },
    };
    const method = 'GET';
    const url = 'http://localhost:3000/course';
    const sourceComponent = 'Comp';

    const mockError: AxiosError = {
      config: {},
      isAxiosError: true,
      message: 'Mock Error Message',
      name: 'Mock Error',
      toJSON: () => Object,
    };

    const expectedErrorState = {
      error: {
        errors: {
          [sourceComponent]: {
            errors: {
              [actions.ALL_COURSES_REQUEST.toString()]: {
                fromAction: actions.ALL_COURSES_REQUEST.toString(),
                request: {
                  method: method,
                  sourceComponent: sourceComponent,
                  url: url,
                },
                error: mockError,
              },
            },
            actions: [actions.ALL_COURSES_REQUEST.toString()],
          },
        },
        components: [sourceComponent],
      },
    };

    // create a mock store
    const store = mockStore();

    // dispatch a failure action (NOTIFY_API_ERROR) onto the store
    store.dispatch({
      type: errorConstants.actions.NOTIFY_API_ERROR,
      payload: {
        fromAction: actions.ALL_COURSES_REQUEST.toString(),
        request: {
          method: method,
          sourceComponent: sourceComponent,
          url: url,
        },
        error: mockError,
      },
    });

    const state = store.getState() as RootState;
    const reducedCourses = state.courses;
    const reducedError = state.errors;

    // test that isLoadingCourses => if reducer receives failure action then isLoadingCourses = false
    expect(reducedCourses).toEqual({
      ...initialCoursesState,
      isLoadingCourses: false,
    });
    expect(reducedCourses).not.toBe(initialCoursesState);

    // test that the error has been logged to the store
    expect(reducedError).toEqual(expectedErrorState);
  });
});
