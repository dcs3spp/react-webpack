import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';
import { DeepReadonly } from 'utility-types';
import produce from 'immer';

import { allCourseAction } from './actions';
import { ApiCourse } from './model';
import { Courses } from './types';

/**
 * State structure
 *
 * Normalised store of courses
 * This has the scope to be further expanded, e.g. with a property containing the
 * index of the active course
 *
 * {
 *  coursesById: {
 *    '14760': {
 *      courseID: 14760,
 *      courseName: 'Course 1',
 *    },
 *    '12345': {
 *      courseID: 12345,
 *      courseName: 'Course 2',
 *    },
 *  }
 *  courseIds: [14760, 12345]
 * }
 */

/**
 * Initial state
 */
const initialState: Courses = { coursesById: {}, courseIds: [] };

/**
 * Functions
 */

/** Type-guard to determine if axios response payload is course or course[]  */
function isCourses(
  data: DeepReadonly<Courses | ApiCourse | ApiCourse[]>,
): data is Courses {
  const obj: Courses = data as Courses;
  return obj.courseIds !== undefined && obj.coursesById !== undefined;
}

/**
 * Reducers
 */

const isLoadingCourses = createReducer(false as boolean)
  .handleAction([allCourseAction.request], () => true)
  .handleAction(
    [allCourseAction.success, allCourseAction.failure],
    () => false,
  );

const courses = createReducer(initialState).handleAction(
  allCourseAction.success,
  (state, action) => {
    // use negated type-guard to determine if axios response is T[] for returning data
    if (isCourses(action.payload.data)) {
      return produce(state, draft => {
        const data: Courses = action.payload.data as Courses;
        draft.courseIds = data.courseIds;
        draft.coursesById = data.coursesById;
      });
    }
    return state;
  },
);

/**
 * Exports
 */
const coursesReducer = combineReducers({
  isLoadingCourses,
  courses,
});

export default coursesReducer;
export type CoursesState = ReturnType<typeof coursesReducer>;
