import { ActionsObservable, StateObservable } from 'redux-observable';
import { RootState } from 'typesafe-actions';
import { Subject, of } from 'rxjs';
import { toArray } from 'rxjs/operators';

import { AxiosResponse } from 'axios';

import { allCourseAction, getCourseAction } from './actions';
import { allCoursesHandler, getCourseHandler } from './epics';
import apiService from '../../../services';
import { Api } from '../api/service';
import { ApiCourse } from './model';
import { courseConstants } from '.';
import { Courses } from './types';
import { errorConstants } from '../error';
import { IApiFailure, IApiSuccess } from '../api/interfaces';
import { requestFactories } from './utils';

describe('All Course epics', () => {
  /**
   * Observable for root state
   */
  let state$: StateObservable<RootState>;

  /**
   * Create an observable of root state
   */
  beforeEach(() => {
    state$ = new StateObservable<RootState>(
      new Subject<RootState>(),
      undefined as any,
    );
  });

  /**
   * When an allCourseAction.request actions is dispatched and rest API server is running
   * 1. Api service http://localhost:3000/courses
   * 2. API_COURSES_SUCCESS is dispatched on sucess
   * 3. Courses in payload should match those mocked
   */
  test('allCourseAction dispatches success when all courses fetched', done => {
    const expectedCourseRequest = allCourseAction.request(
      requestFactories.getCourses('CompA'),
    );
    const action$ = ActionsObservable.of(expectedCourseRequest);
    // mock the Api all method to return expected courses
    const allSpy = jest.spyOn<Api, 'all'>(Api.prototype, 'all');
    try {
      // mock a return value for Api.all
      const expectedResponse: AxiosResponse<ApiCourse[]> = {
        data: [
          { CourseID: 1014760, CourseName: 'Course 1' },
          { CourseID: 1015762, CourseName: 'Course 2' },
        ],
        status: 200,
        statusText: 'ok',
        headers: {},
        config: {},
      };

      const expectedSuccessData: Courses = {
        coursesById: {
          ['1014760']: { courseID: 1014760, courseName: 'Course 1' },
          ['1015762']: { courseID: 1015762, courseName: 'Course 2' },
        },
        courseIds: [1014760, 1015762],
      };

      const expectedResponseObs = of(expectedResponse);
      allSpy.mockReturnValueOnce(expectedResponseObs);
      // create the epic stream
      const epic$ = allCoursesHandler(action$, state$, apiService);
      // test that success action is dispatched and apiService.all has been called
      // with expected data payload & status
      epic$.pipe(toArray()).subscribe(actions => {
        expect(allSpy).toHaveBeenCalledWith(
          'http://localhost:3000/course',
          undefined,
        );
        expect(allSpy).toReturnWith(expectedResponseObs);
        expect(actions.length).toBe(1);
        expect(actions[0].type).toBe(
          courseConstants.actions.ALL_COURSES_SUCCESS,
        );
        const payload = actions[0].payload as IApiSuccess<ApiCourse, Courses>;
        expect(payload.request).toBeTruthy();
        expect(payload.request.request).toEqual({
          method: 'GET',
          sourceComponent: 'CompA',
          url: 'http://localhost:3000/course',
        });
        expect(payload.response.data).toEqual(expectedResponse.data);
        expect(payload.response.status).toEqual(200);
        expect(payload.response.statusText).toEqual('ok');
        expect(payload.data).toEqual(expectedSuccessData);
        done();
      });
    } finally {
      allSpy.mockRestore();
    }
  });

  /**
   *  When an allCourseAction.request action is dispatched and rest API server is not running
   *  1. NOTIFY_ERROR action is dispatched on fail, e.g. due to network connection error
   *  2. Fail action payload should contain http request url, method and source component id
   *     and should contain axios error response.
   */
  test('apiCourseAction triggers apiCourseAction.fail due to network error', done => {
    const expectedCourseRequest = allCourseAction.request(
      requestFactories.getCourses('CompA'),
    );
    const action$ = ActionsObservable.of(expectedCourseRequest);
    const epic$ = allCoursesHandler(action$, state$, apiService);

    // test that NOTIFY_API_ERROR action is dispatched and IApiFailure payload
    // contains expected request attributes and is an axios error.
    epic$.pipe(toArray()).subscribe(actions => {
      expect(actions.length).toBe(1);
      expect(actions[0].type).toBe(errorConstants.actions.NOTIFY_API_ERROR);

      const payload = actions[0].payload as IApiFailure;
      expect(payload.fromAction).toBe(
        courseConstants.actions.ALL_COURSES_REQUEST,
      );

      expect(payload.request).toEqual({
        method: 'GET',
        sourceComponent: 'CompA',
        url: 'http://localhost:3000/course',
      });

      expect(payload.error.isAxiosError).toEqual(true);
      done();
    });
  });
});

describe('Get Course epics', () => {
  /**
   * Observable for root state
   */
  let state$: StateObservable<RootState>;

  /**
   * Create an observable of root state
   */
  beforeEach(() => {
    state$ = new StateObservable<RootState>(
      new Subject<RootState>(),
      undefined as any,
    );
  });

  test('getCourseAction dispatches success when a course is fetched', done => {
    const CourseID = 1014760;
    const ComponentID = 'GetCourse Action';

    const expectedCourseRequest = getCourseAction.request(
      requestFactories.getCourse(CourseID, ComponentID),
    );

    const action$ = ActionsObservable.of(expectedCourseRequest);

    // mock the Api get method to return expected courses
    const getSpy = jest.spyOn<Api, 'get'>(Api.prototype, 'get');
    try {
      // mock response
      const expectedResponse: AxiosResponse<ApiCourse> = {
        data: { CourseID: CourseID, CourseName: 'Course 1' },
        status: 200,
        statusText: 'ok',
        headers: {},
        config: {},
      };
      const expectedResponseObs = of(expectedResponse);
      getSpy.mockReturnValueOnce(expectedResponseObs);

      // create the epic stream
      const epic$ = getCourseHandler(action$, state$, apiService);

      // test that success action is dispatched and apiService.all has been called
      // with expected data payload & status
      epic$.pipe(toArray()).subscribe(actions => {
        expect(getSpy).toHaveBeenCalledWith(
          `http://localhost:3000/course/${CourseID}`,
          undefined,
        );
        expect(getSpy).toReturnWith(expectedResponseObs);
        expect(actions.length).toBe(1);
        expect(actions[0].type).toBe(
          courseConstants.actions.GET_COURSE_SUCCESS,
        );

        const payload = actions[0].payload as IApiSuccess<ApiCourse, Courses>;
        expect(payload.request.request).toEqual(
          expectedCourseRequest.payload.request,
        );

        expect(payload.response.data as ApiCourse).toEqual(
          expectedResponse.data,
        );
        expect(payload.response.status).toEqual(expectedResponse.status);
        expect(payload.response.statusText).toEqual(
          expectedResponse.statusText,
        );
        expect(payload.data).toEqual({
          coursesById: {
            [CourseID.toString()]: {
              courseID: CourseID,
              courseName: 'Course 1',
            },
          },
          courseIds: [CourseID],
        });
        done();
      });
    } finally {
      getSpy.mockRestore();
    }
  });

  test('getCourseAction triggers a fail action due to network error', done => {
    const CourseID = 1014760;
    const ComponentID = 'GetCourse Action';
    const GetMethod = 'GET';

    const expectedCourseRequest = getCourseAction.request(
      requestFactories.getCourse(CourseID, ComponentID),
    );
    const action$ = ActionsObservable.of(expectedCourseRequest);
    const epic$ = getCourseHandler(action$, state$, apiService);

    // test that NOTIFY_API_ERROR action is dispatched and IApiFailure payload
    // contains expected request attributes and is an axios error.
    epic$.pipe(toArray()).subscribe(actions => {
      expect(actions.length).toBe(1);
      expect(actions[0].type).toBe(errorConstants.actions.NOTIFY_API_ERROR);

      const payload = actions[0].payload as IApiFailure;
      expect(payload.fromAction).toBe(
        courseConstants.actions.GET_COURSE_REQUEST,
      );

      expect(payload.request).toEqual({
        method: GetMethod,
        sourceComponent: ComponentID,
        url: `http://localhost:3000/course/${CourseID}`,
      });

      expect(payload.error.isAxiosError).toEqual(true);
      done();
    });
  });
});
