import { Course } from './model';

/**
 * Types
 *
 * Normalised store of courses
 * This has the scope to be further expanded, e.g. with a property containing the
 * index of the active course
 *
 * {
 *  coursesById: {
 *    '14760': {
 *      courseID: 14760,
 *      courseName: 'Course 1',
 *    },
 *    '12345': {
 *      courseID: 12345,
 *      courseName: 'Course 2',
 *    },
 *  }
 *  courseIds: [14760, 12345]
 *
 * }
 */

type CourseProp = {
  [course: string]: Course;
};

export type Courses = {
  coursesById: CourseProp;
  courseIds: number[];
};
