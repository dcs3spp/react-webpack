import {
  getCourses,
  getIsLoadingCourses,
  getCoursesTransformed,
} from './selectors';
import { CoursesState } from './reducer';
import { Course } from './model';

describe('course selector tests', () => {
  const isLoadingStatus = false;

  let expectedCourses: Course[];
  let mockStoreState: CoursesState;

  beforeAll(() => {
    mockStoreState = {
      courses: {
        coursesById: {
          ['Course 1']: {
            courseID: 1014760,
            courseName: 'Course 1',
          },
          ['Course 2']: {
            courseID: 1014761,
            courseName: 'Course 2',
          },
        },
        courseIds: [1014760, 1014761],
      },
      isLoadingCourses: isLoadingStatus,
    };

    expectedCourses = [
      {
        courseName: 'Course 1',
        courseID: 1014760,
      },
      {
        courseName: 'Course 2',
        courseID: 1014761,
      },
    ];
  });

  test('getCourses returns list of courses', () => {
    const response = getCourses(mockStoreState);
    expect(response).toEqual(mockStoreState.courses);
  });

  test('getCoursesTransformed returns denormalised list of courses', () => {
    const response = getCoursesTransformed(mockStoreState);
    expect(response).toEqual(expectedCourses);
  });

  test('getCoursesIsLoading returns loading status', () => {
    const response = getIsLoadingCourses(mockStoreState);
    expect(response).toEqual(isLoadingStatus);
  });
});
