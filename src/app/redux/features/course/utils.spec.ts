import { ApiCourseTransform, requestFactories, CourseNormalise } from './utils';
import { ApiCourse, Course } from './model';
import { ApiRequest } from '../api/actions';
import { Courses } from './types';

describe('course utils tests', () => {
  let transformer: ApiCourseTransform;

  beforeAll(() => {
    transformer = new ApiCourseTransform();
  });

  test('ApiTransform initialises with default constructor', () => {
    expect(transformer).toBeDefined();
  });

  test('transformFromArray method transforms list into a list of objects of transformed type', () => {
    const mockCourses: ApiCourse[] = [
      { CourseName: 'Course 1', CourseID: 1234 },
      { CourseName: 'Course 2', CourseID: 4321 },
    ];

    const expectedTransformArray: Course[] = [
      { courseName: 'Course 1', courseID: 1234 },
      { courseName: 'Course 2', courseID: 4321 },
    ];

    const actualTransformedArray: Course[] = transformer.transformFromArray(
      mockCourses,
    );

    expect(actualTransformedArray).toEqual(expectedTransformArray);
  });

  test('transformFrom method transforms object into an object of transformed type', () => {
    const mockCourse: ApiCourse = {
      CourseName: 'Course 1',
      CourseID: 1234,
    };

    const expectedTransformedCourse: Course = {
      courseName: 'Course 1',
      courseID: 1234,
    };

    const actualTransformedCourse: Course = transformer.transformFrom(
      mockCourse,
    );

    expect(actualTransformedCourse).toEqual(expectedTransformedCourse);
  });
});

describe('requestFactories tests', () => {
  test('requestFactories is an object that has a getCourses property', () => {
    expect(requestFactories.getCourses).toBeDefined();
    expect(requestFactories.getCourses).toBeDefined();
  });

  test('requestFactories.getCourses creates an ApiRequest action creator instance', () => {
    const expectedRequest = {
      method: 'GET',
      sourceComponent: 'MyComponentID',
      url: 'http://localhost:3000/course',
    };

    const action: ApiRequest<
      ApiCourse,
      ApiCourse,
      never
    > = requestFactories.getCourses('MyComponentID');

    expect(action.request).toEqual(expectedRequest);
    expect(action.transform).not.toBeDefined();
  });

  test('requestFactories.getCourse creates an ApiRequest action creator instance with expected type and payload', () => {
    const CourseID = 1014760;
    const ComponentID = 'MyComponentID';
    const GetMethod = 'GET';

    // mock expected request
    const expectedRequest = {
      method: GetMethod,
      sourceComponent: ComponentID,
      url: `http://localhost:3000/course/${CourseID}`,
    };

    const action: ApiRequest<
      ApiCourse,
      ApiCourse,
      never
    > = requestFactories.getCourse(CourseID, ComponentID);

    expect(action.request).toEqual(expectedRequest);
    expect(action.transform).not.toBeDefined();
  });
});

describe('normaliser tests', () => {
  let normaliser: CourseNormalise;

  beforeAll(() => {
    normaliser = new CourseNormalise();
  });

  test('normalises from an array to a target type', () => {
    const array: ApiCourse[] = [];
    const total = 3;

    for (let i = 0; i < total; ++i) {
      array.push({ CourseID: i, CourseName: `Course ${i}` });
    }

    const expectedNormalised: Courses = {
      coursesById: {},
      courseIds: [],
    };

    array.forEach(item => {
      expectedNormalised.coursesById[`${item.CourseID}`] = {
        courseID: item.CourseID,
        courseName: item.CourseName,
      };
      expectedNormalised.courseIds.push(item.CourseID);
    });

    const normalised: Courses = normaliser.normaliseFromArray(array);

    expect(normalised.coursesById).toEqual(expectedNormalised.coursesById);
    expect(normalised.courseIds).toEqual(expectedNormalised.courseIds);
  });

  test('normalises from a type to a target type', () => {
    const data: ApiCourse = {
      CourseID: 1,
      CourseName: 'Course 1',
    };

    const expectedNormalised: Courses = { coursesById: {}, courseIds: [] };

    expectedNormalised.coursesById[data.CourseID.toString()] = {
      courseID: data.CourseID,
      courseName: data.CourseName,
    };
    expectedNormalised.courseIds.push(data.CourseID);

    const normalised = normaliser.normaliseFrom(data);
    expect(normalised.courseIds).toEqual(expectedNormalised.courseIds);
    expect(normalised.coursesById).toEqual(expectedNormalised.coursesById);
  });
});
