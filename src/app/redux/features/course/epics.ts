import { allCourseAction, getCourseAction } from './actions';
import { CourseNormalise } from './utils';
import { ApiCourse } from './model';
import { apiEpics } from '../api';
import { actions as courseActions } from '../course/constants';
import { actions as errorActions } from '../error/constants';
import { Courses } from './types';

/** create epic middleware to handle all courses request */
export const allCoursesHandler = apiEpics.allHandler<
  courseActions.ALL_COURSES_REQUEST,
  courseActions.ALL_COURSES_SUCCESS,
  errorActions.NOTIFY_API_ERROR,
  ApiCourse,
  never,
  never,
  Courses,
  CourseNormalise
>(allCourseAction, new CourseNormalise());

/** create epic middleware to handle get course request */
export const getCourseHandler = apiEpics.getHandler<
  courseActions.GET_COURSE_REQUEST,
  courseActions.GET_COURSE_SUCCESS,
  errorActions.NOTIFY_API_ERROR,
  ApiCourse,
  never,
  never,
  Courses,
  CourseNormalise
>(getCourseAction, new CourseNormalise());
