import { apiActions } from '../api';
import { ApiCourse, Course } from './model';
import { apiInterfaces } from '../api/';
import { INormalise } from '../api/interfaces';
import { Courses } from './types';

/**
 * Class that transforms from ApiCourse to Course
 */
export class ApiCourseTransform
  implements apiInterfaces.ITransform<ApiCourse, Course> {
  transformFromArray(data: ApiCourse[]): Course[] {
    return data.map(item => ({
      courseName: item.CourseName,
      courseID: item.CourseID,
    }));
  }

  transformFrom(data: ApiCourse): Course {
    return {
      courseName: data.CourseName,
      courseID: data.CourseID,
    } as Course;
  }
}

/**
 * Factory object that creates ApiRequest<Course> payloads for course endpoints
 */
export const requestFactories = {
  getCourses: (sourceComponent: string): apiActions.ApiRequest<ApiCourse> => {
    const url = `${APP_CONF.apiBaseURL}/course`;
    return new apiActions.ApiRequest<ApiCourse>('GET', sourceComponent, url);
  },

  getCourse: (
    courseID: number,
    sourceComponent: string,
  ): apiActions.ApiRequest<ApiCourse> => {
    const url = `${APP_CONF.apiBaseURL}/course/${courseID}`;
    return new apiActions.ApiRequest<ApiCourse>('GET', sourceComponent, url);
  },
};

export class CourseNormalise implements INormalise<ApiCourse, Courses> {
  normaliseFromArray(data: ApiCourse[]): Courses {
    const courses: Courses = { courseIds: [], coursesById: {} };
    data.map(course => {
      courses.coursesById[course.CourseID.toString()] = {
        courseID: course.CourseID,
        courseName: course.CourseName,
      };
      courses.courseIds.push(course.CourseID);
    });

    return courses;
  }

  normaliseFrom(data: ApiCourse): Courses {
    const courses: Courses = { courseIds: [], coursesById: {} };
    courses.coursesById[data.CourseID.toString()] = {
      courseID: data.CourseID,
      courseName: data.CourseName,
    };
    courses.courseIds.push(data.CourseID);

    return courses;
  }
}
