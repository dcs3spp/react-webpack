import * as errorActions from '../features/error/actions';
import * as courseActions from '../features/course/actions';

export default {
  errors: errorActions,
  courses: courseActions,
};
