import { combineEpics } from 'redux-observable';

import * as courseEpics from '../features/course/epics';
import * as errorEpics from '../features/error/epics';

export default combineEpics(
  courseEpics.allCoursesHandler,
  errorEpics.clearErrorEpic,
);
