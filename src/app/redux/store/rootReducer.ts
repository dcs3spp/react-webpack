import { combineReducers } from 'redux';

import coursesReducer from '../features/course/reducer';
import errorsReducer from '../features/error/reducer';

const rootReducer = combineReducers({
  courses: coursesReducer,
  errors: errorsReducer,
});

export default rootReducer;
