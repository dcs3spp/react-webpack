import { compose } from 'redux';

export const composeEnhancers =
  (APP_CONF.mode === 'development' &&
    window &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;
