import React, { Component, ErrorInfo, ReactNode } from 'react';

import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';

import * as stackTrace from 'stacktrace-js';
import { RouteComponentProps, withRouter } from 'react-router';
import { UnregisterCallback } from 'history';

type ErrorBoundaryProps = {};
type ErrorBoundaryState = {
  hasError: boolean;
  error?: Error;
  info?: ErrorInfo;
};

class ErrorBoundary extends Component<
  ErrorBoundaryProps & RouteComponentProps,
  ErrorBoundaryState
> {
  private static homePath = '/';

  private unlisten!: UnregisterCallback;

  constructor(props: ErrorBoundaryProps & RouteComponentProps) {
    super(props);
    this.state = {
      hasError: false,
      error: undefined,
      info: undefined,
    };
    this.goHome = this.goHome.bind(this);
  }

  componentDidMount(): void {
    this.unlisten = this.props.history.listen(() => {
      if (this.state.hasError) {
        this.setState({ hasError: false });
      }
    });
  }

  componentWillUnmount(): void {
    this.unlisten();
  }

  async componentDidCatch(error: Error, info: ErrorInfo): Promise<void> {
    this.setState({
      hasError: true,
      error: APP_CONF.isProd ? await this.normaliseError(error) : error,
      info: info,
    });
  }

  goHome(): void {
    this.props.history.push(ErrorBoundary.homePath);
  }

  render(): JSX.Element | ReactNode {
    if (this.state.hasError) {
      return (
        <Dialog
          open={this.state.hasError}
          onClose={this.goHome}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          scroll="paper"
        >
          <DialogTitle
            disableTypography
            id="alert-dialog-title"
            style={{ backgroundColor: 'lightseagreen', color: 'cyan' }}
          >
            <Typography variant="h6">
              {this.state.error
                ? this.state.error.message
                : 'error message unavailable'}
            </Typography>
          </DialogTitle>
          <DialogContent>
            <DialogContentText component="div" id="alert-dialog-description">
              <Typography
                variant="caption"
                style={{ color: 'midnightblue' }}
                component="span"
              >
                <Box textAlign="justify" mt={1} style={{ whiteSpace: 'pre' }}>
                  {this.state.error
                    ? this.state.error.stack
                    : 'stack trace unavailable'}
                </Box>
                <Box textAlign="justify" mt={1} style={{ whiteSpace: 'pre' }}>
                  {this.state.info && APP_CONF.mode !== 'production'
                    ? this.state.info.componentStack
                    : 'component stack unavailable in production'}
                  <br />
                </Box>
              </Typography>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.goHome}>Home</Button>
          </DialogActions>
        </Dialog>
      );
    }

    return this.props.children;
  }

  private async normaliseError(error: Error): Promise<Error> {
    const stackFrames: Array<stackTrace.StackFrame> = await stackTrace.fromError(
      error,
    );

    const stack: string = stackFrames.reduce((prev, cur) => {
      return prev.toString() + cur.toString() + '\n';
    }, '');

    return {
      name: error.name,
      message: error.message,
      stack: stack.trim(),
    };
  }
}

export default withRouter(ErrorBoundary);
