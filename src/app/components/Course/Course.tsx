import React, { Component, ReactFragment } from 'react';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import { Course as CourseModel } from '../../redux/features/course/model';

interface CourseProps {
  course: CourseModel;
}

export default class Course extends Component<CourseProps, {}> {
  constructor(props: CourseProps) {
    super(props);
  }

  public render(): ReactFragment {
    return (
      <div>
        <Card role="listitem">
          <CardActionArea>
            <CardContent>
              <Typography
                aria-label="course name"
                gutterBottom
                variant="h5"
                component="h2"
              >
                {this.props.course.courseName}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </div>
    );
  }
}
