import React from 'react';
import { shallow, mount, ReactWrapper } from 'enzyme';

import { createMount, createShallow } from '@material-ui/core/test-utils';

import Course from './Course';
import { Course as CourseModel } from '../../redux/features/course/model';

/**
 * Locate an element describing coursename via it's aria-label
 * @param wrapper The wrapper to perform search inside
 * @returns Result of wrapper find operation
 * @remarks The find selector used is '[aria-label="course name]'
 */
const findCourse = (wrapper: ReactWrapper): ReactWrapper => {
  return wrapper.find('[aria-label="course name"]');
};

describe('Course Component', () => {
  let shallowFunc: typeof shallow;
  let mountFunc: typeof mount;

  beforeAll(() => {
    shallowFunc = createShallow();
    mountFunc = createMount();
  });

  test('renders with course property accessible', () => {
    const course: CourseModel = { courseID: 1014760, courseName: 'Course 1' };

    const wrapper = shallowFunc<Course>(<Course course={course} />);

    const courseInstance = wrapper.instance();
    expect(courseInstance.props.course).toBeDefined();

    const courseProp: CourseModel = courseInstance.props.course;
    expect(courseProp.courseID).toEqual(course.courseID);
    expect(courseProp.courseName).toEqual(course.courseName);

    expect(wrapper).toMatchSnapshot();
  });

  test('renders courseName property on screen', () => {
    const course: CourseModel = { courseID: 1014760, courseName: 'Course 1' };

    const wrapper = mountFunc(<Course course={course} />);

    try {
      const courseName = findCourse(wrapper);
      expect(courseName.length).toBeGreaterThan(0);
      expect(courseName.first().text()).toEqual(course.courseName);
    } finally {
      wrapper.unmount();
    }
  });
});
