import React, { Component, Suspense, lazy } from 'react';

import { BrowserRouter, Route, Switch } from 'react-router-dom';

import { NavBar } from './NavBar/NavBar';

/**
 * Lazy load definitions
 */
const About = lazy(() =>
  import(
    /*
  webpackChunkName: "about-page",
  webpackPrefetch: true
  */ '../views/About/About'
  ),
);

const CourseList = lazy(() =>
  import(
    /*
  webpackChunkName: "course-list",
  webpackPrefetch: true
  */ '../containers/CourseList'
  ),
);

const Home = lazy(() =>
  import(
    /*
webpackChunkName: "home-page",
webpackPrefetch: true
*/ '../views/Home/Home'
  ),
);

type AppProps = {};

export class App extends Component<AppProps, {}> {
  public render(): JSX.Element {
    return (
      <BrowserRouter>
        <div>
          <NavBar />
          <Suspense fallback={<div>Loading page...</div>}>
            <Switch>
              <Route exact path="/" component={Home}></Route>
              <Route exact path="/about" component={About}></Route>
              <Route
                exact
                path="/courses"
                render={(props): JSX.Element => <CourseList {...props} />}
              ></Route>
            </Switch>
          </Suspense>
        </div>
      </BrowserRouter>
    );
  }
}
