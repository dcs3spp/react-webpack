import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, screen } from '@testing-library/react';

import React from 'react';
import { Route, RouteProps } from 'react-router-dom';
import { shallow, ShallowWrapper } from 'enzyme';

import { createShallow } from '@material-ui/core/test-utils';

import { App } from './App';
import { reactTesting } from '../../../test/utils';

describe('<App/>', () => {
  let shallowFunc: typeof shallow;

  beforeAll(() => {
    shallowFunc = createShallow();
  });

  test('App renders', () => {
    const wrapper = shallowFunc<App>(<App />);
    const routes = wrapper.find<Route>(Route);

    expect(routes).toBeDefined();
    expect(routes).toHaveLength(3);

    const pathMap: {
      [x: string]:
        | React.LazyExoticComponent<() => JSX.Element>
        | React.ReactNode;
    } = routes.reduce<{
      [x: string]:
        | React.LazyExoticComponent<() => JSX.Element>
        | React.ReactNode;
    }>((pathMap, route: ShallowWrapper<Route<RouteProps>>) => {
      const routeProps = route.props() as Readonly<RouteProps> &
        Readonly<{
          children?: React.ReactNode;
        }>;

      if (routeProps.path && routeProps.component) {
        pathMap[
          routeProps.path as string
        ] = routeProps.component as React.LazyExoticComponent<
          () => JSX.Element
        >;
      } else if (routeProps.path && routeProps.render) {
        pathMap[routeProps.path as string] = routeProps.render;
      }
      return pathMap;
    }, {});
    expect(pathMap['/about']).toBeDefined();
    expect(pathMap['/']).toBeDefined();
    expect(pathMap['/courses']).toBeDefined();
  });

  test('renders home page by default', async () => {
    render(<App />);

    const home = screen.queryByText(/Home/);
    expect(home).toBeTruthy();
    expect(home).toBeInTheDocument();
  });

  test('renders fallback in test environment when click About on navbar', () => {
    const wrapper = render(<App />);

    // fallback should not be rendered
    expect(wrapper.queryByText('Loading page...')).not.toBeTruthy();

    // simulate click event on Navbar
    const allAbouts = wrapper.getAllByText('About');
    expect(allAbouts.length).toBeGreaterThan(0);
    fireEvent.click(allAbouts[0], { button: 0 });

    // fallback should render
    const loading = screen.queryByText(/Loading page.../);
    expect(loading).toBeTruthy();
    expect(loading).toBeInTheDocument();
  });

  test('renders fallback in test environment when click Courses on navbar', () => {
    const wrapper = reactTesting.renderWithRedux(<App />);

    // fallback should not be rendered
    expect(wrapper.queryByText('Loading page...')).not.toBeTruthy();

    // simulate click event on Navbar
    const allCourses = wrapper.getAllByText('Courses');
    expect(allCourses.length).toBeGreaterThan(0);
    fireEvent.click(allCourses[0], { button: 0 });

    // fallback should render
    const loading = screen.queryByText(/Loading page.../);
    expect(loading).toBeTruthy();
    expect(loading).toBeInTheDocument();
  });
});
