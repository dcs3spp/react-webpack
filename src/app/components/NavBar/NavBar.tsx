import React from 'react';

import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import TypoGraphy from '@material-ui/core/Typography';

import { NavLink } from 'react-router-dom';

import navbar from './NavBar.css';

const NavBarLinks = (): JSX.Element => (
  <>
    <Button
      color="inherit"
      component={NavLink}
      activeClassName={navbar.highlighted}
      to="/about"
    >
      About
    </Button>
    <Button
      activeClassName={navbar.highlighted}
      color="inherit"
      component={NavLink}
      to="/courses"
    >
      Courses
    </Button>
    <Button
      activeClassName={navbar.highlighted}
      color="inherit"
      component={NavLink}
      exact={true}
      to="/"
    >
      Home
    </Button>
  </>
);

export const NavBar = (): JSX.Element => (
  <AppBar color="primary" position="static">
    <Toolbar className={navbar.container}>
      <IconButton edge="start" color="inherit" aria-label="menu">
        <MenuIcon />
      </IconButton>
      <TypoGraphy className={navbar.lefthalf} variant="h6" color="inherit">
        React Demo
      </TypoGraphy>
      {<NavBarLinks />}
    </Toolbar>
  </AppBar>
);
