import '@testing-library/jest-dom/extend-expect';
import { fireEvent, RenderResult } from '@testing-library/react';
import React from 'react';
import { shallow } from 'enzyme';

import { createShallow } from '@material-ui/core/test-utils';
import MenuIcon from '@material-ui/icons/Menu';

import { NavBar } from './NavBar';
import { reactTesting } from '../../../../test/utils';

describe('<NavBar />', () => {
  let shallowFunc: typeof shallow;

  beforeAll(() => {
    shallowFunc = createShallow();
  });

  test('renders "React Demo", a MenuIcon and NavBarLinks instance', () => {
    const wrapper = shallowFunc(<NavBar />);
    const reactDemo = wrapper.find({ children: 'React Demo' });
    expect(reactDemo).toBeDefined();
    expect(reactDemo.length).toBeGreaterThan(0);
    expect(reactDemo.first().text()).toEqual('React Demo');

    const menuIcon = wrapper.find(MenuIcon);
    expect(menuIcon).toBeDefined();
    expect(menuIcon).toHaveLength(1);

    const navbarLinks = wrapper.find('NavBarLinks');
    expect(navbarLinks).toHaveLength(1);

    expect(wrapper).toMatchSnapshot();
  });

  test('renders About with /about path', () => {
    const wrapper: RenderResult = reactTesting.renderWithRouter(<NavBar />, [
      '/',
    ]);

    expect(wrapper.queryByText(/About/)).toBeTruthy();
    expect(wrapper.container.querySelector('[href="/about"]')).toBeTruthy();
  });

  test('renders Courses with /courses path', () => {
    const wrapper: RenderResult = reactTesting.renderWithRouter(<NavBar />, [
      '/',
    ]);

    expect(wrapper.queryByText(/Courses/)).toBeTruthy();
    expect(wrapper.container.querySelector('[href="/courses"]')).toBeTruthy();
  });

  test('renders Home with / path', () => {
    const wrapper: RenderResult = reactTesting.renderWithRouter(<NavBar />, [
      '/',
    ]);

    expect(wrapper.queryByText(/Home/)).toBeTruthy();
    expect(wrapper.container.querySelector('[href="/"]')).toBeTruthy();
  });

  test('click event for About navigates to /about path', () => {
    const wrapper: RenderResult = reactTesting.renderWithRouter(<NavBar />, [
      '/',
    ]);

    const about = wrapper.queryByText(/About/);
    expect(about).toBeTruthy();

    // test that ABOUT from '/about' path is not rendered
    expect(wrapper.queryByText('ABOUT')).not.toBeTruthy();

    // fire left click event on about
    fireEvent.click(wrapper.getByText(/About/), { button: 0 });

    // test that ABOUT from '/about' path is rendered
    expect(wrapper.queryByText('ABOUT')).toBeTruthy();
  });

  test('click event for Courses navigates to /courses path', () => {
    const wrapper: RenderResult = reactTesting.renderWithRouter(<NavBar />, [
      '/',
    ]);

    const about = wrapper.queryByText(/Courses/);
    expect(about).toBeTruthy();

    // test that ABOUT from '/courses' path is not rendered
    expect(wrapper.queryByText('COURSES')).not.toBeTruthy();

    // fire left click event on courses
    fireEvent.click(wrapper.getByText(/Courses/), { button: 0 });

    // test that ABOUT from '/courses' path is rendered
    expect(wrapper.queryByText('COURSES')).toBeTruthy();
  });

  test('click event for Home navigates to /home path', () => {
    const wrapper: RenderResult = reactTesting.renderWithRouter(<NavBar />, [
      '/about',
    ]);

    const home = wrapper.queryByText(/Home/);
    expect(home).toBeTruthy();

    // test that HOME from '/' route is not rendered
    expect(wrapper.queryByText('HOME')).not.toBeTruthy();

    // fire a left click event on home
    fireEvent.click(wrapper.getByText(/Home/), { button: 0 });

    // test that '/' is rendered
    expect(wrapper.queryByText('HOME')).toBeTruthy();
  });
});
