const links = {
  cuid: 'https://www.npmjs.com/package/cuid',
  material: 'https://material-ui.com',
  nestjs: 'https://nestjs.com',
  react: 'https://reactjs.org',
  redux: 'https://redux.js.org',
  readme: 'https://gitlab.com/dcs3spp/nestjs_application/blob/master/README.md',
  repository: 'https://gitlab.com/dcs3spp/nestjs_application',
};

export default links;
