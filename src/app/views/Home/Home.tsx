import React, { Fragment } from 'react';

import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { NavLink } from 'react-router-dom';

import links from './Links';
import homeStyles from './Styles';

const HomePage = (): JSX.Element => {
  const classes = homeStyles();

  return (
    <Fragment>
      <Paper className={classes.root}>
        <Typography variant="h5" component="h3">
          Home
        </Typography>
        <Typography component="span">
          <Box data-testid="homeIntroParagraph" textAlign="justify" mt={1}>
            This is small demo that uses <Link href={links.react}>React</Link>{' '}
            and <Link href={links.material}>Material-UI</Link> to retrieve
            resources from a Rest API. The Rest API is implemented using{' '}
            <Link href={links.nestjs}>NestJS</Link> and is available for
            download <Link href={links.repository}>here</Link>. Further details,
            concerning the build environment are available on the{' '}
            <Link component={NavLink} to="/about">
              About
            </Link>{' '}
            page.
          </Box>
          <Box textAlign="justify" mt={1}>
            <Link href={links.redux}>Redux</Link> is used to track errors from
            Http requests triggered by <Link href={links.react}>React</Link>{' '}
            components.
          </Box>
          <Box textAlign="justify" mt={1}>
            The architecture is illustrated in the image below:
          </Box>
          <div>
            <img
              className={classes.marginauto}
              src="https://i.ibb.co/VQgXR7v/redux-architecture.png"
              alt="Redux architecture"
            />
          </div>
          <Box>
            The following features are provided by the repository:
            <ul>
              <li>
                Generic epics are used to issue get all and get http requests.
                These can be instantiated with typed parameters for each api
                resource payload. The concept can be extended for patch and post
                requests. This reduces boilerplate code associated with creating
                Redux middleware handlers for making asynchronous http requests.
              </li>
              <li>
                Higher Order Component (HoC) for assigning a unique id to a
                React component, using <Link href={links.cuid}>cuid</Link>{' '}
                library.
              </li>
              <li>
                Higher Order Component (HoC) for logging api errors in the Redux
                store. If a component dispatches an asynchronous request that
                triggers an action then an error is raised on the store and
                associated with the component. The{' '}
                <em>withReduxErrorListener</em> Higher Order Component (HoC)
                bypasses rendering the base component if there are errors on the
                Redux store. In this case a minimised error card is rendered for
                the base component. This can be expanded to display an error
                dialog detailing further information, such as the source action
                that raised the error, component id, stack trace etc.
              </li>
            </ul>
          </Box>
          <Box
            data-testid="apiInstructionsParagraph"
            textAlign="justify"
            mt={1}
          >
            Clone the code from the{' '}
            <Link href={links.repository}>repository</Link> and start the Rest
            API Server following the instructions detailed in the{' '}
            <Link href={links.readme}>ReadMe</Link>. Visit the{' '}
            <Link component={NavLink} to="/courses">
              Courses
            </Link>{' '}
            page to retrieve and display courses.
          </Box>
        </Typography>
      </Paper>
    </Fragment>
  );
};

const Home = (): JSX.Element => {
  return <HomePage />;
};

export default Home;
