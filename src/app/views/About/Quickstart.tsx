import React, { Fragment } from 'react';

import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

const Quickstart = (): JSX.Element => {
  return (
    <Fragment>
      <Typography variant="h6">
        <Box textAlign="justify" mt={2}>
          <strong id="quickstart">Quickstart</strong>
        </Box>
      </Typography>
      <Typography component="span">
        <Box textAlign="justify" mt={1}>
          <em>npm run start:dev</em> will perform a development build and start
          webpack-dev-server running on port 8080. Visit http://localhost:8080
          to view the demo.
        </Box>
        <Box textAlign="justify" mt={1}>
          <em>npm run start:production</em> will perform a production build and
          then serve the contents of <em>dist</em> build folder running on port
          8080. Visit http://localhost:8080 to view the demo.
        </Box>
        <Box textAlign="justify" mt={1}>
          <em>npm run build:dev</em> will perform a development build, with
          build files stored in the dist folder.
        </Box>
        <Box textAlign="justify" mt={1}>
          <em>npm run build</em> will perform a production build, with build
          files stored in the dist folder.
        </Box>
        <Box textAlign="justify" mt={1}>
          <em>npm run build-report</em> will open the default browser to display
          the Webpack Bundle Analyzer report from the latest build.
        </Box>
        <Box textAlign="justify" mt={1}>
          <em>npm run coverage</em> will run tests and generate a coverage
          report in <em>coverage</em> folder in project root.
        </Box>
        <Box textAlign="justify" mt={1}>
          <em>npm run lint</em> will perform linting of Typescript source files
          under the <em>src</em> folder. Errors will be reported but not fixed.
        </Box>
        <Box textAlign="justify" mt={1}>
          <em>npm run lint:fix</em> will perform linting of Typescript source
          files under the <em>src</em> folder and automatically fix errors
        </Box>
        <Box textAlign="justify" mt={1}>
          <em>npm run clean</em> will remove the <em>dist</em> build folder.
        </Box>
      </Typography>
    </Fragment>
  );
};

export default Quickstart;
