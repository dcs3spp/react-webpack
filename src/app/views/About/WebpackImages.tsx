import React, { Fragment } from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

const Images = (): JSX.Element => {
  return (
    <Fragment>
      <Typography variant="h6">
        <Box textAlign="justify" mt={2}>
          <strong id="image">Images</strong>
        </Box>
      </Typography>
      <Typography component="span">
        <Box textAlign="justify" mt={1}>
          The paths for images are transformed to <em>assets/images/</em> within
          the <em>dist</em> build folder. Webpack will perform this
          transformation for images referenced in html files and images imported
          from Typescript source files.
        </Box>
        <Box textAlign="justify" mt={1}>
          The <em>global.d.ts</em> type declaration file has been added to the{' '}
          <em>src/@types</em> folder. This allows importing images for the
          following file types:
          <ul>
            <li>gif</li>
            <li>jpg</li>
            <li>jpeg</li>
            <li>png</li>
            <li>svg</li>
          </ul>
        </Box>
      </Typography>
    </Fragment>
  );
};

export default Images;
