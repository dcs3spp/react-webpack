import React, { Fragment } from 'react';

import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';

import links from './Links';

const useStyles = makeStyles({
  root: {
    width: '75%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 650,
  },
});

const EnvironmentVariablesTable = (): JSX.Element => {
  const classes = useStyles();
  return (
    <Paper className={classes.root}>
      <Table
        aria-label="environment variables table"
        className={classes.table}
        size="small"
      >
        <TableHead>
          <TableRow>
            <TableCell align="left">Environment</TableCell>
            <TableCell align="right">Variable</TableCell>
            <TableCell align="left">Description</TableCell>
            <TableCell align="right">Default</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow key="server_host">
            <TableCell align="left">
              <strong>Server</strong>
            </TableCell>
            <TableCell align="right">SERVER_HOST</TableCell>
            <TableCell align="left">
              The host or IP address for the server{' '}
            </TableCell>
            <TableCell align="right">localhost</TableCell>
          </TableRow>
          <TableRow key="server_port">
            <TableCell align="left"></TableCell>
            <TableCell align="right">SERVER_PORT</TableCell>
            <TableCell align="left">The port for the server</TableCell>
            <TableCell align="right">5000</TableCell>
          </TableRow>
          <TableRow key="api_base_url">
            <TableCell align="left">
              <strong>Application</strong>
            </TableCell>
            <TableCell align="right">API_BASE_URL</TableCell>
            <TableCell align="left">The url for the rest API </TableCell>
            <TableCell align="right">http://localhost:3000</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Paper>
  );
};

const Configuration = (): JSX.Element => {
  return (
    <Fragment>
      <Typography variant="h6">
        <Box textAlign="justify" mt={2}>
          <strong id="configuration">Configuration</strong>
        </Box>
      </Typography>
      <Typography component="span">
        <Box textAlign="justify" mt={1}>
          The server and application is configured using environment variables.
          The <em>webpack/webpack.env</em> file provides a function,{' '}
          <em>createAppConfig</em>, that returns the application configuration
          as an object.
        </Box>
        <Box textAlign="justify" mt={1}>
          A summary of the configuration environment variables is provided in
          the table below:
        </Box>
        <EnvironmentVariablesTable />
        <Box textAlign="justify" mt={1}>
          Development environments load environment variables from a
          configuration file located within a <em>dotenv</em> file (
          <em>.env</em>) from the root folder of the source repository.{' '}
        </Box>
        <Box textAlign="justify" mt={1}>
          Production environments should initialise configuration via
          environment variables on the host environment, e.g. using a{' '}
          <em>bash</em> script. Most cloud providers allow environment variables
          to be set using a web user interface.
        </Box>
        <Box textAlign="justify" mt={1}>
          Sensitive configuration details such as database credentials, API keys
          etc., should be configured by other means. For example, using secrets
          in docker and Kubernetes environments.
        </Box>
        <Box textAlign="justify" mt={1}>
          Webpack uses the <Link href={links.definePLugin}>DefinePlugin</Link>{' '}
          to expose a constant, <em>APP_CONF</em>, that is available for use in
          react pages. A type declaration for this object is provided in{' '}
          <em>./src/@types/custom-typings.d.ts</em>.
        </Box>
      </Typography>
    </Fragment>
  );
};

export default Configuration;
