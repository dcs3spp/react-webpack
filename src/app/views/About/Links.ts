const links = {
  brotli:
    'https://medium.com/oyotech/how-brotli-compression-gave-us-37-latency-improvement-14d41e50fee4',
  cssTypescriptTypings:
    'https://github.com/Jimdo/typings-for-css-modules-loader',
  eslint: 'https://eslint.org',
  definePLugin: 'https://webpack.js.org/plugins/define-plugin/',
  fileLoader: 'https://github.com/webpack-contrib/file-loader',
  gzip: 'https://www.gnu.org/software/gzip/',
  htmlLoader: 'https://webpack.js.org/loaders/html-loader/',
  terser: 'https://webpack.js.org/plugins/terser-webpack-plugin/',
  tsloader: 'https://github.com/TypeStrong/ts-loader',
  typescript: 'https://www.typescriptlang.org',
  webpack: 'https://webpack.js.org',
  webpackBundleAnalyzer:
    'https://github.com/webpack-contrib/webpack-bundle-analyzer',
  webpackMerge: 'https://github.com/survivejs/webpack-merge',
};

export default links;
