import React, { Fragment } from 'react';

import Box from '@material-ui/core/Box';

import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import aboutStyles from './Styles';
import Configuration from './Configuration';
import Images from './WebpackImages';
import links from './Links';
import Quickstart from './Quickstart';
import Stylesheet from './Stylesheet';
import Webpack from './Webpack';

const AboutPage = (): JSX.Element => {
  const classes = aboutStyles();

  return (
    <Fragment>
      <Paper className={classes.root}>
        <Typography variant="h4">About</Typography>
        <Typography component="span">
          <Box textAlign="justify" mt={1}>
            The following technologies are used for the build environment:
            <ul>
              <li>
                <Link href={links.eslint}>Eslint</Link>
              </li>
              <li>
                <Link href={links.typescript}>Typescript</Link>
              </li>
              <li>
                <Link href={links.webpack}>Webpack</Link>
              </li>
            </ul>
            The remainder of this page explains:
            <ul>
              <li>
                <Link href="#quickstart">Quickstart</Link>
              </li>
              <li>
                <Link href="#configuration">Configuration</Link>
              </li>
              <li>
                <Link href="#webpack">Webpack</Link>
              </li>
              <li>
                <Link href="#stylesheet">Stylesheet loading</Link>
              </li>
              <li>
                <Link href="#image">Image loading</Link>
              </li>
            </ul>
          </Box>
          <Quickstart />
          <Configuration />
          <Webpack />
          <Stylesheet />
          <Images />
        </Typography>
      </Paper>
    </Fragment>
  );
};

const About = (): JSX.Element => {
  return <AboutPage />;
};

export default About;
