import React, { Fragment } from 'react';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';

import links from './Links';

const Webpack = (): JSX.Element => {
  return (
    <Fragment>
      <Typography variant="h6">
        <Box textAlign="justify" mt={2}>
          <strong id="webpack">Webpack</strong>
        </Box>
      </Typography>
      <Typography component="span">
        <Box textAlign="justify" mt={2}>
          The webpack configuration files are stored within the webpack folder
          of the source repository:
          <ul>
            <li>
              <em>webpack.config.common</em>
            </li>
            <li>
              <em>webpack.config.development</em>
            </li>
            <li>
              <em>webpack.config.production</em>
            </li>
          </ul>
          <Link href="https://github.com/survivejs/webpack-merge">
            Webpack-merge
          </Link>{' '}
          is used to combine common configuration with development and
          production build settings.
        </Box>
        <Box textAlign="justify" mt={1}>
          <strong>Common</strong> configuration operations include:
          <ul>
            <li>
              Linting using <Link href={links.eslint}>es-lint</Link>. Linting
              rules are configured within <em>.eslintrc.yaml</em> of the source
              repository.
            </li>
            <li>
              Transpiling Typescript into Javascript using{' '}
              <Link href={links.tsloader}>ts-loader</Link>.
            </li>
            <li>
              Code splitting to create vendor bundles for material-ui and other
              node_modules.
            </li>
            <li>
              Loading static html files using the{' '}
              <Link href={links.htmlLoader}>html-loader</Link> plugin. Reference
              images are transformed to the path defined by the{' '}
              <Link href={links.fileLoader}>file-loader</Link> configuration.
            </li>
            <li>
              The <Link href={links.fileLoader}>file-loader</Link> plugin is
              configured to transform image paths to <em>assets/images/</em>
              within the <em>dist</em> folder.
            </li>
            <li>
              The{' '}
              <Link href={links.webpackBundleAnalyzer}>
                Webpack Bundle Analyzer
              </Link>{' '}
              plugin is configured to output build statistics to file{' '}
              <em>stats.json</em>, within the <em>dist</em> build directory.
            </li>
            <li>
              Resolves <em>@material-ui/core</em> as{' '}
              <em>@material-ui/core/es</em>, using es6. This works for use in
              evergreen browsers, e.g. NOT IE11. If need support for IE11 then
              remove the resolve alias.
            </li>
          </ul>
        </Box>
        <Box textAlign="justify" mt={1}>
          <strong>Development</strong> configuration operations include:
          <ul>
            <li>Inline source maps</li>
            <li>
              Webpack development server with (H)ot (M)odule (R)eplacement
              enabled on port 8080
            </li>
            <li>
              CSS stylesheets are embedded within the bundled entrypoint file{' '}
              <em>main.js</em>
            </li>
          </ul>
        </Box>
        <Box textAlign="justify" mt={1}>
          <strong>Production</strong> configuration operations include:
          <ul>
            <li>Minified source maps</li>
            <li>
              CSS stylesheets extracted into main.css within the <em>dist</em>{' '}
              build folder
            </li>
            <li>
              Minified bundling using the{' '}
              <Link href={links.terser}>TerserPlugin</Link>.
            </li>
            <li>
              React and React-dom loaded from CDN, reducing bundled assets size.
            </li>
            <li>
              Bundles compressed using <Link href={links.brotli}>Brotli</Link>{' '}
              and <Link href={links.gzip}>gzip</Link> compression.
            </li>
          </ul>
        </Box>
        <Box textAlign="justify" mt={1}>
          For both development and production builds a <em>stats.json</em> file
          from the Webpack Bundle Analyzer plugin is output to the <em>dist</em>{' '}
          directory. This can be viewed in the default browser by issuing the
          command:
          <em>bash npm run bundle-report</em>.
        </Box>
      </Typography>
    </Fragment>
  );
};

export default Webpack;
