import React, { Fragment } from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

const Stylesheet = (): JSX.Element => {
  return (
    <Fragment>
      <Typography variant="h6">
        <Box textAlign="justify" mt={2}>
          <strong id="stylesheet">Stylesheets</strong>
        </Box>
      </Typography>
      <Typography component="span">
        <Box textAlign="justify" mt={1}>
          Global css stylesheets are stored in{' '}
          <em>src/styles/styles/styles.css</em>. Css modules for components etc.
          can be added inside the <em>src/app</em> folder of the repository.
        </Box>
        <Box textAlign="justify" mt={1}>
          The <em>global.d.ts</em> type declaration file has been added to the{' '}
          <em>src/@types</em> folder, to allow importing of css files within
          Typescript source code.
        </Box>
      </Typography>
    </Fragment>
  );
};

export default Stylesheet;
