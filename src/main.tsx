import React from 'react';

import { Provider } from 'react-redux';
import { render } from 'react-dom';

import { App } from './app/components';
import store from './app/redux/store';

const rootElement = document.getElementById('root');
render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootElement,
);
