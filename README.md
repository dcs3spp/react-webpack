# Readme

This repository is a [Material-UI](https://material-ui.com) demo that uses [Redux](https://redux.js.org) to track errors from Http requests triggered by [React](https://reactjs.org) components. A Rest API is implemented using [NestJS](https://nestjs.com) and is available for download [here](https://gitlab.com/dcs3spp/nestjs_application). [Webpack](https://webpack.js.org) lints and bundles the main entrypoint file _./src/main.tsx_.

The project uses the following libaries:

- [axios](https://github.com/axios/axios) for making http requests
- [cuid](https://www.npmjs.com/package/cuid) for assigning unique component id.
- [redux-observable](https://redux-observable.js.org) for redux middleware
- [typesafe-actions](https://github.com/piotrwitek/typesafe-actions) for reducing boilerplate associated with created typed actions and reducer handlers

The architecture is illustrated in the image below:

![image](https://i.ibb.co/VQgXR7v/redux-architecture.png)

The following features are provided by the repository:

- Generic epics are used to issue get all and get http requests. These can be instantiated with typed parameters for each api resource payload. The concept can be extended for patch and post requests. This reduces boilerplate code associated with creating redux middleware handlers for making asynchronous http requests.
- Higher Order Component (HoC) for assigning a unique id to a react component, using [cuid](https://www.npmjs.com/package/cuid) library.
- Higher Order Component (HoC) for logging api errors in the redux store. If a component dispatches an asynchronous request that
  triggers an action then an error is raised on the store and associated with the component. The _withReduxErrorListener_ Higher Order Component (HoC) bypasses rendering the base component if there are errors on the redux store. In this case a minimised error card is rendered for the base component. This can be expanded to display an error dialog detailing further information, such as the source action that raised the error, component id, stack trace etc.
- Unit tests implemented using [react-testing-library](https://testing-library.com/docs/react-testing-library/intro) and [enzyme](https://airbnb.io/enzyme/).
- End-to-end tests implemented using [cypress](https://www.cypress.io).

## Quickstart

```bash
# clone source repository
git clone https://gitlab.com/dcs3spp/react-webpack

# change directory to the repo
cd react-webpack

# install the repo with npm
npm install

# create a .env file in the root of local source repository folder
# set environment variables using the .env.sample file as a guide

# perform development build and start webpack development server
# running on port configured from the SERVER_PORT value in .env file
npm run start:dev
```

Visit _http://localhost:${SERVER_PORT}_ to view the demo.

Other scripts include:

```bash
# perform development build only
npm run build:dev

# perform production build
npm run build

# view webpack-bundle-analyzer report
npm run bundle-report

# clean build files in ./dist
npm run clean

# perform type checking on root project
npm run check

# perform type checking on e2e tests in e2e folder
npm run check:cypress

# lint files in ./src/ and ./e2e/cypress, errors are reported
npm run lint

# lint files in ./src/ and ./e2e/cypress, errors are reported and automatically fixed
npm run lint:fix

# perform a development build and start webpack-dev-server
npm run start:dev

# perform a production build and use serve package to serve dist content
npm run start:prod

# Start a locally installed instance of the API Server.
npm run start:api

# Start a locally installed instance of the API Server, followed by the Webserver.
npm run start:server-and-api

# run unit and integration tests
npm run test

# run unit and integration tests with a test coverage report written to _./coverage_ folder
npm run coverage
```

# Environment

## Webpack

The webpack configuration files are stored within the webpack folder
of the source repository:

- webpack.config.common
- webpack.config.development
- webpack.config.production

[Webpack-merge](https://github.com/survivejs/webpack-merge) is used to combine common configuration with development and production build settings.

**Common** configuration operations include:

- Loading configuration from environment variables. A global _APP_CONF_ object is made avilable for use in react pages.
- Linting using [es-lint](https://eslint.org). Lint rules are configured within _.eslintrc.yaml_ of the source repository.
- Transpiling Typescript into Javascript using [ts-loader](https://github.com/TypeStrong/ts-loader).
- Code splitting to create vendor bundles for material-ui and other vendors in node_modules.
- Loading static html files using [html-loader](https://webpack.js.org/loaders/html-loader/). Images paths are transformed to the path defined by the [file-loader](https://github.com/webpack-contrib/file-loader) configuration.
- The [file-loader](https://github.com/webpack-contrib/file-loader) is configured to transform image paths to _assets/images/_ within the _dist_ folder.
- The [Webpack Bundle Analyzer](https://github.com/webpack-contrib/webpack-bundle-analyzer) plugin is configured to output build statistics to file _stats.json_, within the _dist_ build directory.
- Resolves @material-ui/core as @material-ui/core/es, using es6. This works for use in evergreen browsers, e.g. NOT IE11. If need support for IE11 then remove the resolve alias.

**Development** configuration operations include:

- Inline source maps
- Webpack development server with (H)ot (M)odule (R)eplacement enabled on port 8080
- CSS stylesheets are embedded within the bundled entrypoint file main.js

**Production** configuration operations include:

- Minified source maps
- CSS stylesheets extracted into main.css within the _dist_
  build folder
- Minified bundling using the [TerserPlugin](https://webpack.js.org/plugins/terser-webpack-plugin/).
- React and React-dom loaded from CDN, reducing bundled assets size.
- Bundles compressed using [Brotli](https://medium.com/oyotech/how-brotli-compression-gave-us-37-latency-improvement-14d41e50fee4) and [Gzip](https://www.gnu.org/software/gzip/) compression.

**Bundle Analyzer**

For both development and production builds a _stats.json_ file from the Webpack Bundle Analyzer plugin is output to the _dist_ directory. This can be viewed in the default browser by issuing the following command:

```bash
npm run bundle-report
```

## Stylesheets

Global css stylesheets are stored in _src/styles/styles/styles.css_. Css modules for components etc.
can be added inside the _src/app_ folder of the repository.

The _global.d.ts_ type declaration file has been added to the _src/@types_ folder. This allows importing css files within Typescript source code.

The [typings-for-css-modules-loader](https://github.com/Jimdo/typings-for-css-modules-loader) was considering to automatically generate the type declaration files for css files. However, this has a documented [bug](https://github.com/Jimdo/typings-for-css-modules-loader#typescript-doesnt-find-the-typings) that introduces a slower webpack build while generating the type declarations files for stylesheets. Consequently, the build process may have to be repeated for Typescript to successfully locate the generated type declaration files.

Stylesheets are imported using the default import syntax, e.g. `import styles from './mycss.css'`. This triggers webpack build to process bundling of the css stylesheet. Stylesheets are included _inline_ within the entry point bundle for development builds. For production builds, stylesheets are combined into a minimised _main.css_ file within the _dist_ build folder.

## Images

The paths for images are transformed to _assets/images/_ within the _dist_ build folder. Webpack will perform this transformation for images referenced in html files and images imported from Typescript source files.

The _global.d.ts_ type declaration file has been added to the _src/@types_ folder. This allows importing images for the following file types:

- gif
- jpg
- jpeg
- png
- svg

## Configuration

The server and application is configured using environment variables. The _webpack/webpack.env_ file provides a function, _createAppConfig_, that returns the application configuration as an object.

```javascript
const createAppConfig = function(env) {
  return Object.freeze({
    env: {
      apiBaseURL: `${process.env.API_BASE_URL}`,
      isProd: (env === 'production') | (env === 'prod'),
      mode: env,
    },
  });
};
```

Webpack uses the _DefinePlugin_ to expose a constant, _APP_CONF_, that is available for use in React pages. A type declaration for this object is provided in _./src/@types/custom-typings.d.ts_.

```typescript
declare const APP_CONF: AppConfig;

interface AppConfig {
  apiBaseURL: string;
  isProd: boolean;
  mode: string;
}
```

It also possible to set the _API_PATH_ environment variable to configure startup of a locally installed instance of the backend [API server](https://gitlab.com/dcs3spp/nestjs_application).

A summary of the configuration environment variables for the Webserver and API server, is provided in the table below:

| Environment |     Variable | Description                                     |               Default |
| :---------- | -----------: | ----------------------------------------------- | --------------------: |
| Webserver   | API_BASE_URL | The base url for the backend API server         | http://localhost:3000 |
|             |  SERVER_HOST | The host or IP address for the server           |             localhost |
|             |  SERVER_PORT | The port for the server                         |                  5000 |
| API server  |     API_PATH | Absolute path to installation dir of API server |             undefined |
|             |      DB_HOST | The host or IP address of backend DB            |             undefined |
|             |      DB_NAME | Name of backend DB                              |             undefined |
|             |      DB_USER | Username for DB connection                      |             undefined |
|             |  DB_PASSWORD | Password for DB connection                      |             undefined |
|             |    DB_SCHEMA | Schema to use for DB connection                 |             undefined |
|             |      DB_PORT | Port to use for DB connection                   |             undefined |

Development environments load configuration environment variables from within a _dotenv_ file (_.env_), located within the root folder of the source repository.

Production environments should initialise configuration via environment variables on the host environment, e.g. using a _bash_ script. Most cloud providers allow environment variables to be set using a web user interface. Unless the fall stack (Webserver, API server and backend DB) is installed within the local development environment, sensitive configuration details such as database credentials, API keys etc., should be configured by other means. For example, using secrets in docker and Kubernetes.

# Starting a local instance of the API Server

A local instance of the backend [API server](https://gitlab.com/dcs3spp/nestjs_application) can be started in addition to the webserver. This can be achieved by:

- Installing a local instance of the API Server, downloaded from the [repository](https://gitlab.com/dcs3spp/nestjs_application). For further details view the README file in the repository.
- Set the environment variables to configure the API Server before running the webserver. For example, the variables could be exported from within a _bash_ script. In particular, the _API_PATH_ environment variable should be configured to reference the absolute path of the locally installed API Server instance.
- NodeJS scripts are provided to run the API Server:
  - **start:api**: Start a locally installed instance of the API Server.
  - **start:server-and-api**: Start a locally installed instance of the API Server, followed by the Webserver.

# Testing

The following libraries are used for testing:

- [Cypress](https://www.cypress.io): Used for end to end tests
- [Enzyme](https://airbnb.io/enzyme/): Used with for unit and integration tests
- [React-Testing-Library](https://testing-library.com/docs/react-testing-library/intro): Used for unit and integration tests
- [Jest](https://jestjs.io): Test runner for Enzyme and React-Testing-Library tests

## Unit and Integration Tests

[Enzyme](https://airbnb.io/enzyme/) and [React-Testing-Library](https://testing-library.com/docs/react-testing-library/intro) are used with [Jest](https://jestjs.io) to perform unit and integration tests. The _/test_ folder, located within the repository root, has the following structure:

- The Jest configuration file for unit and integration tests is located within the _test/config/unit.config.js_.
- Jest pretest environment setup files should be placed within _test/setup_.
- Mocks for Redux store, API resources etc. should be placed within _test/mocks_.
- Resuable test utilities should be placed within _test/utils_.

Integration and unit test files should end with the suffix _.spec.ts_ or _.spec.tsx_. Each test file should be placed within the same folder as the source file that it tests.

Unit and integration test scripts include:

```bash
# run unit and integration tests
npm run test

# run unit and integration tests with a test coverage report written to ./coverage_ folder
npm run coverage
```

## End to End Testing

[Cypress](https://www.cypress.io) is used for end to end testing. Configuration and tests are located within _e2e/cypress_ and _e2e/cypress/tests_ folder, respectively. Video recordings of tests are disabled by default, but can easily be enabled by configuring the video option as true within _e2e/cypress/cypress.json_. Screenshots and videos (if enabled) are saved within _e2e/cypress/screenshots_ and _e2e/cypress/videos_. These locations are excluded from source control. 

A separate package has been created to accomodate Cypress end to end tests within the _e2e_ folder. This is to isolate dependencies from the main project, reducing the risk of conflicts. Furthermore, the e2e ```tsconfig.json``` and a separate ```node_modules``` folder prevents global type definition conflicts. For example, in a VSCode project that use Jest and Cypress, intellisense for either Jest or Cypress ```chai``` assertions could be lost.

The following end-to-end test scripts are provided in the package within the _e2e_ folder:
- **test:e2e:gui**: start a local instance of API server and webserver and open cypress tests in browser environment.
- **test:e2e:cli**: start a local instance of API server and webserver and open cypress tests in a terminal environment.
- **test:e2e:remoteApi:gui**: start a local instance of webserver and open cypress tests in a browser environment, assuming remote staging API server is started.
- **npm run test:e2e:remoteApi:cli**: start a local instance of webserver and open cypress tests in a terminal environment, assuming remote staging API server is started.

To perform end-to-end testing that starts a local instance of API and webserver before opening cypress tests within a browser environment, issue the commands listed below:

```bash
# move to the e2e subfolder and install the local package
cd e2e
npm install 

# run tests within browser instance
npm run test:e2e:gui

# run tests within a terminal
npm run test:e2e:cli
```

The above scripts assume a full stack installation of the webserver and API server on the local development machine. Please configure all environment variables before running this form of end-to-end test. The _configuration_ section details the environment variables required for configuration.

To perform end-to-end testing with a remote staging API server issue the commands listed below:

```bash
# move to the e2e subfolder and install the local package
cd e2e
npm install 

# run tests within browser instance
npm run test:e2e:remoteApi:gui

# run tests within a terminal
npm run test:e2e:remoteApi:cli
```

This assumes that the back end API server is up and running. Furthermore, the following environment variables should be set before the tests are started:

- _API_BASE_URL_: Remote url for the backend API server
- _SERVER_HOST_: Name or IP address of the webserver
- _SERVER_PORT_: The port number that the webserver is listening to requests on.

For a full list of configuration environment variables, please refer to the _Configuration_ section.
